//
//  MBGalleryViewController.m
//  ImageGallery
//
//  Created by Mobinius on 26/01/13.
//  Copyright (c) 2013 Mobinius. All rights reserved.
//
#import "ParentMessengerApi.h"
#import "MBGalleryViewController.h"
#import "UIImageView+AFNetworking.h"

@interface MBGalleryViewController(){
    
}

@end

@implementation MBGalleryViewController
@synthesize galleryImages = galleryImages_;
@synthesize imageHostScrollView = imageHostScrollView_;
@synthesize currentIndex = currentIndex_;

@synthesize prevImage;
@synthesize nxtImage;

@synthesize counterTitle;

@synthesize prevImgView;
@synthesize centerImgView;
@synthesize nextImgView;

@synthesize event;

@synthesize indexToSet;

// this implementation does a negative safe modulo operation, to compensate for this
//    NSLog(@"MOD -1 m 5 = %d", -1 % 5); => -1
//    NSLog(@"MOD -7 m 5 = %d", -7 % 5); => -2
//    NSLog(@"MOD -7 m 5 = %d", (5 + (-7 % 5)) % 5); => 3
// see also http://stackoverflow.com/questions/989943/weird-objective-c-mod-behavior
#define safeModulo(x,y) ((y + x % y) % y)

#pragma mark - IBAction methods

- (IBAction)close:(id)sender{
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Init toolbar

- (void)toolbarInit{
    
    UIBarButtonItem *itemLike = [[UIBarButtonItem alloc]
                                 initWithImage:[UIImage imageNamed:@"like"]
                                 style:UIBarButtonItemStyleBordered target:self
                                 action:@selector(like:)];
    UIBarButtonItem *itemShare = [[UIBarButtonItem alloc]
                                  initWithImage:[UIImage imageNamed:@"share"]
                                  style:UIBarButtonItemStyleBordered target:self
                                  action:@selector(share:)];
    UIBarButtonItem *flexibleItem = [[UIBarButtonItem alloc]
                                     initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                     target:nil action:nil];
    UIBarButtonItem *fixedItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedItem.width = -3.0f;
    
    [self.toolBar setItems:@[fixedItem, itemShare, flexibleItem, itemLike, fixedItem]];
}

#pragma mark - View lifecycle

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self toolbarInit];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    self.view.backgroundColor = [UIColor blackColor];
    self.imageHostScrollView.backgroundColor = [UIColor clearColor];
    
    
    self.imageHostScrollView.showsVerticalScrollIndicator = NO;
    self.imageHostScrollView.showsHorizontalScrollIndicator = NO;

    self.imageHostScrollView.contentSize = CGSizeMake(CGRectGetWidth(self.imageHostScrollView.frame)*3, CGRectGetHeight(self.imageHostScrollView.frame));
    
    self.imageHostScrollView.delegate = self;
    
    CGRect rect = CGRectZero;
    
    CGFloat offset = 0.0f;
    
    if ([UIScreen mainScreen].bounds.size.height == 568)
        offset = 64.0f;
    else
        offset = 64.0f + 568.0f - 480.0f;
    
    rect.size = CGSizeMake(CGRectGetWidth(self.imageHostScrollView.frame), CGRectGetHeight(self.imageHostScrollView.frame) - offset);
    
    CGRect imageFrame = CGRectMake(0.0f, 0.0f, 320.0f, rect.size.height);
    
    // add prevView as first in line
    self.prevImgView = [[UIImageView alloc] initWithFrame:imageFrame];
    self.prevImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.prevImgView.backgroundColor = [UIColor clearColor];

    
    UIScrollView *scrView = [[UIScrollView alloc] initWithFrame:rect];
    scrView.backgroundColor = [UIColor clearColor];
    
    scrView.delegate = self;
    scrView.minimumZoomScale = 0.5;
    scrView.maximumZoomScale = 2.5;
    
    [scrView addSubview:self.prevImgView];
    [self.imageHostScrollView addSubview:scrView];
    
    // add currentView in the middle (center)
    rect.origin.x += CGRectGetWidth(self.imageHostScrollView.frame);
    self.centerImgView = [[UIImageView alloc] initWithFrame:imageFrame];
    self.centerImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.centerImgView.backgroundColor = [UIColor clearColor];
    
    scrView = [[UIScrollView alloc] initWithFrame:rect];
    scrView.backgroundColor = [UIColor clearColor];
    scrView.delegate = self;
    scrView.minimumZoomScale = 0.5;
    scrView.maximumZoomScale = 2.5;
    
    
    [scrView addSubview:self.centerImgView];
    [self.imageHostScrollView addSubview:scrView];
    
    // add nextView as third view
    rect.origin.x += CGRectGetWidth(self.imageHostScrollView.frame);
    self.nextImgView = [[UIImageView alloc] initWithFrame:imageFrame];
    self.nextImgView.contentMode = UIViewContentModeScaleAspectFit;
    self.nextImgView.backgroundColor = [UIColor clearColor];
    
    scrView = [[UIScrollView alloc] initWithFrame:rect];
    scrView.backgroundColor = [UIColor clearColor];
    scrView.delegate = self;
    scrView.minimumZoomScale = 0.5;
    scrView.maximumZoomScale = 2.5;
    
    [scrView addSubview:self.nextImgView];
    [self.imageHostScrollView addSubview:scrView];
    
    
    // center the scrollview to show the middle view only
    [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
    self.imageHostScrollView.userInteractionEnabled = YES;
    self.imageHostScrollView.pagingEnabled = YES;
    self.imageHostScrollView.delegate = self;
    
    //some data for testing
    self.galleryImages = [[NSMutableArray alloc] init];
    
    for (EventPropertyValue * epv in event.event2EventProperty){
        
        for (EventPropertyValueBag * epvBag in epv.eventProperty2EventPropertyValueBag){
            
            [self.galleryImages addObject:[[ParentMessengerApi create].baseUrl stringByAppendingFormat: @"image/%@", epvBag.value]];
        }
    }
    
    self.currentIndex = self.indexToSet;
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recognizer.numberOfTapsRequired = 2;
    recognizer.delegate = self;
    [self.imageHostScrollView addGestureRecognizer:recognizer];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 320.0f, 1.0f)];
    view.backgroundColor = [UIColor colorWithRed:29.0f/255.0f
                                           green:29.0f/255.0f
                                            blue:29.0f/255.0f
                                           alpha:1.0f];
    [self.view addSubview:view];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];

    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationController.navigationBar.barTintColor = [UIColor clearColor];
    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor whiteColor]};
}

#pragma mark -navigation methods

- (IBAction)nextImage:(id)sender {
//    [self setRelativeIndex:1];    
}

- (IBAction)prevImage:(id)sender {
//    [self setRelativeIndex:-1];
    
    NSLog(@"Action taken ");
}

#pragma mark - color button actions-    
 #pragma mark -page controller action-

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView;  {
    //incase we are zooming the center image view parent 
    if (self.centerImgView.superview == scrollView){
        return self.centerImgView;
    }
    
    return nil;
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView
{
    UIView *subView = [scrollView.subviews objectAtIndex:0];
    
    CGFloat offsetX = (scrollView.bounds.size.width > scrollView.contentSize.width)?
    (scrollView.bounds.size.width - scrollView.contentSize.width) * 0.5 : 0.0;
    
    CGFloat offsetY = (scrollView.bounds.size.height > scrollView.contentSize.height)?
    (scrollView.bounds.size.height - scrollView.contentSize.height) * 0.5 : 0.0;
    
    subView.center = CGPointMake(scrollView.contentSize.width * 0.5 + offsetX,
                                 scrollView.contentSize.height * 0.5 + offsetY);
}

- (void)scrollViewDidScroll:(UIScrollView *)sender {
    [sender setContentOffset: CGPointMake(sender.contentOffset.x, 0)];
    // or if you are sure you wanna it always on top:
    // [aScrollView setContentOffset: CGPointMake(aScrollView.contentOffset.x, 0)];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;{
    CGFloat pageWidth = scrollView.frame.size.width;
    previousPage_ = floor((scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1;

}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)sender {
    CGFloat pageWidth = sender.frame.size.width;
    int page = floor((sender.contentOffset.x - pageWidth / 2) / pageWidth) + 1;
    
    //incase we are still in same page, ignore the swipe action
    if(previousPage_ == page) return;
    
    if(sender.contentOffset.x >= sender.frame.size.width) {   
        //swipe left, go to next image
        [self setRelativeIndex:1];
        
        // center the scrollview to the center UIImageView
        [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
	}     
	else if(sender.contentOffset.x < sender.frame.size.width) { 
        //swipe right, go to previous image
        [self setRelativeIndex:-1];
        
        // center the scrollview to the center UIImageView
        [self.imageHostScrollView setContentOffset:CGPointMake(CGRectGetWidth(self.imageHostScrollView.frame), 0)  animated:NO];
	}     
    
    UIScrollView *scrollView = (UIScrollView *)self.centerImgView.superview;
    scrollView.zoomScale = 1.0;
}

- (void)handleTap:(UITapGestureRecognizer *)recognizer {
    UIScrollView *scrollView = (UIScrollView*)self.centerImgView.superview;
    float scale = scrollView.zoomScale;
    scale += 1.0;
    if(scale > 2.0) scale = 1.0;
    [scrollView setZoomScale:scale animated:YES];
}

#pragma mark - image loading-

- (NSString *)getPathForIndex:(NSInteger)path{
    
    path = safeModulo(path, [self totalImages]);
    
    NSString *filePath = [self.galleryImages objectAtIndex:path];
    
	return filePath;
}

- (NSURL *)urlForIndex:(NSInteger)index{
    
    return [NSURL URLWithString:[self getPathForIndex:index]];
}

#pragma mark -

- (NSInteger)totalImages {
    return [self.galleryImages count];
}
- (NSInteger)currentIndex {
    
    return safeModulo(currentIndex_, [self totalImages]);
}

- (void)setCurrentIndex:(NSInteger)inIndex {
    
    currentIndex_ = inIndex;
    
    NSString *title = [NSString stringWithFormat:@"%d of %d",self.currentIndex+1,[self.galleryImages count]];
    self.title = title;
//    titleLabel.text = title;
    
    if([galleryImages_ count] > 0){
        
        [self.prevImgView setImageWithURLRequest:[NSURLRequest requestWithURL:[self urlForIndex:[self relativeIndex:-1]]]
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                             
                                         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                             
                                         }];
        
        [self.centerImgView setImageWithURLRequest:[NSURLRequest requestWithURL:[self urlForIndex:[self relativeIndex:0]]]
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                             
                                         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                             
                                         }];
        
        [self.nextImgView setImageWithURLRequest:[NSURLRequest requestWithURL:[self urlForIndex:[self relativeIndex:+1]]]
                                         success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                             
                                         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
                                             
                                         }];
        
//        self.prevImgView.image   = [self imageAtIndex:[self relativeIndex:-1]];
//        self.centerImgView.image = [self imageAtIndex:[self relativeIndex: 0]];
//        self.nextImgView.image   = [self imageAtIndex:[self relativeIndex: 1]];
    }
}

- (NSInteger)relativeIndex:(NSInteger)inIndex {
    return safeModulo(([self currentIndex] + inIndex), [self totalImages]);
}

- (void)setRelativeIndex:(NSInteger)inIndex {
    [self setCurrentIndex:self.currentIndex + inIndex];
}

#pragma mark - action

- (void)like:(id)sender{
    
}

- (void)share:(id)sender{
    
//    NSString *path = self.galleryImages[self.currentIndex];
//    NSURL *urlPath = [NSURL URLWithString:path];
//    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:urlPath]];
    
    UIImage *image = self.centerImgView.image;
    
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[image]
                                      applicationActivities:nil];
    
    [self.navigationController presentViewController:activityViewController
                                            animated:YES
                                          completion:^{
                                              // ...
                                          }];
    
}

@end