//
//  DetailCollectionCell.h
//  ParentMessenger
//
//  Created by lolsi on 30.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface DetailCollectionCell : UICollectionViewCell

@property (nonatomic, weak) IBOutlet UIImageView *imgView;

@end