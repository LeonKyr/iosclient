//
//  HelperRegister.h
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <Foundation/Foundation.h>

@interface HelperRegister : NSObject

@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *phone;
@property (nonatomic, strong) NSString *password;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) UIImage *photo;

@property (nonatomic, strong) NSString *childCode;
@property (nonatomic, assign) BOOL rememberMe;

@property (nonatomic, strong, getter =  getSfullName, readonly) NSString *fullName;

+ (HelperRegister*)sharedHelper;

@end