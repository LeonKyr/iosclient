//
//  LearningINViewController.m
//  ParentMessenger
//
//  Created by lolsi on 16.06.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "LearningINViewController.h"

@interface LearningINViewController ()

@end

@implementation LearningINViewController

@synthesize skipButton;
@synthesize pageControl;
@synthesize scrollView;

@synthesize imageData;

@synthesize isPushed;

#define keyTitle @"titleKey"
#define keyImage @"imageKey"

#pragma mark - UI customize methods

- (void)customizeUI{
    
    self.fullSkipButton.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    [self.fullSkipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.fullSkipButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.skipButton.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    [self.skipButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.skipButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.mainButon.backgroundColor = [UIColor colorWithRed:150.0f/255.0f green:150.0f/255.0f blue:150.0f/255.0f alpha:1.0f];
    [self.mainButon setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.mainButon setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
}

#pragma mark - IBAction methods

- (IBAction)skip:(id)sender{
    
    if (isPushed)
        [self.navigationController popViewControllerAnimated:YES];
    else
        [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - view life cycle

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationItem.hidesBackButton = YES;
    [self.navigationController.navigationBar setHidden:YES];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewDidDisappear:animated];
    
    [self.navigationController.navigationBar setHidden:NO];
}


- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self customizeUI];
    
    self.imageData = @[@{keyTitle: NSLocalizedString(@"Follow you child", nil), keyImage: @"main-screen.png"},
                       @{keyTitle: NSLocalizedString(@"Find details of every event", nil), keyImage: @"event-details.png"},
                       @{keyTitle: NSLocalizedString(@"Start receiving events about your Child", nil), keyImage: @"add-child.png"},
                       @{keyTitle: NSLocalizedString(@"Introduce your Day Care Centre", nil), keyImage: @"introduce-dcc.png"},
@{keyTitle: NSLocalizedString(@"Provide your feedback", nil), keyImage: @"1.png"}
                       //                       @{keyTitle: NSLocalizedString(@"Provide your feedback", nil), keyImage: @"feedback.png"}
                       ];
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
	// define the scroll view content size and enable paging
	[self.scrollView setPagingEnabled: YES];
    
	[self.scrollView setContentSize: CGSizeMake(self.scrollView.bounds.size.width * [self.imageData count], self.scrollView.bounds.size.height)] ;
    
    self.pageControl.backgroundColor = [UIColor clearColor];
	[self.pageControl setNumberOfPages:[self.imageData count]];
	[self.pageControl setCurrentPage:0];
	[self.pageControl addTarget:self
                         action:@selector(pageControlClicked:)
               forControlEvents:UIControlEventValueChanged] ;
	[self.pageControl setDefersCurrentPageDisplay:YES];
	[self.pageControl setType:DDPageControlTypeOnFullOffEmpty];
	[self.pageControl setOnColor:[UIColor colorWithRed:153.0/255.0f
                                                 green:153.0/255.0f
                                                  blue:153.0/255.0f
                                                 alpha:1.0f]];
	
    [self.pageControl setOffColor:[UIColor colorWithRed:204.0f/255.0f
                                                  green:204.0f/255.0f
                                                   blue:204.0f/255.0f alpha:1.0f]];
    
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 5.0f];
	
    UILabel *pageLabel;
    UIImageView *imageView;
	CGRect pageFrame;
    
	for (int i = 0 ; i < [self.imageData count] ; i++){
        
		NSDictionary *dict = self.imageData[i];
        
        pageFrame = CGRectMake(i * scrollView.bounds.size.width, 0.0f, scrollView.bounds.size.width, scrollView.bounds.size.height);
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(pageFrame.origin.x + 50.0f, pageFrame.origin.y + 30, pageFrame.size.width - 100.0f, pageFrame.size.height - 90.0f)];
        imageView.image =  [LearningINViewController drawText:@"Some text"
                                        inImage:[UIImage imageNamed:dict[keyImage]]
                                        atPoint:CGPointMake(50, 30)];
        imageView.backgroundColor = [UIColor clearColor];
        
        CALayer * l = [imageView layer];
        [l setMasksToBounds:YES];
        [l setCornerRadius:6.25];
        
        [self.scrollView addSubview:imageView];
        
        pageLabel = [[UILabel alloc] initWithFrame:CGRectMake(pageFrame.origin.x, pageFrame.size.height - 30.0f, pageFrame.size.width, 20.0f)];
        pageLabel.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
        pageLabel.textAlignment = NSTextAlignmentCenter;
        pageLabel.numberOfLines = 1;
        pageLabel.text = dict[keyTitle];
        pageLabel.backgroundColor = [UIColor clearColor];
        pageLabel.textColor = [UIColor colorWithRed:153.0f/255.0f
                                              green:153.0f/255.0f
                                               blue:153.0f/255.0f
                                              alpha:1.0f];
        [self.scrollView addSubview:pageLabel];
	}
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
}

+(UIImage*) drawText:(NSString*) text
             inImage:(UIImage*)  image
             atPoint:(CGPoint)   point
{
    
    UIFont *font = [UIFont boldSystemFontOfSize:12];
    UIGraphicsBeginImageContext(image.size);
    [image drawInRect:CGRectMake(0,0,image.size.width,image.size.height)];
    CGRect rect = CGRectMake(point.x, point.y, image.size.width, image.size.height);
    [[UIColor whiteColor] set];
    [text drawInRect:CGRectIntegral(rect) withFont:font];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (void)setShowHideTwoButton:(BOOL)value{
    
    if (value){
     
        self.mainButon.hidden = YES;
        self.skipButton.hidden = YES;
        
        self.fullSkipButton.hidden = NO;
    
    } else{
        
        self.mainButon.hidden = NO;
        self.skipButton.hidden = NO;
        
        self.fullSkipButton.hidden = YES;
    }
}

#pragma mark - DDPageControl triggered actions

- (void)pageControlClicked:(id)sender{
    
	DDPageControl *thePageControl = (DDPageControl *)sender ;
    
    [scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
    
    [pageControl updateCurrentPageDisplay];
}

#pragma mark - UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView{
    
	CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (pageControl.currentPage != nearestNumber)
	{
		pageControl.currentPage = nearestNumber ;
        
        switch (pageControl.currentPage) {
            case 0:
                [self setShowHideTwoButton:YES];
                break;
            case 1:
                [self setShowHideTwoButton:YES];
                break;
            case 2:{
             
                [self.mainButon addTarget:self
                                   action:@selector(setMainSelector)
                         forControlEvents:UIControlEventTouchUpInside];
                [self.mainButon setTitle:@"Add Now!" forState:UIControlStateNormal];
                [self setShowHideTwoButton:NO];
            }
                break;
            case 3:{
                
                [self.mainButon addTarget:self
                                   action:@selector(setMainSelector)
                         forControlEvents:UIControlEventTouchUpInside];
                [self.mainButon setTitle:NSLocalizedString(@"Introduce", nil) forState:UIControlStateNormal];
                [self setShowHideTwoButton:NO];
            }
                break;
            case 4:{
                
                [self.mainButon addTarget:self
                                   action:@selector(setMainSelector)
                         forControlEvents:UIControlEventTouchUpInside];
                [self.mainButon setTitle:@"Feedback" forState:UIControlStateNormal];
                [self setShowHideTwoButton:NO];
            }
                break;
            default:
                break;
        }
        
		
		if (scrollView.dragging)
			[pageControl updateCurrentPageDisplay] ;
	}
}

- (void)setMainSelector{
    
    UIViewController *vc;
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
    
    switch (pageControl.currentPage) {
        case 2:
            
            vc = [storyboard instantiateViewControllerWithIdentifier:@"addChild"];
            break;
        
        case 3:
            
            vc = [storyboard instantiateViewControllerWithIdentifier:@"introduceKindergarten"];
            break;
            
        case 4:
            
            vc = [storyboard instantiateViewControllerWithIdentifier:@"feedbackVC"];
            break;
            
        default:
            break;
    }
    
    if (vc){

        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    
	return (interfaceOrientation == UIInterfaceOrientationPortrait) ;
}

@end