//
//  SecontRegisterViewController.m
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "SecondRegisterViewController.h"

@interface SecondRegisterViewController (){
    
    UIImage *chosenImage;
}

@end

@implementation SecondRegisterViewController

@synthesize firstName;
@synthesize secondName;

@synthesize photoButton;

#pragma mark - UIImagePickerController delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info{
    
    chosenImage = info[UIImagePickerControllerEditedImage];
    
    [self.photoButton setImage:chosenImage forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UIACtionSheet delegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex != 2){
        
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.delegate = self;
        controller.allowsEditing = YES;
        
        if (buttonIndex == 0){
            
            controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:controller animated:YES completion:nil];
            
        } else if(buttonIndex == 1){
            
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]){
                controller.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:controller animated:YES completion:nil];
            } else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Alert", nil)
                                                                message:NSLocalizedString(@"Camera not available", nil)
                                                               delegate:self
                                                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
        }
        
    }
}

#pragma mark - validate field

- (BOOL)validateField:(UITextField*)textField{
    
    BOOL value = textField.text.length > 0 ? YES : NO;
    
    [self setColorForLabelInTextField:textField forValue:value];
    
    return value;
}

#pragma mark - IBAction methods

- (IBAction)setPhoto:(id)sender{
    
    [self.view endEditing:YES];
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Set photo", nil)
                                                             delegate:self
                                                    cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:NSLocalizedString(@"with galery", nil), NSLocalizedString(@"with camera", nil), nil];
    [actionSheet showInView:self.view];

    
}

- (IBAction)nextMethod:(id)sender{
    
    [super nextMethod:sender];
    
    BOOL validateFirst = [self validateField:self.firstName];
    BOOL validateLast = [self validateField:self.secondName];
//    BOOL validatePhoto = chosenImage ? YES : NO;
    
//    if (!validatePhoto){
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Dear user", nil)
//                                                        message:NSLocalizedString(@"Please check photo", nil)
//                                                       delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    
    if (validateFirst && validateLast
//        && validatePhoto
        ){
        
        self.helperRegister.firstName = self.firstName.text;
        self.helperRegister.lastName = self.secondName.text;
        self.helperRegister.photo = chosenImage;
        
        [self performSegueWithIdentifier:@"TwoToThirdIdentifier" sender:self];
    }
}

#pragma mark - customize UI

- (void)customizeUI{
    
    [super customizeUI];
    
    self.stepView.step = 2;
    
    [self customizeTextField:self.firstName andPlaceHolderText:NSLocalizedString(@"First name", nil) andWidth:105.0f];
    [self customizeTextField:self.secondName andPlaceHolderText:NSLocalizedString(@"Last name", nil) andWidth:105.0f];
    
    UIColor *borderColor = [UIColor colorWithRed:217.0f/255.0f
                                           green:217.0f/255.0f
                                            blue:217.0f/255.0f
                                           alpha:1.0f];
    UIView *viewBottomLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.photoButton.frame.size.height - 1.0f, self.photoButton.frame.size.width, 1.0f)];
    viewBottomLine.backgroundColor = borderColor;
    [self.photoButton addSubview:viewBottomLine];
    
    viewBottomLine = [[UIView alloc] initWithFrame:CGRectMake(self.photoButton.frame.size.width - 1.0f, 0.0f, 1.0f, self.photoButton.frame.size.height)];
    viewBottomLine.backgroundColor = borderColor;
    [self.photoButton addSubview:viewBottomLine];
    
}


#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];
}

@end