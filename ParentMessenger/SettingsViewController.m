//
//  SettingsViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 17/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                                                        green:178.0f/255.0f
                                                                         blue:178.0f/255.0f
                                                                        alpha:1.0f];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;

    // check here if key exists in the defaults or not, if yes the retrieve results in array
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"mobileNotificaion"] != nil) {
//        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"mobileNotificaion"]);
        [self.mobileNotification setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"mobileNotificaion"]];
    }
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"emailNotification"] != nil) {
        NSLog(@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:@"emailNotification"]);
        [self.emailNotification setOn:[[NSUserDefaults standardUserDefaults] boolForKey:@"emailNotification"]];
    }

    
    //For saving
//    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
//    [defaults setObject:self.numbers forKey:@"mobileNotificaion"];
//    [defaults synchronize];
 
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}
- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    //For saving
    NSUserDefaults *defaults=[NSUserDefaults standardUserDefaults];
    [defaults setBool:self.mobileNotification.on forKey:@"mobileNotificaion"];
    [defaults setBool:self.emailNotification.on forKey:@"emailNotification"];
    
    [defaults synchronize];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    }   
    else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a story board-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

 */

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

@end
