//
//  RegistrationViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 11/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "AppDelegate.h"
#import "ParentMessengerApi.h"
#import "RegistrationViewController.h"
#import "CryptoHelper.h"

#import <QuartzCore/QuartzCore.h>
#import <MobileCoreServices/UTCoreTypes.h>

#define keyEmail @"emailKeyMy"
#define keyRemember @"rememberMeKey"
#define keyPassword @"passwordKeyMy"

@interface RegistrationViewController (){
    
    BOOL termsEnabled;
}

@end

@implementation RegistrationViewController

@synthesize userView;
@synthesize dataView;

@synthesize emailField;
@synthesize childField;
@synthesize passwordField;

@synthesize firstNameField;
@synthesize secondNameField;

@synthesize photoButton;

@synthesize termsLabel;

@synthesize termsButton;
@synthesize cancelButton;
@synthesize registrationButton;

@synthesize switch_my;
@synthesize remember_label;

#pragma mark - customize UI

- (void)customizeUI{
    
    self.remember_label.text = NSLocalizedString(@"Remember me", nil);
    self.remember_label.font = [UIFont fontWithName:@"Helvetica Neue" size:13.33f];
    self.remember_label.textColor = [UIColor lightTextColor];
    
    self.view.backgroundColor = [UIColor colorWithRed:5.0f/255.0f
                                                green:3.0f/255.0f
                                                 blue:24.0f/255.0f
                                                alpha:1.0f];
    
    self.dataView.layer.cornerRadius = 4.0f;
    self.userView.layer.cornerRadius = 4.0f;
    
    self.dataView.backgroundColor = [UIColor whiteColor];
    self.userView.backgroundColor = [UIColor whiteColor];
    
    self.emailField.layer.cornerRadius = 4.0f;
    self.passwordField.layer.cornerRadius = 4.0f;
    
    UIColor *backgroundColor = [UIColor whiteColor];
    self.emailField.backgroundColor = backgroundColor;
    self.childField.backgroundColor = backgroundColor;
    self.passwordField.backgroundColor = backgroundColor;
    
    self.firstNameField.backgroundColor = backgroundColor;
    self.secondNameField.backgroundColor = backgroundColor;
    
    self.firstNameField.layer.cornerRadius = 4.0f;
    self.secondNameField.layer.cornerRadius = 4.0f;
    
    self.emailField.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
    self.childField.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
    self.passwordField.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
    self.firstNameField.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
    self.secondNameField.font = [UIFont fontWithName:@"Helvetica Neue" size:14.0f];
    
    self.emailField.placeholder = @"john_smith@gmail.com";
    self.childField.placeholder = @"Unique child code (if exist)";
    self.passwordField.placeholder = @"Min 7 characters";
    
    self.firstNameField.placeholder = @"John";
    self.secondNameField.placeholder = @"Smith";
    
    self.passwordField.secureTextEntry = YES;

    [self createToTextField:self.emailField andText:NSLocalizedString(@"Email", nil) andWidth:80.0f];
    [self createToTextField:self.childField andText:NSLocalizedString(@"Child code", nil) andWidth:80.0f];
    [self createToTextField:self.passwordField andText:NSLocalizedString(@"Password", nil) andWidth:80.0f];
    
    [self createToTextField:self.firstNameField andText:@"First" andWidth:55.0f];
    [self createToTextField:self.secondNameField andText:@"Second" andWidth:55.0f];
    
    self.termsLabel.textColor = [UIColor colorWithRed:163.0f/255.0f
                                                green:161.0f/255.0f
                                                 blue:182.0f/255.0f
                                                alpha:1.0f];
    
    self.termsLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:10.0f];
    
    self.termsButton.layer.cornerRadius = 2.5;
    self.termsButton.layer.borderWidth = 0.5f;
    self.termsButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.termsButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:11.5f];
    [self.termsButton setTitle:NSLocalizedString(@"Terms", nil) forState:UIControlStateNormal];
    
    self.registrationButton.layer.cornerRadius = 2.5;
    self.registrationButton.layer.borderWidth = 0.5f;
    self.registrationButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.registrationButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:11.5f];
    [self.registrationButton setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    
    self.cancelButton.layer.cornerRadius = 2.5;
    self.cancelButton.layer.borderWidth = 0.5f;
    self.cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:11.5f];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
    [self.cancelButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self.registrationButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.switch_my.onTintColor = [UIColor lightTextColor];
    self.switch_my.isRounded = NO;
    self.switch_my.backgroundColor = [UIColor clearColor];
}

#pragma mark - create rightView to UITextField

- (void)createToTextField:(UITextField*)textField andText:(NSString*)text andWidth:(CGFloat)width{
    
    textField.leftView = ({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width, textField.frame.size.height)];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width - 5.0f, textField.frame.size.height)];
       
        label.font = [UIFont fontWithName:@"Helvetica Neue" size:12.0f];
        label.text = text;
        label.textAlignment = NSTextAlignmentRight;
        label.textColor = [UIColor colorWithRed:101.0f/255.0f
                                          green:101.0f/255.0f
                                           blue:101.0f/255.0f
                                          alpha:1.0f];
        
        [view addSubview:label];
        
        view;
    });
    
    textField.leftViewMode = UITextFieldViewModeAlways;
}

#pragma mark - touch delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];

    [self customizeUI];
    
    [self.activityIndicator stopAnimating];
}

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    textField.returnKeyType = UIReturnKeyDone;
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

#pragma mark - IBAction methods

- (IBAction)termsSet:(id)sender{
    
    if (termsEnabled){
        
        [self.termsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.termsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
        self.termsButton.layer.borderColor = [UIColor whiteColor].CGColor;
        
    } else {
        
        [self.termsButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        [self.termsButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
        self.termsButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
        
    }
    
    termsEnabled = !termsEnabled;
}

- (IBAction)selectPhoto:(id)sender{
    
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return;
    }
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    mediaUI.mediaTypes = @[(NSString*)kUTTypeImage];
    
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = self;
    
    [self.navigationController presentViewController:mediaUI animated:YES completion:nil];
}

- (IBAction)registration:(id)sender{
    
    if (termsEnabled){
        
        BOOL value = self.switch_my.isOn;
        
        NSUserDefaults *standartUserDefaults = [NSUserDefaults standardUserDefaults];
        
        if (value){
            
            [standartUserDefaults setObject:self.emailField.text forKey:keyEmail];
            [standartUserDefaults setObject:self.passwordField.text forKey:keyPassword];
            [standartUserDefaults setBool:YES forKey:keyRemember];
        }
        
        [standartUserDefaults synchronize];
        
        [self.activityIndicator startAnimating];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ParentMessengerApi* api = [ParentMessengerApi create];
            
            NSString *name = [[self.firstNameField.text stringByAppendingString:@" "] stringByAppendingString:self.secondNameField.text];
            NSString *email = self.emailField.text;
            NSString * hashedPassword = [CryptoHelper md5:self.passwordField.text];
            
            Listener* listener = [api registerWithName:name
                                              andEmail:email
                                           andPassword:hashedPassword
                                              andImage:self.photoButton.imageView.image];
            
            if (listener)
            {
                NSLog(@"RegistrationViewController=%@", listener.externalId);
            }
            [self performSelectorOnMainThread:@selector(fetchedData:)
                                   withObject:listener waitUntilDone:YES];
        });
    } else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Dear user"
                                                        message:@"Please agree terms & conditions"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark - Feetch data

- (void)fetchedData:(Listener *)listener {
    
    [self.activityIndicator stopAnimating];
    
    if (listener)
    {
        NSLog(@"LISTENERID=%@", listener.externalId);
        NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
        [settings setBool:YES forKey:@"newUserRegister"];
        [settings setObject:listener.externalId forKey:@"listenerExternalId"];
        [settings synchronize];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.model.currentListener = listener;
        [appDelegate.model.currentListener.image setImage:self.photoButton.imageView.image];
        
        
        [self performSegueWithIdentifier:@"registerSuccessfulSegue" sender:self];
    }
    else
    {
        UIAlertView * view = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Registration failed, please try again." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
        [view show];
    }
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    UIImage *image = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
    
    if (image){
        
        self.photoButton.imageView.image = image;
        
    }
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

@end