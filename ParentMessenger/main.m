//
//  main.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 10/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"
#import "Flurry.h"


int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
