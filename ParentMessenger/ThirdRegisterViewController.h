//
//  ThirdRegisterViewController.h
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "BaseRegisterViewController.h"

@interface ThirdRegisterViewController : BaseRegisterViewController

@property (nonatomic, strong) IBOutlet UITextField *childCodeField;

@property (nonatomic, strong) IBOutlet UISwitch *rememberMeSwitch;

@property (nonatomic, strong) IBOutlet UILabel *termsLabel;
@property (nonatomic, strong) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong) IBOutlet UILabel *rememberLabel;

@end