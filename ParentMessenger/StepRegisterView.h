//
//  StepRegisterView.h
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface StepRegisterView : UIView

@property (nonatomic, assign) NSInteger step;

@end