//
//  MainTabBarController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 11/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrostedViewController.h"
#import "Model.h"

@interface MainTabBarController : UITabBarController<UITabBarControllerDelegate>

@property (nonatomic, weak) Model* model;

@property (nonatomic, strong) UIBarButtonItem *rightItem;

- (IBAction)showMenu:(id)sender;

- (void)makeNavigationItem;

@end