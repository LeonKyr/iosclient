//
//  PhotoTableViewCell.m
//  ParentMessenger
//
//  Created by lolsi on 08.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "PhotoTableViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation PhotoTableViewCell

@synthesize photoView;
@synthesize separatorLineView;

@synthesize eventTypeView;
@synthesize timeLabel;

@synthesize photoViews;

@synthesize titleLabel;

- (void)awakeFromNib{
    
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = [UIColor whiteColor];

    self.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.titleLabel.textColor = [UIColor blackColor];
    
    self.photoView.backgroundColor = [UIColor clearColor];
//    self.photoView.layer.cornerRadius = self.photoView.frame.size.width/2.0f;
    
    self.timeLabel.textColor = [UIColor colorWithRed:153.0f/255.0f
                                               green:153.0f/255.0f
                                                blue:153.0f/255.0f
                                               alpha:1.0f];
    
    self.timeLabel.font = [UIFont fontWithName:@"Tahoma" size:12.5f];

    self.separatorLineView.backgroundColor = [UIColor colorWithRed:219.0f/255.0f
                                                             green:219.0f/255.0f
                                                              blue:219.0f/255.0f
                                                             alpha:1.0f];
    
    self.teachersLabel.textColor = [UIColor colorWithRed:128.0f/255.0f
                                                   green:128.0f/255.0f
                                                    blue:128.0f/255.0f
                                                   alpha:1.0f];
    self.teachersLabel.font = [UIFont italicSystemFontOfSize:12.5f];
}

- (void)drawRect:(CGRect)rect{
    
    [super drawRect:rect];
    /*
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSaveGState(context);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    NSArray *colors = [NSArray arrayWithObjects:(id)[UIColor colorWithRed:227.0f/255.0f
                                                                    green:227.0f/255.0f
                                                                     blue:228.0f/255.0f
                                                                    alpha:1.0f].CGColor, (id)[UIColor colorWithRed:209.0f/255.0f
                                                                                                             green:209.0f/255.0f
                                                                                                              blue:210.0f/255.0f
                                                                                                             alpha:1.0f].CGColor, nil];
    
    CGGradientRef gradient = CGGradientCreateWithColors(colorSpace, (CFArrayRef)colors, 0);
    
    CGContextDrawLinearGradient(context,
                                gradient,
                                CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMinY(self.bounds)),
                                CGPointMake(CGRectGetMidX(self.bounds), CGRectGetMaxY(self.bounds)),
                                0);
    
    CGColorSpaceRelease(colorSpace);
    CGContextRestoreGState(context);
    */
}


@end