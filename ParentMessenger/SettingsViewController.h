//
//  SettingsViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 17/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsViewController : UITableViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UISwitch *mobileNotification;
@property (weak, nonatomic) IBOutlet UISwitch *emailNotification;

@end
