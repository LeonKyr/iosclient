//
//  CryptoHelper.h
//  ParentMessenger
//
//  Created by Leo on 19/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CryptoHelper : NSObject

+ (NSString*) sha1:(NSString*)input;
+ (NSString*) md5: (NSString*)input;

@end
