//
//  EventDetailViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 10/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "Event.h"

@interface EventDetailViewController : UIViewController

@property (strong, nonatomic) Event * event;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *childImageView;
@property (weak, nonatomic) IBOutlet UIImageView *teacherImageView;
@property (weak, nonatomic) IBOutlet UIImageView *actionTypeImageView;

@property (weak, nonatomic) IBOutlet UIView *container;

- (IBAction)thankYou:(id)sender;
@end
