//
//  MainMenuTabViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 18/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "MainMenuTabViewController.h"

@interface MainMenuTabViewController ()

@end

@implementation MainMenuTabViewController

@synthesize addChildButton;
@synthesize introdiceButton;

- (void)customizeUI{
    
    self.addChildButton.backgroundColor = [UIColor colorWithRed:179.0f/255.0f
                                                          green:179.0f/255.0f
                                                           blue:179.0f/255.0f
                                                          alpha:1.0f];
    [self.addChildButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addChildButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.introdiceButton.backgroundColor = [UIColor colorWithRed:204.0f/255.0f
                                                           green:204.0f/255.0f
                                                            blue:204.0f/255.0f
                                                           alpha:1.0f];
    [self.introdiceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.introdiceButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self customizeUI];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBar.translucent = NO;
}

@end