
#import <UIKit/UIKit.h>
#import "UIViewController+FrostedViewController.h"

typedef NS_ENUM(NSInteger, FrostedViewControllerDirection) {
    FrostedViewControllerDirectionLeft,
    FrostedViewControllerDirectionRight,
    FrostedViewControllerDirectionTop,
    FrostedViewControllerDirectionBottom
};

typedef NS_ENUM(NSInteger, FrostedViewControllerLiveBackgroundStyle) {
    FrostedViewControllerLiveBackgroundStyleLight,
    FrostedViewControllerLiveBackgroundStyleDark
};

@interface FrostedViewController : UIViewController

@property (assign, readwrite, nonatomic) FrostedViewControllerDirection direction;
@property (strong, readwrite, nonatomic) UIColor *blurTintColor;
@property (assign, readwrite, nonatomic) CGFloat blurRadius; // Used only when live blur is off
@property (assign, readwrite, nonatomic) CGFloat blurSaturationDeltaFactor; // Used only when live blur is off
@property (assign, readwrite, nonatomic) NSTimeInterval animationDuration;
@property (assign, readwrite, nonatomic) BOOL limitMenuViewSize;
@property (assign, readwrite, nonatomic) CGSize minimumMenuViewSize;
@property (assign, readwrite, nonatomic) BOOL liveBlur;
@property (assign, readwrite, nonatomic) FrostedViewControllerLiveBackgroundStyle liveBlurBackgroundStyle;

@property (strong, readwrite, nonatomic) UIViewController *contentViewController;
@property (strong, readwrite, nonatomic) UIViewController *menuViewController;

- (id)initWithContentViewController:(UIViewController *)contentViewController menuViewController:(UIViewController *)menuViewController;
- (void)presentMenuViewController;
- (void)hideMenuViewController;
- (void)panGestureRecognized:(UIPanGestureRecognizer *)recognizer;

@end
