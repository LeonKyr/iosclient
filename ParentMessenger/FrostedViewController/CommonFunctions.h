
#import <Foundation/Foundation.h>

#ifndef UIKitIsFlatModeFunction
#define UIKitIsFlatModeFunction
BOOL UIKitIsFlatMode();
#endif