#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface UIView (FrostedViewController)

- (UIImage *)screenshot;

@end
