#import <UIKit/UIKit.h>

@class FrostedViewController;

@interface FrostedContainerViewController : UIViewController

@property (strong, readwrite, nonatomic) UIImage *screenshotImage;
@property (weak, readwrite, nonatomic) FrostedViewController *frostedViewController;
@property (assign, readwrite, nonatomic) BOOL animateApperance;

- (void)panGestureRecognized:(UIPanGestureRecognizer *)recognizer;
- (void)hide;
- (void)refreshBackgroundImage;

@end
