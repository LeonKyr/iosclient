#import <UIKit/UIKit.h>

@class FrostedViewController;

@interface UIViewController (FrostedViewController)

@property (strong, readonly, nonatomic) FrostedViewController *frostedViewController;

- (void)re_displayController:(UIViewController *)controller frame:(CGRect)frame;
- (void)re_hideController:(UIViewController *)controller;

@end
