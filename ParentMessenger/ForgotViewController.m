//
//  ForgotViewController.m
//  ParentMessenger
//
//  Created by lolsi on 01.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <ParentMessengerApi.h>
#import "ForgotViewController.h"
#import <QuartzCore/QuartzCore.h>

#define successTag 100

@interface ForgotViewController ()<UIAlertViewDelegate>

@end

@implementation ForgotViewController

@synthesize forgotView;
@synthesize emailField;
@synthesize sendButton;
@synthesize cancelButton;

@synthesize delegate;

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    textField.layer.borderColor = [UIColor clearColor].CGColor;
    
    return YES;
}

#pragma mark - Customize UI

- (void)customizeUI{
    
    self.forgotView.layer.cornerRadius = 5.0f;
    self.forgotView.backgroundColor = [UIColor colorWithRed:0.2f green:0.2f blue:0.2f alpha:1.0f];
    
    self.emailField.backgroundColor = [UIColor whiteColor];
    self.emailField.layer.cornerRadius = 2.5f;
    self.emailField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.emailField.placeholder = NSLocalizedString(@"Enter email address", nil);
    self.emailField.font = [UIFont fontWithName:@"Helvetica Neue" size:12.5f];
    
    self.emailField.leftView = ({UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 5.0f, self.emailField.frame.size.height)];
        view;
    });
    self.emailField.leftViewMode = UITextFieldViewModeAlways;
    
    self.emailField.keyboardType = UIKeyboardTypeEmailAddress;
    
    self.sendButton.layer.cornerRadius = 2.5f;
    self.sendButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.sendButton.layer.borderWidth = 0.3f;
    
    self.cancelButton.layer.cornerRadius = 2.5f;
    self.cancelButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.cancelButton.layer.borderWidth = 0.3f;
    
    self.sendButton.tintColor = [UIColor whiteColor];
    self.cancelButton.tintColor = [UIColor whiteColor];
    
    self.sendButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12.5f];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:@"Helvetica Neue" size:12.5f];
    
    [self.sendButton setTitle:NSLocalizedString(@"Recover", nil) forState:UIControlStateNormal];
    [self.cancelButton setTitle:NSLocalizedString(@"Cancel", nil) forState:UIControlStateNormal];
    
    self.view.backgroundColor = [UIColor clearColor];
}

#pragma mark - dismiss action

- (void)dismiss{
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(controllerDismiss)])
            [self.delegate performSelector:@selector(controllerDismiss)];
    }];
    
}

#pragma mark - IBAction methods

- (IBAction)send:(id)sender{
    
    NSString *email = self.emailField.text;
    [self.view endEditing:YES];
    
    if (email.length == 0){
        
        self.emailField.layer.borderWidth = 0.666f;
        self.emailField.layer.borderColor = [UIColor redColor].CGColor;
        
    } else {
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            
            ParentMessengerApi* api = [ParentMessengerApi create];
            [api forgotPasswordWithEmail:email andResultCompletion:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                
                if (connectionError){
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", nil)
                                                                    message:[connectionError localizedDescription]
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                                                          otherButtonTitles:nil];
                    [alert show];
                    
                } else {
                    
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Success", nil)
                                                                    message:NSLocalizedString(@"Password send to email", nil)
                                                                   delegate:self
                                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                                          otherButtonTitles:nil];
                    alert.tag = successTag;
                    
                    [alert show];
                    
                }
                
            }];
        });
        
    }
}

- (IBAction)cancel:(id)sender{
    
    [self dismiss];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (alertView.tag == successTag){
        
        [self dismiss];
    }
}

#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];

    [self customizeUI];
}

@end