//
//  AddChildViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"

@interface AddChildViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *childCode;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;

@property (weak, nonatomic) IBOutlet UIButton *addChildButton;
@property (weak, nonatomic) IBOutlet UIButton *introduceButton;

- (IBAction)addChild:(id)sender;
- (IBAction)cancel:(id)sender;

@end