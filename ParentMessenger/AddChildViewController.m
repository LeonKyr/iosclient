//
//  AddChildViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "AddChildViewController.h"
#import "ParentMessengerApi.h"
#import "AppDelegate.h"
#import "SyncEngine.h"

@interface AddChildViewController ()

@end

@implementation AddChildViewController

@synthesize titleLabel;
@synthesize introduceLabel;

@synthesize addChildButton;
@synthesize introduceButton;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                                                        green:178.0f/255.0f
                                                                         blue:178.0f/255.0f
                                                                        alpha:1.0f];
    self.title = NSLocalizedString(@"Add child", nil);
    
    [self.activityIndicator setHidden:YES];

    self.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.titleLabel.textColor = [UIColor colorWithRed:77.0f/255.0f
                                                green:77.0f/255.0f
                                                 blue:77.0f/255.0f
                                                alpha:1.0f];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.numberOfLines = 0;
    
    self.childCode.textColor = [UIColor colorWithRed:204.0f/255.0f
                                               green:204.0f/255.0f
                                                blue:204.0f/255.0f
                                               alpha:1.0f];
    self.childCode.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
    self.childCode.placeholder = @"Code";
    self.childCode.leftView = ({
    
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, self.childCode.frame.size.height)];
        view.backgroundColor = [UIColor clearColor];
        
        view;
        
    });
    
    UIView *viewTop = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.childCode.frame.size.width, 1.0f)];
    viewTop.backgroundColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
    
    UIView *viewButtom = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.childCode.frame.size.height - 1.0f, self.childCode.frame.size.width, 1.0f)];
    viewButtom.backgroundColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
    
    [self.childCode addSubview:viewTop];
    [self.childCode addSubview:viewButtom];
    
    self.childCode.leftViewMode = UITextFieldViewModeAlways;
    
    self.addChildButton.backgroundColor = [UIColor colorWithRed:179.0f/255.0f green:179.0f/255.0f blue:179.0f/255.0f alpha:1.0f];
    [self.addChildButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.addChildButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.introduceButton.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    [self.introduceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.introduceButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.introduceLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.introduceLabel.textColor = [UIColor colorWithRed:77.0f/255.0f
                                                green:77.0f/255.0f
                                                 blue:77.0f/255.0f
                                                alpha:1.0f];
    self.introduceLabel.textAlignment = NSTextAlignmentCenter;
    self.introduceLabel.numberOfLines = 0;
}

- (IBAction)addChild:(id)sender
{
    
    [self.view endEditing:YES];
    
    if ([self.childCode.text isEqualToString:@""])
        return;
    
    [self.addChildButton setEnabled:NO];
    
    [self.activityIndicator startAnimating];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ParentMessengerApi* api = [ParentMessengerApi create];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

        Child* child = [api addChild:self.childCode.text toListener:appDelegate.model.currentListener];
        
        if (child != nil)
        {
            NSLog(@"AddChildViewController::addChild childExternalId=%@", child.externalId);

            [self performSelectorOnMainThread:@selector(addChildWorked:)
                               withObject:child waitUntilDone:NO];
        }
        else
        {
            [self performSelectorOnMainThread:@selector(addChildDidNotWork:)
                                   withObject:child waitUntilDone:NO];
        }
    });
     
    [self dismissViewControllerAnimated:YES completion:nil];
}
                                        
-(void)addChildDidNotWork:(Listener *)listener
{
    [self.activityIndicator stopAnimating];
    
    [self.addChildButton setEnabled:YES];
    
    UIAlertView * view = [[UIAlertView alloc] initWithTitle:@"Error" message:@"The code is not valid, please try again later." delegate:nil cancelButtonTitle:@"Cancel" otherButtonTitles:nil, nil];
    [view show];
}

- (void)addChildWorked:(Child *)child
{
    [self.activityIndicator stopAnimating];
    
    [self.addChildButton setEnabled:YES];
    
    NSLog(@"Added child to listener: %@", child.externalId);
    
    if (child.externalId != nil)
    {
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        [appDelegate.model.currentListener.children setValue:child forKey:child.externalId];
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSLog(@"Before sync");
    [[SyncEngine sharedEngine] startSync];
    NSLog(@"After sync");
}

- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

@end
