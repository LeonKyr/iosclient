//
//  IntroduceKindergartenViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IntroduceKindergartenViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *phoneDcc;
@property (weak, nonatomic) IBOutlet UITextField *addressDcc;
@property (weak, nonatomic) IBOutlet UITextField *websiteDcc;
@property (weak, nonatomic) IBOutlet UITextField *phoneParent;
@property (weak, nonatomic) IBOutlet UITextField *emailDcc;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (weak, nonatomic) IBOutlet UIButton *introduceButton;

- (IBAction)introduce:(id)sender;
- (IBAction)cancel:(id)sender;

@end
