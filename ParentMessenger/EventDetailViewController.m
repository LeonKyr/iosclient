//
//  EventDetailViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 10/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "EventDetailViewController.h"
#import "EventDetail/EventDetailViewControllerProtocol.h"
#import "EventDetail/NoteEventDetailViewController.h"
#import "EventDetail/SignInOutEventDetailViewController.h"
#import "EventDetail/MealEventDetailViewController.h"
#import "EventDetail/ChildWillHaveMealEventDetailViewController.h"
#import "EventDetail/ChildAbsenceEventDetailViewController.h"
#import "EventDetail/SleepEventDetailViewController.h"
#import "ActionType.h"
#import "ActionTypeConverter.h"
#import "ParentMessengerApi.h"
#import "UIImageView+AFNetworking.h"

@interface EventDetailViewController ()


@end

@implementation EventDetailViewController

UIBarButtonItem *prevBarButton;
UIBarButtonItem *nextBarButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)makeNavigationButton
{
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(close:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                                                        green:178.0f/255.0f
                                                                         blue:178.0f/255.0f
                                                                        alpha:1.0f];
	// Do any additional setup after loading the view.
    
    // add navigation buttons
//    prevBarButton = [[UIBarButtonItem alloc] initWithTitle:@"Prev >" style:UIBarButtonItemStyleBordered target:self action:@selector(prevBarButtonPushed:)];
//    
//    nextBarButton = [[UIBarButtonItem alloc] initWithTitle:@"< Next" style:UIBarButtonItemStyleBordered target:self action:@selector(nextBarButtonPushed:)];
    
    if (self.event)
    {
        NSLog(@"Event Details id = %@", [self.event valueForKey:@"id"]);
        UIViewController *vc = [self getViewControllerForEvent: self.event];

        [((id<EventDetailViewControllerProtocol>)vc) setEvent: (Event*)self.event];
        [vc.view setFrame: self.container.bounds];
        [vc willMoveToParentViewController:self];
        [self.container addSubview:vc.view];
        [self addChildViewController:vc];
        [vc didMoveToParentViewController:self];

//        [self.titleLabel setText: self.event.getTitle];
        [self.titleLabel setText: [self.event propertyValueByName:@"title"]];
        [self.timeLabel setText: [self.event createdAt]];
        
//        [prevBarButton setEnabled:self.event.prev != nil];
//        [nextBarButton setEnabled:self.event.next != nil];

        self.actionTypeImageView.image = [UIImage imageNamed:[ActionTypeConverter eventTypeStringForInt:[self.event actionType].integerValue]];
        
        NSString *image_id = self.event.child_ext_image;
        
        NSURL *url = [NSURL URLWithString:[[[ParentMessengerApi create].baseUrl stringByAppendingPathComponent:@"image"] stringByAppendingPathComponent:image_id]];
        
        [self setImageToImageView:self.childImageView forUrl:url];
    }
}

- (void)setImageToImageView:(UIImageView*)imageView forUrl:(NSURL*)url{
    
    __weak UIImageView *img = imageView;
    
    [imageView setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:url]
                     placeholderImage:[UIImage imageNamed:@"no-available-image"]
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                  
                                  if (request) {
                                      
                                      [UIView transitionWithView:img
                                                        duration:0.75f
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{[img setImage:image];}
                                                      completion:NULL];
                                      
                                  }
                                  
                              }
                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                  
                                  img.image = [UIImage imageNamed:@"no-available-image"];
                                  
                              }];
}

-(void)viewWillAppear:(BOOL)animated
{
//    self.parentViewController.navigationController.navigationBarHidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated
{
//    self.parentViewController.navigationController.navigationBarHidden = NO;
}

-(void)swipeRightHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received (Right).");
    [self showNextEvent];
}

-(void)showPreviousEvent
{
    if (self.event != nil &&
        self.event.prev != nil)
    {
        NSLog(@"Event %@ has prevEvent %@", self.event.externalId, self.event.prev);
        [self setEvent: self.event.prev];

        [self viewDidLoad];
    }
}

-(void)showNextEvent
{
    if (self.event != nil &&
        self.event.next != nil)
    {
        NSLog(@"Event %@ has next %@", self.event.externalId, self.event.next);
        [self setEvent: self.event.next];
        [self viewDidLoad];
    }
}

-(void)swipeLeftHandler:(UISwipeGestureRecognizer *)recognizer {
    NSLog(@"Swipe received (Left).");
    
    [self showPreviousEvent];
}

-(void)animateSwipe:(UISwipeGestureRecognizer *)recognizer
{
    // Get the location of the gesture
    CGPoint location = [recognizer locationInView:self.view];
    CGPoint defaultLocation = self.view.center;
    [UIView animateWithDuration:0.5 animations:^{
        self.view.alpha = 0.5;
        self.view.center = location;
    }
                     completion:^(BOOL completed){
                         if (completed)
                         {
                             self.view.alpha = 1;
                             self.view.center = defaultLocation;
                         }
                         
                     }];
}

-(IBAction)prevBarButtonPushed:(id)sender{
    // toggle hidden state for navButton
//    [navButton setHidden:!nav.hidden];
    [self showPreviousEvent];
}

-(IBAction)nextBarButtonPushed:(id)sender{
    // toggle hidden state for navButton
//    [navButton setHidden:!nav.hidden];
    [self showNextEvent];
}

- (UIViewController*)getViewControllerForEvent: (Event*)event
{
    UIViewController *vc = nil;
    ActionType actionType = [event.actionType intValue];
    NSLog(@"event.actionType=%d", actionType);
    // TODO: make factory or reflection usage
    if (actionType == NOTE)
    {
        vc = [[NoteEventDetailViewController alloc]
              initWithNibName:NoteEventDetailViewController.getViewName bundle:nil];
    } else if (actionType == SINGIN || actionType == SINGOUT)
    {
        vc = [[SignInOutEventDetailViewController alloc]
              initWithNibName:SignInOutEventDetailViewController.getViewName bundle:nil];
    } else if (actionType == MEAL)
    {
        vc = [[MealEventDetailViewController alloc]
              initWithNibName:MealEventDetailViewController.getViewName bundle:nil];
    } else if (actionType == CHILD_WILL_HAVE_MEAL)
    {
        vc = [[ChildWillHaveMealEventDetailViewController alloc]
              initWithNibName:ChildWillHaveMealEventDetailViewController.getViewName bundle:nil];
    } else if (actionType == CHILD_ABSENCE)
    {
        vc = [[ChildAbsenceEventDetailViewController alloc]
              initWithNibName:ChildAbsenceEventDetailViewController.getViewName bundle:nil];
    } else if (actionType == SLEEP)
    {
        vc = [[SleepEventDetailViewController alloc]
              initWithNibName:SleepEventDetailViewController.getViewName bundle:nil];
    }
    NSLog(@"event.actionType=%@", vc);
    
    if (vc == nil)
    {
        [NSException raise:@"View was not mapped for ActionType." format:@"ActionType is %d ", actionType];
    }
    return vc;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)thankYou:(id)sender
{
    [[self navigationController] popViewControllerAnimated:YES];
}

@end
