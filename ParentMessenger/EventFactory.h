//
//  EventFactory.h
//  ParentMessenger
//
//  Created by Leo on 01/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventDto.h"

@interface EventFactory : NSObject

-(EventDto*)createWithAction:(NSString*) action andEventId:(NSString*)eventId;

@end
