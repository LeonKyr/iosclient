//
//  AppDelegate.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 10/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#define kPushTokenTransmitted @"PushNitificationAccepted"

#import <CoreData/CoreData.h>
#import "AppDelegate.h"
#import "Flurry.h"
#import "API/Child.h"
#import "API/ParentMessengerApi.h"
#import "SyncEngine.h"

@implementation AppDelegate

@synthesize model;
@synthesize window;

- (void)loadInstantiateStoryBoard{

    id viewController = nil;
    
    UIStoryboard *storyBoard = ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone)?[UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil]:[UIStoryboard storyboardWithName:@"Main_iPad" bundle:nil];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone){
        
        if ([[UIScreen mainScreen] bounds].size.height == 568)
            viewController = [storyBoard instantiateViewControllerWithIdentifier:@"start4"];
        else
            viewController = [storyBoard instantiateViewControllerWithIdentifier:@"start3_5"];

    } else {
        
        viewController = [storyBoard instantiateInitialViewController];
    }
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    self.window.rootViewController = viewController;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
//    [Crashlytics startWithAPIKey:@"aac65c96b2321630d76d77aa4137fba53b846dd0"];

    
//    NSSetUncaughtExceptionHandler(&myExceptionHandler);

    // flurry
    [Flurry setCrashReportingEnabled:YES];
    //note: iOS only allows one crash reporting tool per app; if using another, set to: NO
    [Flurry startSession:@"C2XH3G76G4MFR4V622J4"];
    
    // Override point for customization after application launch.
    UIPageControl *pageControl = [UIPageControl appearance];
    pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
    pageControl.currentPageIndicatorTintColor = [UIColor blackColor];
    pageControl.backgroundColor = [UIColor whiteColor];
    
    // appearance
//    NSShadow *shadow = [[NSShadow alloc] init];
//    shadow.shadowColor = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.8];
//    shadow.shadowOffset = CGSizeMake(0, 1);
//
//    [[UINavigationBar appearance] setTintColor:[UIColor yellowColor]];
//    [[UINavigationBar appearance] setTitleTextAttributes: [NSDictionary dictionaryWithObjectsAndKeys:
//                                                           [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:245.0/255.0 alpha:1.0], NSForegroundColorAttributeName,
//                                                           shadow, NSShadowAttributeName,
//                                                           [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:21.0], NSFontAttributeName, nil]];
//    
//	[UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;

    // Create the navigation and view controllers
//    UINavigationController *navController = (UINavigationController *)self.window.rootViewController;
//    EventsTableViewController *rootViewController = (EventsTableViewController *)navController.topViewController;
//
//	[rootViewController setRegions:[APLRegion knownRegions]];
    
    
    [self createModel];
    
    [self requestPermissionsForPushNotification];
    
    // determine if app launched from a push notification
    NSDictionary *pushNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];

    [self handlePushNotificationWithOptions: pushNotification];
    
    [self loadInstantiateStoryBoard];
    
    return YES;
}

- (void)createModel
{
    self.model = [[Model alloc] init];
    NSUUID* deviceId = [[UIDevice currentDevice] identifierForVendor];
    [self.model setDeviceId: [deviceId UUIDString]];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSData *encodedObject = [defaults objectForKey:@"listener"];
    self.model.currentListener = [NSKeyedUnarchiver unarchiveObjectWithData:encodedObject];
}

- (void)requestPermissionsForPushNotification
{
        NSLog(@"requestPermissionsForPushNotification");
    // request permission to deliver remote notifications, if needed
//    BOOL requested = [[NSUserDefaults standardUserDefaults] boolForKey:kPushTokenTransmitted];
    
//    if (requested != YES)
    {
        [[UIApplication sharedApplication]
            registerForRemoteNotificationTypes: (
                                                 UIRemoteNotificationTypeAlert
                                                 | UIRemoteNotificationTypeBadge
                                                 | UIRemoteNotificationTypeSound)];
    }
}


-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    NSLog(@"RECEIVED PUSH");
    
    [self handlePushNotificationWithOptions:userInfo];
}
    
- (void) handlePushNotificationWithOptions:(NSDictionary *)options
{
    if (options != nil)
    {
        NSString *action = [[options objectForKey:@"aps"]
                             objectForKey:@"alert"];
        
        NSLog(@"Push notification received %@", action);

        [self sync];
        
        [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    }
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    [FBAppEvents activateApp];
    
    // We need to properly handle activation of the application with regards to SSO
    //  (e.g., returning from iOS 6.0 authorization dialog or from fast app switching).
    [FBAppCall handleDidBecomeActiveWithSession:self.session];
    
    [self sync];
    
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
}

- (void)sync
{
    if (self.model != nil &&
        self.model.currentListener != nil)
    {
        NSLog(@"Before sync");
        [[SyncEngine sharedEngine] startSync];
        NSLog(@"After sync");
    }
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
    // FBSample logic
    // if the app is going away, we close the session if it is open
    // this is a good idea because things may be hanging off the session, that need
    // releasing (completion block, etc.) and other components in the app may be awaiting
    // close notification in order to do cleanup
    [self.session close];

}

- (void)application:(UIApplication *)application
    didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // hardcode the current user, this would typically
    // be a token or value retrieved after they logged
    // in to use the app
    NSLog(@"My token is: %@", deviceToken);
    
//    NSString *userId = @"nate@emaildomain.com";
    NSString *token = [NSString stringWithFormat:@"%@", deviceToken];
    
    // clean the token
    token = [token stringByTrimmingCharactersInSet:
             [NSCharacterSet characterSetWithCharactersInString:@"<>"]]; token = [token stringByReplacingOccurrencesOfString:@" "
                                                   withString:@""];
    
    NSString* deviceId = self.model.deviceId;
    NSLog(@"DeviceId=%@", deviceId);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ParentMessengerApi* api = [ParentMessengerApi create];
        
        [api registerDevice:deviceId forPushNotificationWithToken:token isEnabled:YES];
    });

}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
	NSLog(@"Failed to get token, error: %@", error);
}

void myExceptionHandler(NSException *exception)
{
    NSLog(@"%@",exception.name);
    NSLog(@"%@",exception.reason);
}

// ---------- FACEBOOK -----------
// FBSample logic
// The native facebook application transitions back to an authenticating application when the user
// chooses to either log in, or cancel. The url passed to this method contains the token in the
// case of a successful login. By passing the url to the handleOpenURL method of FBAppCall the provided
// session object can parse the URL, and capture the token for use by the rest of the authenticating
// application; the return value of handleOpenURL indicates whether or not the URL was handled by the
// session object, and does not reflect whether or not the login was successful; the session object's
// state, as well as its arguments passed to the state completion handler indicate whether the login
// was successful; note that if the session is nil or closed when handleOpenURL is called, the expression
// will be boolean NO, meaning the URL was not handled by the authenticating application
- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    NSLog(@"application openes url %@", url);
    // attempt to extract a token from the url
    return [FBAppCall handleOpenURL:url
                  sourceApplication:sourceApplication
                        withSession:self.session];
}


@end
