//
//  TutorialPageContainerViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 12/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "DDPageControl.h"

@interface TutorialPageContainerViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, weak) IBOutlet DDPageControl *pageControl;

@property (nonatomic, strong) IBOutlet UIButton *loginButton;
@property (nonatomic, strong) IBOutlet UIButton *registerButton;
@property (nonatomic, strong) IBOutlet UIScrollView *scrollView;

@property (nonatomic, strong) NSArray *imageData;

@end