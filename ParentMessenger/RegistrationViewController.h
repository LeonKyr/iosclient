//
//  RegistrationViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 11/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "Model.h"
#import <UIKit/UIKit.h>
#import "SevenSwitch.h"

@interface RegistrationViewController : UIViewController<UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITextField *emailField;
@property (weak, nonatomic) IBOutlet UITextField *childField;
@property (weak, nonatomic) IBOutlet UITextField *passwordField;

@property (weak, nonatomic) IBOutlet UITextField *firstNameField;
@property (weak, nonatomic) IBOutlet UITextField *secondNameField;

@property (weak, nonatomic) IBOutlet UIButton *photoButton;

@property (weak, nonatomic) IBOutlet UILabel *termsLabel;

@property (weak, nonatomic) IBOutlet UIButton *termsButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *registrationButton;

@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UIView *userView;

@property (weak, nonatomic) IBOutlet SevenSwitch *switch_my;
@property (weak, nonatomic) IBOutlet UILabel *remember_label;

- (IBAction)termsSet:(id)sender;
- (IBAction)selectPhoto:(id)sender;
- (IBAction)registration:(id)sender;

@end