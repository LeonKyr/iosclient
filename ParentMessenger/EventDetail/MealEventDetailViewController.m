//
//  MealEventDetailViewController.m
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "MealEventDetailViewController.h"
#import "MainTabBarController.h"

@interface MealEventDetailViewController ()

@end

@implementation MealEventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makeNavigationButton];
    
    if (self.event)
    {
        NSString* mq = [self.event propertyValueByName:@"meal-quality"];
        NSLog(@"meal-quality=%@", mq);
        
        NSString* mt = [self.event propertyValueByName:@"meal-type"];
        NSLog(@"meal-type=%@", mt);
        
        NSMutableAttributedString *stringAttributed = [[NSMutableAttributedString alloc] init];
        
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Meal Quality", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:[self convertMealQuality:[mq integerValue]]]];
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Meal Type", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:[self convertMealType:[mt integerValue]]]];
        
        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineSpacing = 10.0f;
        
        NSRange range = [[stringAttributed string] rangeOfString:[stringAttributed string]];
        
        [stringAttributed addAttribute:NSParagraphStyleAttributeName value:paragraph range:range];
        /////
        self.textView.attributedText = stringAttributed;
    }
}

- (void)makeNavigationButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(back:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.tabBarController.tabBar.hidden = YES;

}

- (NSString*)convertMealType:(NSInteger)value
{
    NSString* result;
    switch (value) {
        case 1:
            result = NSLocalizedString(@"Breakfast", nil);
            break;
        case 2:
            result = NSLocalizedString(@"Second Breakfast", nil);
            break;
        case 3:
            result = NSLocalizedString(@"Lunch", nil);
            break;
        case 4:
            result = NSLocalizedString(@"Afternoon Snack", nil);
            break;
        case 5:
            result = NSLocalizedString(@"Dinner", nil);
            break;
        case 6:
            result = NSLocalizedString(@"Ocassional Meal", nil);
            break;
        default:
            result = @"";
            break;
    }
    
    return result;
}


- (NSString*)convertMealQuality:(NSInteger)value
{
    NSString* result;
    switch (value) {
        case 1:
            result = NSLocalizedString(@"Ate everything", nil);
            break;
        case 2:
            result = NSLocalizedString(@"Are most of the meal", nil);
            break;
        case 3:
            result = NSLocalizedString(@"Ate part of the meal", nil);
            break;
        case 4:
            result = NSLocalizedString(@"Are small part of the meal", nil);
            break;
        case 5:
            result = NSLocalizedString(@"Ate nothing", nil);
            break;
        default:
            result = NSLocalizedString(@"", nil);
            break;
    }
    
    return result;
}

- (IBAction)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(NSString*)getViewName
{
    return @"MealEventDetailViewController";
}

@end
