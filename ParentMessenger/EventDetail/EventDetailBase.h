//
//  EventDetailBase.h
//  BooGooGoo
//
//  Created by Leo on 01/08/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "EventDetailViewControllerProtocol.h"

@interface EventDetailBase : UIViewController<EventDetailViewControllerProtocol>

- (NSAttributedString*)stringForTitleString:(NSString*)string;
- (NSAttributedString*)stringForValue:(NSString*)string;

@end
