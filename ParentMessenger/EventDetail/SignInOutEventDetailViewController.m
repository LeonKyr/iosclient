//
//  SignInOutEventDetailViewController.m
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "SignInOutEventDetailViewController.h"

@interface SignInOutEventDetailViewController ()

@end

@implementation SignInOutEventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    if (self.event)
    {
        NSString* timeAndDate = [self.event propertyValueByName:@"time-and-date"];
        NSLog(@"ShowThetimeAndDate=%@", timeAndDate);
        
        NSTimeInterval epoch = [timeAndDate doubleValue]/1000;
        NSDate * date = [NSDate dateWithTimeIntervalSince1970:epoch];
        
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        
        
        NSMutableAttributedString *stringAttributed = [[NSMutableAttributedString alloc] init];
        
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Time", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:[formatter stringFromDate:date]]];
        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineSpacing = 10.0f;
        
        NSRange range = [[stringAttributed string] rangeOfString:[stringAttributed string]];
        
        [stringAttributed addAttribute:NSParagraphStyleAttributeName value:paragraph range:range];
        /////
        self.textView.attributedText = stringAttributed;
    }
    [self makeNavigationButton];
}

- (void)makeNavigationButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(back:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (IBAction)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(NSString*)getViewName
{
    return @"SignInOutEventDetailViewController";
}

@end
