//
//  ChildAbsenceEventDetailViewController.m
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "ChildAbsenceEventDetailViewController.h"

@interface ChildAbsenceEventDetailViewController ()

@end

@implementation ChildAbsenceEventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makeNavigationButton];
    
    if (self.event)
    {
        NSString* absenceReason = [self.event propertyValueByName:@"absence-reason"];
        NSLog(@"absence-reason=%@", absenceReason);
        
        NSString* message = [self.event propertyValueByName:@"message"];
        NSLog(@"message=%@", message);
        
        NSString* td = [self.event propertyValueByName:@"time-and-date"];
        NSLog(@"time-and-date=%@", td);
        
        NSMutableAttributedString *stringAttributed = [[NSMutableAttributedString alloc] init];
        
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Absence Reason", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:[self convertAbsebceReason:[absenceReason integerValue]]]];
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Message", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:message]];
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Time", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:td]];

        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineSpacing = 10.0f;
        
        NSRange range = [[stringAttributed string] rangeOfString:[stringAttributed string]];
        
        [stringAttributed addAttribute:NSParagraphStyleAttributeName value:paragraph range:range];
        /////
        self.textView.attributedText = stringAttributed;

    }
}

- (void)makeNavigationButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(back:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (IBAction)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(NSString*)getViewName
{
    return @"ChildAbsenceEventDetailViewController";
}

- (NSString*)convertAbsebceReason:(NSInteger)value
{
    NSString* result;
    switch (value) {
        case 1:
            result = NSLocalizedString(@"Sickness", nil);
            break;
        case 2:
            result = NSLocalizedString(@"Reasonable Excuse", nil);
            break;
        case 3:
            result = NSLocalizedString(@"Unreasonable Excuse", nil);
            break;
        default:
            result = @"";
            break;
    }
    
    return result;
}

@end
