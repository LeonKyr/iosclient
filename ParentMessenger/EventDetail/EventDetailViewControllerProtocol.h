//
//  EventDetailViewControllerProtocol.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#ifndef ParentMessenger_EventDetailViewControllerProtocol_h
#define ParentMessenger_EventDetailViewControllerProtocol_h

#import "Event.h"

@protocol EventDetailViewControllerProtocol

-(void)setEvent: (Event *) event;
+(NSString*)getViewName;

@end

#endif
