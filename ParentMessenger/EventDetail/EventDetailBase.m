//
//  EventDetailBase.m
//  BooGooGoo
//
//  Created by Leo on 01/08/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "EventDetailBase.h"

@implementation EventDetailBase

- (NSAttributedString*)stringForTitleString:(NSString*)string{
    
    NSDictionary *dict = @{NSForegroundColorAttributeName: [UIColor blackColor], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:17.0f]};
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:[string stringByAppendingString:@"\n"]attributes:dict];
    
    return str;
}

- (NSAttributedString*)stringForValue:(NSString*)string{
    
    NSDictionary *dict = @{NSForegroundColorAttributeName: [UIColor colorWithRed:100.0f/255.0f
                                                                           green:100.0f/255.0f
                                                                            blue:100.0f/255.0f
                                                                           alpha:1.0f], NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue" size:15.0f]};
    
    NSAttributedString *str = [[NSAttributedString alloc] initWithString:[string stringByAppendingString:@"\n"]attributes:dict];
    
    return str;
}

@end
