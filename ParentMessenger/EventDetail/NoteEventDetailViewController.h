//
//  NoteEventDetailViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailViewControllerProtocol.h"
#import "NoteEvent.h"

@interface NoteEventDetailViewController : UIViewController<EventDetailViewControllerProtocol>

@property (weak, nonatomic) IBOutlet UITextView *message;

@property (nonatomic, strong) Event * event;

+(NSString*)getViewName;

@end
