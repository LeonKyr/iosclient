//
//  EventsTableViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "Event.h"

@interface EventsTableViewController : UITableViewController

@property (nonatomic, retain) NSFetchedResultsController *fetchedResultsController;

@property (nonatomic, strong) NSArray *childWithEvents;

@end