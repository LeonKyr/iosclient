//
//  NoteEventDetailViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "NoteEventDetailViewController.h"
#import "NoteEvent.h"

@implementation NoteEventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

+(NSString*)getViewName
{
    return @"NoteEventDetailViewController";
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (self.event)
    {
        NSString* message = [self.event propertyValueByName:@"message"];
        NSLog(@"ShowTheMessage=%@", message);
        
        self.message.text = message;
    }
        
    [self makeNavigationButton];
}

- (void)makeNavigationButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(back:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}


- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.tabBarController.tabBar.hidden = YES;
}

- (IBAction)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

@end
