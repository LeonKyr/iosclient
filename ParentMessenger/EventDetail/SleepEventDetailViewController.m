//
//  SleepEventDetailViewController.m
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "SleepEventDetailViewController.h"
#import "MainTabBarController.h"
@interface SleepEventDetailViewController ()

@end

@implementation SleepEventDetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self makeNavigationButton];
    
    self.textView.contentInset = UIEdgeInsetsMake(-5.0f, -5.0f, 0.0f, 0.0f);
}

- (void)makeNavigationButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(back:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (self.event)
    {
        NSString* message = [self.event propertyValueByName:@"message"];
        NSLog(@"Message=%@", message);
        
        NSString* sd = [self.event propertyValueByName:@"sleep-duration"];
        NSLog(@"sleep-duration=%@", sd);
        
        NSString* sq = [self.event propertyValueByName:@"sleep-quality"];
        NSLog(@"sleep-quality=%@", sq);
        
        
        NSMutableAttributedString *stringAttributed = [[NSMutableAttributedString alloc] init];
        
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Message", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:message]];
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Sleep Duration", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:[self convertSleepDuration:[sd integerValue]]]];
        [stringAttributed appendAttributedString:[self stringForTitleString:NSLocalizedString(@"Sleep Quality", nil)]];
        [stringAttributed appendAttributedString:[self stringForValue:[self convertSleepQuality:[sq integerValue]]]];
        
        
        NSMutableParagraphStyle *paragraph = [[NSMutableParagraphStyle alloc] init];
        paragraph.lineSpacing = 10.0f;
        
        NSRange range = [[stringAttributed string] rangeOfString:[stringAttributed string]];
        
        [stringAttributed addAttribute:NSParagraphStyleAttributeName value:paragraph range:range];
        /////
        self.textView.attributedText = stringAttributed;
    }
    
    self.navigationController.tabBarController.tabBar.hidden = YES;

}

- (NSString*)convertSleepDuration:(NSInteger)sleepDuration
{
    NSString* result;
    switch (sleepDuration) {
        case 1:
            result = NSLocalizedString(@"30 mins", nil);
            break;
        case 2:
            result = NSLocalizedString(@"45 mins", nil);
            break;
        case 3:
            result = NSLocalizedString(@"1 hour", nil);
            break;
        case 4:
            result = NSLocalizedString(@"1h 15 mins", nil);
            break;
        case 5:
            result = NSLocalizedString(@"1h 30 mins", nil);
            break;
        case 6:
            result = NSLocalizedString(@"1h 45 mins", nil);
            break;
        case 7:
            result = NSLocalizedString(@"2h", nil);
            break;
        case 8:
            result = NSLocalizedString(@"2h 15 mins", nil);
            break;
        case 9:
            result = NSLocalizedString(@"2h 30 mins", nil);
            break;
        default:
            result = @"";
            break;
    }
    
    return result;
}

- (NSString*)convertSleepQuality:(NSInteger)sleepQuality
{
    NSString* result;
    switch (sleepQuality) {
        case 1:
            result = NSLocalizedString(@"Healthy Restful Sleep without Interruptions", nil);
            break;
        case 2:
            result = NSLocalizedString(@"Healthy Restful Sleep with Interruptions", nil);
            break;
        case 3:
            result = NSLocalizedString(@"Restless sleep", nil);
            break;
        case 4:
            result = NSLocalizedString(@"Periodic Sleep with long periods of Wakefulness", nil);
            break;
        case 5:
            result = NSLocalizedString(@"No Sleep", nil);
            break;
        default:
            result = @"";
            break;
    }
    
    return result;
}

- (IBAction)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

+(NSString*)getViewName
{
    return @"SleepEventDetailViewController";
}

@end
