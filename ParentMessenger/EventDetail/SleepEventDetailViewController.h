//
//  SleepEventDetailViewController.h
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EventDetailViewControllerProtocol.h"
#import "Event.h"
#import "EventDetailBase.h"

@interface SleepEventDetailViewController : EventDetailBase

@property (nonatomic, strong) Event * event;

@property (nonatomic, weak) IBOutlet UITextView *textView;

+(NSString*)getViewName;


@end
