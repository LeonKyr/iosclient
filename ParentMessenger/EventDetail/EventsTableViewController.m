//
//  EventsTableViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <CoreData/CoreData.h>
#import <QuartzCore/QuartzCore.h>
#import "EventsTableViewController.h"
#import "EventDetailViewController.h"
#import "ParentMessengerApi.h"
#import "Listener.h"
#import "ChildWithEvents.h"
#import "CoreDataController.h"
#import "AppDelegate.h"
#import "SyncEngine.h"
#import "ActionType.h"
#import "ImageDb.h"
#import "SVPullToRefresh.h"
#import "MainTableViewCell.h"
#import "PhotoTableViewCell.h"
#import "UIImageView+AFNetworking.h"
#import "ActionTypeConverter.h"

#import "DetailCollectionViewController.h"

#import "MainTabBarController.h"

#import "LearningINViewController.h"

@interface EventsTableViewController ()<NSFetchedResultsControllerDelegate>{
    
    NSMutableArray * childIds;
}

@end

@implementation EventsTableViewController

@synthesize fetchedResultsController = _fetchedResultsController;

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setHidden:NO];
    self.navigationController.tabBarController.tabBar.hidden = NO;
    
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
}

- (void)refresh{

    NSLog(@"Before sync");
    [[SyncEngine sharedEngine] startSync];
    NSLog(@"After sync");
}

- (void)ifRegisterShowTutorial{
    
    NSUserDefaults *standartDefaults = [NSUserDefaults standardUserDefaults];
    
    if ([standartDefaults boolForKey:@"newUserRegister"]){
        
        LearningINViewController *vc = nil;
        
        if ([[UIScreen mainScreen] bounds].size.height == 568)
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialController4"];
        else
            vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialController3_5"];
        
        vc.isPushed = NO;
        
        [standartDefaults setBool:NO forKey:@"newUserRegister"];
        [standartDefaults synchronize];
        
        UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
        nc.navigationBar.barStyle = UIBarStyleDefault;
        nc.navigationBar.translucent = NO;
        nc.navigationBar.barTintColor = [UIColor whiteColor];
        nc.navigationBarHidden = YES;
        
        [self presentViewController:nc animated:YES completion:nil];
        
    }
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self ifRegisterShowTutorial];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self.tableView addPullToRefreshWithActionHandler:^{
        
        NSLog(@"Before sync");
        [[SyncEngine sharedEngine] startSync];
        NSLog(@"After sync");
        
    } position:SVPullToRefreshPositionTop];
    
    self.fetchedResultsController = nil;
    
    [self fetchEvents];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(syncCompleted:)
                                                 name:@"SyncEngineSyncCompleted"
                                               object:nil];

    [self.tableView reloadData];
    
    self.tableView.tableFooterView = nil;
}

- (void)fetchEvents{
    
    NSLog(@"fetchEvents called");
    
    NSError *error;
	if (![[self fetchedResultsController] performFetch:&error]) {
		// Update to handle the error appropriately.
		NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
		exit(-1);  // Fail
	}
}

#pragma mark - Table view data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    NSLog(@"Number of sections=%lu",(unsigned long)[[self.fetchedResultsController sections] count]);
	return [[self.fetchedResultsController sections] count];
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    id sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    NSUInteger numberOfObjects = [sectionInfo numberOfObjects];
    NSLog(@"Section %ld has numberOfRowsInSection = %lu", (long)section, (unsigned long)numberOfObjects);
    return numberOfObjects;
}

- (NSFetchedResultsController *)fetchedResultsController {
    NSLog(@"fetchedResultsController called");
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSLog(@"fetchedResultsController - 2 ");
    
    self.fetchedResultsController = [self createNSFetchedResultsController];
    _fetchedResultsController.delegate = self;
    
    NSLog(@"Called fetchedResultsController");
    
    return _fetchedResultsController;
}

- (NSFetchedResultsController*)createNSFetchedResultsController{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSManagedObjectContext *managedObjectContext = [[CoreDataController sharedInstance] masterManagedObjectContext];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Event" inManagedObjectContext:managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSSortDescriptor *sortByCreatedAt = [[NSSortDescriptor alloc]
                              initWithKey:@"createdAt" ascending:NO];

    [fetchRequest setSortDescriptors:[NSArray arrayWithObject:sortByCreatedAt]];

    [fetchRequest setFetchBatchSize:20];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"listener_id == %@", appDelegate.model.currentListener.externalId]];
    
    NSLog(@"appDelegate.model.currentListener.children count=%lu", (unsigned long)[appDelegate.model.currentListener.children count]);
    
    childIds = [[NSMutableArray alloc] init];
    
    for (Child * child in appDelegate.model.currentListener.children){

        NSLog(@"Checking for child %@", child.externalId);
        [childIds addObject:child];
    }

    NSFetchedResultsController *theFetchedResultsController =
    [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest
                                        managedObjectContext:managedObjectContext
                                          sectionNameKeyPath:@"day"
                                                   cacheName:nil];
    
    return theFetchedResultsController;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
   
    UILabel * sectionHeader = [[UILabel alloc] initWithFrame:CGRectZero];
    sectionHeader.textAlignment = NSTextAlignmentCenter;
    
    sectionHeader.backgroundColor = [UIColor colorWithRed:219.0/255.0
                                                    green:219.0/255.0
                                                     blue:219.0/255.0
                                                    alpha:1];
    
    sectionHeader.font = [UIFont fontWithName:@"Tahoma" size:10.0f];
    sectionHeader.textColor = [UIColor blackColor];
    id sectionInfo = [[_fetchedResultsController sections] objectAtIndex:section];
    sectionHeader.text = [sectionInfo name];
    
    return sectionHeader;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    
    Event * event = (Event *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *cellIdentifier = [NSString stringWithFormat:@"Cell%@identifier", [ActionTypeConverter eventTypeStringForInt:[event actionType].integerValue]];
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if ([event.actionType integerValue] == PHOTOS){
        
        if (!cell){
            
            [tableView registerNib:[UINib nibWithNibName:NSStringFromClass([PhotoTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
        
    } else {
        
        if (!cell){
            
            [tableView registerNib:[UINib nibWithNibName:NSStringFromClass([MainTableViewCell class]) bundle:nil] forCellReuseIdentifier:cellIdentifier];
            cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
        }
    }
    
    return cell;
    
}

- (void)setImageToImageView:(UIImageView*)imageView forUrl:(NSURL*)url{
    
    __weak UIImageView *img = imageView;
    
    [imageView setImageWithURLRequest:[[NSURLRequest alloc] initWithURL:url]
                     placeholderImage:[UIImage imageNamed:@"no-available-image"]
                              success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
                                
                                  if (request) {
                                      
                                      [UIView transitionWithView:img
                                                        duration:0.25f
                                                         options:UIViewAnimationOptionTransitionCrossDissolve
                                                      animations:^{[img setImage:image];}
                                                      completion:NULL];
                                      
                                  }
                                  
                              }
                              failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error){
                                  
                                  img.image = [UIImage imageNamed:@"no-available-image"];
                                  
                              }];
}

- (void)printForEvent:(Event*)event andString:(NSString*)str{
    
    NSLog(@"for event %@", str);
    
    for (EventPropertyValue * epv in event.event2EventProperty){
        
        NSLog(@"type %@ value %@", epv.type, epv.value);
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Event * event = (Event *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    NSString *image_id = event.child_ext_image;
    
    NSURL *url = [NSURL URLWithString:[[[ParentMessengerApi create].baseUrl stringByAppendingPathComponent:@"image"] stringByAppendingPathComponent:image_id]];
    
    if ([event.actionType integerValue] == PHOTOS){
        
        PhotoTableViewCell *cell_ = (PhotoTableViewCell*)cell;
        
        [self setImageToImageView:cell_.photoView forUrl:url];
                
        cell_.eventTypeView.image = [UIImage imageNamed:[ActionTypeConverter eventTypeStringForInt:[event actionType].integerValue]];
        cell_.timeLabel.text = [event createdAtAsString];
        
        NSLog(@"---->");
        cell_.titleLabel.hidden = true;
        
        for (EventPropertyValue * epv in event.event2EventProperty){
            
            NSInteger index = 0;
            
            NSLog(@"Name=%@, Type=%@, Value=%@", epv.name, epv.type, epv.value);
            if ([epv.name isEqualToString:@"message"])
            {
                cell_.titleLabel.text = epv.value;
                continue;
            }
            else if (    [epv.name isEqualToString:@"title"])
            {
                cell_.titleLabel.text = epv.value;
                continue;
            }
            
            for (int i = 0; i < [cell_.photoViews count]; i++){
                
                UIImageView *imageView = [cell_.photoViews objectAtIndex:i];
                imageView.image = nil;
                imageView.hidden = YES;
            }
                
            
            for (EventPropertyValueBag * epvBag in epv.eventProperty2EventPropertyValueBag){
                
                NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
                NSManagedObjectContext *managedObjectContext = [[CoreDataController sharedInstance] masterManagedObjectContext];
                NSEntityDescription *entity = [NSEntityDescription
                                               entityForName:@"Image" inManagedObjectContext:managedObjectContext];
                [fetchRequest setEntity:entity];
                
                [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"external_id == %@", epvBag.value]];
                
                NSError *error;
                ImageDb * pic = (ImageDb*)[[managedObjectContext executeFetchRequest:fetchRequest error:&error] lastObject];
                
                UIImageView *imageView = [cell_.photoViews objectAtIndex:index];
                imageView.hidden = NO;
                imageView.image = [UIImage imageWithData:pic.thumb];
                
                index ++;

                // 3 images are enough
                if (index == 3)
                    break;
            }
        }

        cell_.teachersLabel.text = [NSString stringWithFormat:@"from %@",event.teacherName];
        
    } else {
        
        MainTableViewCell *cell_ = (MainTableViewCell*)cell;
        
        [self setImageToImageView:cell_.childrenPhotoView forUrl:url];
        
        cell_.eventTypeView.image = [UIImage imageNamed:[ActionTypeConverter eventTypeStringForInt:[event actionType].integerValue]];
        cell_.timeLabel.text = [event createdAtAsString];
        
        cell_.teachersLabel.text = [NSString stringWithFormat:@"from %@",event.teacherName];
        
        for (EventPropertyValue * epv in event.event2EventProperty)
        {
            NSLog(@"Name=%@, Type=%@, Value=%@", epv.name, epv.type, epv.value);
            if ([epv.name isEqualToString:@"message"])
            {
                cell_.titleLabel.text = epv.value;
            }
            else if ([epv.name isEqualToString:@"title"])
            {
                cell_.titleLabel.text = epv.value;
            }
            // if we do not have message and have time and date - show as a message
            else if ([epv.name isEqualToString:@"time-and-date"])
            {
                NSTimeInterval epoch = [epv.value doubleValue];
                NSDate * date = [NSDate dateWithTimeIntervalSince1970:epoch];
                
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
                
                //Optionally for time zone converstions
                //        [formatter setTimeZone:[NSTimeZone timeZoneWithName:@"..."]];
                
                NSString *stringFromDate = [formatter stringFromDate:date];

                cell_.titleLabel.text = stringFromDate;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Event * event = (Event *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    if ([event.actionType integerValue] == PHOTOS)
        return 116.0f;
    return 70.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    Event *event = (Event *)[self.fetchedResultsController objectAtIndexPath:indexPath];
    
    UIViewController *dtvc;
    if ([event.actionType integerValue] == PHOTOS){
        
        DetailCollectionViewController *dt = (DetailCollectionViewController*)
        [self.storyboard instantiateViewControllerWithIdentifier:@"PhotosDetailView"];
        dt.hidesBottomBarWhenPushed = YES;
        dt.event = event;
        dtvc = dt;
    }
    else if ([event.actionType integerValue] == NOTE ||
             [event.actionType integerValue] == SLEEP ||
             [event.actionType integerValue] == MEAL ||
             [event.actionType integerValue] == CHILD_ABSENCE ||
             [event.actionType integerValue] == CHILD_WILL_HAVE_MEAL ||
             [event.actionType integerValue] == SINGIN ||
             [event.actionType integerValue] == SINGOUT){
        
        EventDetailViewController *dt = (EventDetailViewController*)
        [self.storyboard instantiateViewControllerWithIdentifier:@"EventDetailView"];
        dt.event = event;
        dtvc = dt;
    }
    
//    dtvc.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self.navigationController pushViewController:dtvc animated:YES];
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    UITableView *tableView = self.tableView;
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:[NSArray arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
          //  [self configureCell:[tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:[NSArray
                                               arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:[NSArray
                                               arrayWithObject:newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id )sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

- (void)syncCompleted:(id)object{
    
    NSLog(@"syncCompleted called.");
    
    self.fetchedResultsController = [self createNSFetchedResultsController];
    _fetchedResultsController.delegate = self;
    
    [self fetchEvents];
    
    [self.tableView reloadData];
    
    [self.tableView.pullToRefreshView stopAnimating];
}

@end