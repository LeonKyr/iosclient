//
//  SignInOutEventDetailViewController.h
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Event.h"
#import "EventDetailViewControllerProtocol.h"
#import "EventDetailBase.h"

@interface SignInOutEventDetailViewController : EventDetailBase

@property (weak, nonatomic) IBOutlet UITextView *textView;

@property (nonatomic, strong) Event * event;

+(NSString*)getViewName;

@end