//
//  SyncEngine.h
//  ParentMessenger
//
//  Created by Leo on 02/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Listener.h"

typedef enum {
    ObjectSynced = 0,
    ObjectCreated,
    ObjectDeleted,
} ObjectSyncStatus;

@interface SyncEngine : NSObject

@property (atomic, readonly) BOOL syncInProgress;

@property (nonatomic, strong) NSMutableArray *registeredClassesToSync;
@property (nonatomic, strong) NSDateFormatter *dateFormatter;


+ (SyncEngine *)sharedEngine;

- (void)registerNSManagedObjectClassToSync:(Class)aClass;
- (void)startSync;
- (void)synchronizeEvents:(Listener*)listener withDeviceId:(NSString*)deviceId;

@end

