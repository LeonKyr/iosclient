//
//  AnalyticsApi.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 16/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "AnalyticsApi.h"
#import "Flurry.h"

@implementation AnalyticsApi

-(void)loginClickedOnTutorial:(NSString*)imageId
{
    NSDictionary *articleParams =
        [NSDictionary dictionaryWithObjectsAndKeys:
         @"imageId", @"imageId", // The tutorial image id
     nil];
    
    [Flurry logEvent:@"Login_Init" withParameters:articleParams];
}

@end
