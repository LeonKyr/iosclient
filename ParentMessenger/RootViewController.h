//
//  RootViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 17/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "FrostedViewController.h"
#import "Model.h"

@interface RootViewController : FrostedViewController

@property (nonatomic, weak) Model* model;

@end
