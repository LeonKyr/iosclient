//
//  AppDelegate.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 10/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Model.h"
#import <FacebookSDK/FacebookSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) Model* model;

// FBSample logic
// In this sample the app delegate maintains a property for the current
// active session, and the view controllers reference the session via
// this property, as well as play a role in keeping the session object
// up to date; a more complicated application may choose to introduce
// a simple singleton that owns the active FBSession object as well
// as access to the object by the rest of the application
@property (strong, nonatomic) FBSession *session;

// Push Notification registration
- (void)requestPermissionsForPushNotification;

@end
