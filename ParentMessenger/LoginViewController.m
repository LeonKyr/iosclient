//
//  LoginViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 11/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "LoginViewController.h"
#import "ParentMessengerApi.h"
#import "RootViewController.h"
#import "AppDelegate.h"
#import "Listener.h"
#import "SyncEngine.h"
#import <FacebookSDK/FacebookSDK.h>
#import <QuartzCore/QuartzCore.h>
#import "TransitionDelegate.h"
#import "ForgotViewController.h"
#import "CryptoHelper.h"

#define keyEmail @"emailKeyMy"
#define keyRemember @"rememberMeKey"
#define keyPassword @"passwordKeyMy"

@interface LoginViewController ()<ControllerDismiss>

@property (nonatomic, strong) TransitionDelegate *transitionController;

@end

@implementation LoginViewController

@synthesize remember_meS;
@synthesize remember_me_label;

@synthesize loginButton;
@synthesize cancelButton;

@synthesize transitionController;

@synthesize orLabel;

@synthesize backItem;

@synthesize faceBookButton;

#define sizeI 125.0f

#pragma mark - action touches detect

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark - UI custom

- (void)customizeUI{
    
    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UINavigationBar appearance] titleTextAttributes]];
    [titleBarAttributes setValue:[UIFont fontWithName:@"Tahoma" size:17.5] forKey:NSFontAttributeName];
    [titleBarAttributes setValue:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    [[UINavigationBar appearance] setTitleTextAttributes:titleBarAttributes];
    
    self.title = NSLocalizedString(@"SIGN IN", nil);
    self.backItem.tintColor = [UIColor colorWithRed:179.0f/255.0f
                                              green:179.0f/255.0f
                                               blue:179.0f/255.0f
                                              alpha:1.0f];
    
    self.forgotPassword.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
    [self.forgotPassword setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.forgotPassword setTitleColor:[UIColor colorWithWhite:0.333f alpha:1.0f] forState:UIControlStateHighlighted];
    
    self.loginButton.backgroundColor = [UIColor colorWithRed:179.0f/255.0f
                                                       green:179.0f/255.0f
                                                        blue:179.0f/255.0f
                                                       alpha:1.0f];
    
    [self.loginButton.titleLabel setFont:[UIFont boldSystemFontOfSize:20.0f]];
    
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.remember_me_label.text = NSLocalizedString(@"Remember Me", nil);
    self.remember_me_label.font = [UIFont fontWithName:@"Tahoma" size:17.5];
    self.remember_me_label.textColor = [UIColor colorWithRed:153.0f/255.0f
                                                       green:153.0f/255.0f
                                                        blue:153.0f/255.0f
                                                       alpha:1.0f];
    
     // Example of a bigger switch with images
    self.remember_meS.onTintColor = [UIColor colorWithRed:153.0f/255.0f
                                                    green:153.0f/255.0f
                                                     blue:153.0f/255.0f
                                                    alpha:1.0f];
    
    self.email.delegate = self;
    self.password.delegate = self;
    self.password.secureTextEntry = YES;
    
    
    [self.email addSubview:({
       
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.email.frame.size.width, 1)];
        view.backgroundColor = [UIColor colorWithRed:213.0f/255.0f
                                               green:213.0f/255.0f
                                                blue:213.0f/255.0f
                                               alpha:1.0f];
        view;
    
    })];
    
    [self.password addSubview:({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.password.frame.size.width, 1)];
        view.backgroundColor = [UIColor colorWithRed:213.0f/255.0f
                                               green:213.0f/255.0f
                                                blue:213.0f/255.0f
                                               alpha:1.0f];
        view;
        
    })];
    
    [self.password addSubview:({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.password.frame.size.height - 1, self.password.frame.size.width, 1)];
        view.backgroundColor = [UIColor colorWithRed:213.0f/255.0f
                                               green:213.0f/255.0f
                                                blue:213.0f/255.0f
                                               alpha:1.0f];
        view;
        
    })];
    
    self.email.backgroundColor = [UIColor clearColor];
    self.password.backgroundColor = [UIColor clearColor];
    
    self.email.leftView = ({UIView *view = [[UIView alloc]
                                            initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, self.email.frame.size.height)];
        view;
    });
    self.email.leftViewMode = UITextFieldViewModeAlways;
    
    self.password.leftView = ({UIView *view = [[UIView alloc]
                                            initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, self.password.frame.size.height)];
        view;
    });
    self.password.leftViewMode = UITextFieldViewModeAlways;
    
    self.email.font = [UIFont fontWithName:@"Tahoma" size:17.5];
    self.password.font = [UIFont fontWithName:@"Tahoma" size:17.5];
    
    self.email.placeholder = NSLocalizedString(@"Email Address", nil);
    self.password.placeholder = NSLocalizedString(@"Password", nil);
//    self.forgotPassword.titleLabel.text = NSLocalizedString(@"Forgot password?", nil);
    [self.forgotPassword setTitle:NSLocalizedString(@"Forgot password?", nil) forState:UIControlStateNormal];
    
    self.orLabel.backgroundColor = [UIColor clearColor];
    self.orLabel.text = NSLocalizedString(@"OR", nil);
    self.orLabel.textColor = [UIColor colorWithRed:179.0f/255.0f
                                             green:179.0f/255.0f
                                              blue:179.0f/255.0f
                                             alpha:1.0f];
    self.orLabel.font = [UIFont boldSystemFontOfSize:13.0f];

    
    self.faceBookButton.titleLabel.font = [UIFont boldSystemFontOfSize:21.5f];
    
    [self.faceBookButton setBackgroundImage:[UIImage imageNamed:@"facebook"] forState:UIControlStateHighlighted];
    
    
    [self.faceBookButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.faceBookButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self.faceBookButton setHidden:YES];
    
    self.loginButton.titleLabel.adjustsFontSizeToFitWidth = YES;
}

#pragma mark - view life cycle

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
    
}

- (void)viewWillDisappear:(BOOL)animated{
    
    [super viewWillDisappear:animated];
    
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self customizeUI];
    
    self.transitionController = [[TransitionDelegate alloc] init];
    
//    [self.email setText:@"parent@boogoogoo.io"];
//    [self.password setText:@"boogoogoo"];
    
    [self.email setKeyboardType:UIKeyboardTypeEmailAddress];
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:keyRemember]){
        
        self.email.text = [[NSUserDefaults standardUserDefaults] stringForKey:keyEmail];
        self.password.text = [[NSUserDefaults standardUserDefaults] stringForKey:keyPassword];
    }
    
    [self.loginButton setTitle:NSLocalizedString(@"Login", nil) forState:UIControlStateNormal];
//    [self.faceBookButton setTitle:NSLocalizedString(@"Facebook", nil) forState:UIControlStateNormal];
//    self.title = NSLocalizedString(@"SIGN IN", nil);
    
    self.remember_meS.on = [[NSUserDefaults standardUserDefaults] boolForKey:keyRemember];
    
    [self.errorMessage setHidden:YES];
    
    [self.activityIndicator setHidesWhenStopped:YES];
    [self.activityIndicator stopAnimating];
}

#pragma mark - textField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

#pragma mark - dismiss delegate

- (void)controllerDismiss{
}

#pragma mark - IBAction method

- (IBAction)forgotPassword:(id)sender{
    
    ForgotViewController *vc = [[ForgotViewController alloc] initWithNibName:NSStringFromClass([ForgotViewController class]) bundle:nil];
    vc.delegate = self;
    [vc setTransitioningDelegate:transitionController];
    vc.modalPresentationStyle = UIModalPresentationCustom;
    [self presentViewController:vc animated:YES completion:nil];
}

- (IBAction)login:(id)sender{
    
    [sender setEnabled:NO];
    
    [self.view endEditing:YES];
    
    [self.errorMessage setHidden:YES];
    
    BOOL value = self.remember_meS.isOn;
    
    NSUserDefaults *standartUserDefaults = [NSUserDefaults standardUserDefaults];
    [standartUserDefaults setBool:value forKey:keyRemember];
    
    if (value){
        
        [standartUserDefaults setObject:self.email.text forKey:keyEmail];
        [standartUserDefaults setObject:self.password.text forKey:keyPassword];
        
    } else {
        
        [standartUserDefaults removeObjectForKey:keyEmail];
        [standartUserDefaults removeObjectForKey:keyPassword];
    }
    
    [standartUserDefaults synchronize];
    
    [self.activityIndicator startAnimating];
    self.loginButton.userInteractionEnabled = NO;
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ParentMessengerApi* api = [ParentMessengerApi create];

        NSString * hashedPassword = [CryptoHelper md5:self.password.text];
        NSLog(@"Hashed password = %@", hashedPassword);
        Listener* listener = [api loginWithEmail:self.email.text andPassword:hashedPassword];

        if (listener != nil && listener.externalId != nil)
        {
            NSLog(@"LoginViewController: LOGGED IN listenerExternalId=%@", listener.externalId);
            
            listener = [api getListenerWithExternalId:listener.externalId];
        
            [self performSelectorOnMainThread:@selector(loginSuccessful:)
                               withObject:listener waitUntilDone:YES];
            
            [[SyncEngine sharedEngine] startSync];
        }
        else
        {
            NSLog(@"LoginViewController: ACCESS DENIED.");
            
            [self performSelectorOnMainThread:@selector(loginDenied:)
                                   withObject:nil waitUntilDone:YES];
        }
        self.loginButton.userInteractionEnabled = YES;
    });
}

-(void)loginDenied:(Listener *)listener{
    
    [self.loginButton setEnabled:YES];
    
    [self.errorMessage setText:NSLocalizedString(@"Access denied", nil)];
    [self.errorMessage setHidden:NO];
    [self.activityIndicator stopAnimating];
}

- (void)loginSuccessful:(Listener *)listener{

    [self.loginButton setEnabled:YES];
    
    [self.activityIndicator stopAnimating];

    NSUserDefaults *prefs = [NSUserDefaults standardUserDefaults];
    [prefs setObject:listener.externalId forKey:@"listenerExternalId"];
    
    [self.errorMessage setHidden:YES];

    NSLog(@"Stored listenerExternalId=%@",listener.externalId);

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.model.currentListener = listener;
    
    NSData *encodedObject = [NSKeyedArchiver archivedDataWithRootObject:listener];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:encodedObject forKey:@"listener"];
    [defaults synchronize];

    [self performSegueWithIdentifier:@"loginSuccessfulSegue" sender:self];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"loginSuccessfulSegue"])
    {
        RootViewController* vc = [segue destinationViewController];
        
        [vc setModel:self.model];
    }
}

- (IBAction)cancel:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

/*

- (IBAction)facebookLogin:(id)sender {
    // get the app delegate so that we can access the session property
    AppDelegate *appDelegate = [[UIApplication sharedApplication]delegate];
    
    NSLog(@"facebookLogin STARTED");
    // this button's job is to flip-flop the session from open to closed
    if (appDelegate.session.isOpen) {
        NSLog(@"appDelegate.session.isOpen");
        // if a user logs out explicitly, we delete any cached token information, and next
        // time they run the applicaiton they will be presented with log in UX again; most
        // users will simply close the app or switch away, without logging out; this will
        // cause the implicit cached-token login to occur on next launch of the application
        [appDelegate.session closeAndClearTokenInformation];
        
        Listener* listener = [[Listener alloc] initWithName:@"NAME" andEmail:@"EMAIL" andExternalId:@"max-tester-01"];
        
        NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@", appDelegate.session.accessTokenData.accessToken]);
        
        [self loginSuccessful:listener];
        
    } else {
        if (appDelegate.session.state != FBSessionStateCreated) {
            // Create a new, logged out session.
            appDelegate.session = [[FBSession alloc] init];
        }
        NSLog(@"appDelegate.session.isOpen - ELSE");
        
        // if the session isn't open, let's open it now and present the login UX to the user
        [appDelegate.session openWithCompletionHandler:^(FBSession *session,
                                                         FBSessionState state,
                                                         NSError *error) {
            // and here we make sure to update our UX according to the new session state
            
            NSLog(@"openWithCompletionHandler");
            
            switch (state) {
                case FBSessionStateOpen: {
                   
                }
                    break;
                case FBSessionStateClosed:
                case FBSessionStateClosedLoginFailed:
                {
                    // Once the user has logged in, we want them to
                    // be looking at the root view.
                    Listener* listener = [[Listener alloc] initWithName:@"NAME" andEmail:@"EMAIL" andExternalId:@"max-tester-01"];
                    
                    NSLog(@"%@", [NSString stringWithFormat:@"https://graph.facebook.com/me/friends?access_token=%@", appDelegate.session.accessTokenData.accessToken]);
                    
                    [self loginSuccessful:listener];

                    [FBSession.activeSession closeAndClearTokenInformation];
                }
                    break;
                default:
                    break;
            }
            
            if (error) {
                UIAlertView *alertView = [[UIAlertView alloc]
                                          initWithTitle:@"Error"
                                          message:error.localizedDescription
                                          delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
                [alertView show];
            }
        }];
    }

}
 
 */
@end