//
//  TutorialPageContainerViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 12/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "TutorialPageContainerViewController.h"

#define keyTitle @"titleKey"
#define keyImage @"imageKey"

@interface TutorialPageContainerViewController ()

@end

@implementation TutorialPageContainerViewController

@synthesize imageData;
@synthesize scrollView;

@synthesize pageControl;

#pragma mark - tutorial create

#define viewTag 111

- (void)makeTutorial{
    
    self.imageData = @[@{keyTitle: NSLocalizedString(@"Swipe to learn more >", nil), keyImage: @"welcome-to-boogoogoo.png"},
                       @{keyTitle: NSLocalizedString(@"Receive updates about your child", nil), keyImage: @"receive-updates.png"},
                       @{keyTitle: NSLocalizedString(@"Make your kid healthier", nil), keyImage: @"make-healthier-kid.png"},
                       @{keyTitle: NSLocalizedString(@"You will not forget anything anymore", nil), keyImage: @"do-not-forget.png"},
                       @{keyTitle: NSLocalizedString(@"Real-time communication channel", nil), keyImage: @"real-time-communication.png"}];
    
    self.scrollView.showsHorizontalScrollIndicator = NO;
    
	// define the scroll view content size and enable paging
	[self.scrollView setPagingEnabled: YES];
    
	[self.scrollView setContentSize: CGSizeMake(self.scrollView.bounds.size.width * [self.imageData count], self.scrollView.bounds.size.height)] ;
    
    self.pageControl.backgroundColor = [UIColor clearColor];
	[self.pageControl setNumberOfPages:[self.imageData count]];
	[self.pageControl setCurrentPage:0];
	[self.pageControl addTarget:self
                         action:@selector(pageControlClicked:)
               forControlEvents:UIControlEventValueChanged] ;
	[self.pageControl setDefersCurrentPageDisplay:YES];
	[self.pageControl setType:DDPageControlTypeOnFullOffEmpty];
	[self.pageControl setOnColor:[UIColor colorWithRed:153.0/255.0f
                                                 green:153.0/255.0f
                                                  blue:153.0/255.0f
                                                 alpha:1.0f]];
	
    [self.pageControl setOffColor:[UIColor colorWithRed:204.0f/255.0f
                                                  green:204.0f/255.0f
                                                   blue:204.0f/255.0f alpha:1.0f]];
    
	[self.pageControl setIndicatorDiameter: 10.0f];
	[self.pageControl setIndicatorSpace: 5.0f];
	
    UILabel *pageLabel;
    UIImageView *imageView;
	CGRect pageFrame;
    
	for (int i = 0 ; i < [self.imageData count] ; i++){
        
		NSDictionary *dict = self.imageData[i];
        
        pageFrame = CGRectMake(i * scrollView.bounds.size.width, 0.0f, scrollView.bounds.size.width, scrollView.bounds.size.height);
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(pageFrame.origin.x + 50.0f, pageFrame.origin.y + 50, pageFrame.size.width - 90.0f, pageFrame.size.height - 120.0f)];
        imageView.image = [UIImage imageNamed:dict[keyImage]];
        imageView.backgroundColor = [UIColor clearColor];
        imageView.tag = viewTag;
        
        CALayer * l = [imageView layer];
        [l setMasksToBounds:YES];
        [l setCornerRadius:6.25];
        
        [self.scrollView addSubview:imageView];
        
        pageLabel = [[UILabel alloc] initWithFrame:CGRectMake(pageFrame.origin.x, pageFrame.size.height - 30.0f, pageFrame.size.width, 20.0f)];
        pageLabel.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
        pageLabel.textAlignment = NSTextAlignmentCenter;
        pageLabel.numberOfLines = 1;
        pageLabel.text = dict[keyTitle];
        pageLabel.backgroundColor = [UIColor clearColor];
        pageLabel.textColor = [UIColor colorWithRed:153.0f/255.0f
                                              green:153.0f/255.0f
                                               blue:153.0f/255.0f
                                              alpha:1.0f];
        [self.scrollView addSubview:pageLabel];
	}
    self.scrollView.backgroundColor = [UIColor clearColor];
    self.view.backgroundColor = [UIColor whiteColor];
    [self.scrollView setContentOffset:CGPointMake(0.0f, 0.0f)];
//    self.automaticallyAdjustsScrollViewInsets = NO;
}

#pragma mark - view life cycle

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL isRemember = [defaults boolForKey:@"rememberMeKey"];
    AppDelegate *appDelegate = (AppDelegate*)[UIApplication sharedApplication].delegate;
    
    if (appDelegate.model.currentListener && isRemember)
        [self performSegueWithIdentifier:@"isRememberSegues" sender:self];
    
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self customizeButtons];
    
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [[self navigationController] setToolbarHidden:YES animated:NO];
    
    [self.view bringSubviewToFront:self.loginButton];
    [self.view bringSubviewToFront:self.registerButton];
    
    [self makeTutorial];
}

#pragma mark - customize buttons

- (void)customizeButtons{
    
    UIFont *font = [UIFont boldSystemFontOfSize:18.5f];
    
    self.loginButton.titleLabel.font = font;
    self.registerButton.titleLabel.font = font;
    
    self.loginButton.backgroundColor = [UIColor colorWithRed:236.0f/255.0f
                                                       green:169.0f/255.0f
                                                        blue:38.0f/255.0f
                                                       alpha:1.0f];
    
    self.registerButton.backgroundColor = [UIColor colorWithRed:2166.0f/255.0f
                                                          green:81.0f/255.0f
                                                           blue:37.0f/255.0f
                                                          alpha:1.0f];
    
    [self.loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [self.loginButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    [self.registerButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    UIEdgeInsets insets = UIEdgeInsetsMake(-1.0f, 0.0f, 0.0f, 0.0f);
    
    self.loginButton.titleEdgeInsets = insets;
    self.registerButton.titleEdgeInsets = insets;
    
    
    [self.loginButton setTitle:NSLocalizedString(@"Login", nil) forState:UIControlStateNormal];
    [self.registerButton setTitle:NSLocalizedString(@"Register", nil) forState:UIControlStateNormal];
    
    self.loginButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.registerButton.titleLabel.adjustsFontSizeToFitWidth = YES;
}

#pragma mark - DDPageControl triggered actions

- (void)pageControlClicked:(id)sender{
    
	DDPageControl *thePageControl = (DDPageControl *)sender ;
    
    [scrollView setContentOffset: CGPointMake(scrollView.bounds.size.width * thePageControl.currentPage, scrollView.contentOffset.y) animated: YES] ;
    
    [pageControl updateCurrentPageDisplay];
}

#pragma mark - UIScrollView delegate methods

- (void)scrollViewDidScroll:(UIScrollView *)aScrollView{
    
	CGFloat pageWidth = scrollView.bounds.size.width ;
    float fractionalPage = scrollView.contentOffset.x / pageWidth ;
	NSInteger nearestNumber = lround(fractionalPage) ;
	
	if (pageControl.currentPage != nearestNumber)
	{
		pageControl.currentPage = nearestNumber ;
		
		if (scrollView.dragging)
			[pageControl updateCurrentPageDisplay] ;
	}
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    
	return (interfaceOrientation == UIInterfaceOrientationPortrait) ;
}

@end