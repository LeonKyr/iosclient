//
//  SyncEngine.m
//  ParentMessenger
//
//  Created by Leo on 02/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "SyncEngine.h"
#import "CoreDataController.h"
#import "API/ParentMessengerApi.h"
#import "EventPropertyValue.h"
#import "ChildWithEvents.h"
#import "EventPropertyValueDto.h"
#import "AppDelegate.h"
#import "EventPropertyValueBag.h"
#import "ImageDb.h"

NSString * const kSyncEngineInitialCompleteKey = @"SyncEngineInitialSyncCompleted";
NSString * const kSyncEngineSyncCompletedNotificationName = @"SyncEngineSyncCompleted";

@implementation SyncEngine

@synthesize syncInProgress = _syncInProgress;

@synthesize registeredClassesToSync = _registeredClassesToSync;
@synthesize dateFormatter = _dateFormatter;

+ (SyncEngine *)sharedEngine {
    static SyncEngine *sharedEngine = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedEngine = [[SyncEngine alloc] init];
    });
    
    return sharedEngine;
}

- (void)registerNSManagedObjectClassToSync:(Class)aClass {
    if (!self.registeredClassesToSync) {
        self.registeredClassesToSync = [NSMutableArray array];
    }
    
    if ([aClass isSubclassOfClass:[NSManagedObject class]]) {
        if (![self.registeredClassesToSync containsObject:NSStringFromClass(aClass)]) {
            [self.registeredClassesToSync addObject:NSStringFromClass(aClass)];
        } else {
            NSLog(@"Unable to register %@ as it is already registered", NSStringFromClass(aClass));
        }
    } else {
        NSLog(@"Unable to register %@ as it is not a subclass of NSManagedObject", NSStringFromClass(aClass));
    }
}

- (void)startSync {
    NSLog(@"startSync syncInProgress = %hhd", self.syncInProgress);
    if (!self.syncInProgress) {
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = YES;
        [self didChangeValueForKey:@"syncInProgress"];
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            Listener * listener = appDelegate.model.currentListener;
            NSString * deviceId = appDelegate.model.deviceId;
            [self synchronizeEvents:listener withDeviceId: deviceId];
            
            [self executeSyncCompletedOperations];
        });
    }
}


- (void)executeSyncCompletedOperations {
    dispatch_async(dispatch_get_main_queue(), ^{
        [self setInitialSyncCompleted];
        NSError *error = nil;
        [[CoreDataController sharedInstance] saveBackgroundContext];
        if (error) {
            NSLog(@"Error saving background context after creating objects on server: %@", error);
        }
        
        [[CoreDataController sharedInstance] saveMasterContext];
        [[NSNotificationCenter defaultCenter]
         postNotificationName:kSyncEngineSyncCompletedNotificationName
         object:nil];
        [self willChangeValueForKey:@"syncInProgress"];
        _syncInProgress = NO;
        [self didChangeValueForKey:@"syncInProgress"];
    });
}


- (BOOL)initialSyncComplete {
    return [[[NSUserDefaults standardUserDefaults] valueForKey:kSyncEngineInitialCompleteKey] boolValue];
}

- (void)setInitialSyncCompleted {
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:kSyncEngineInitialCompleteKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


- (NSDate *)mostRecentUpdatedAtDateForEntityWithName:(NSString *)entityName {
    __block NSDate *date = nil;
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:entityName];
    [request setSortDescriptors:[NSArray arrayWithObject:
                                 [NSSortDescriptor sortDescriptorWithKey:@"updatedAt" ascending:NO]]];
    [request setFetchLimit:1];
    [[[CoreDataController sharedInstance] backgroundManagedObjectContext] performBlockAndWait:^{
        NSError *error = nil;
        NSArray *results = [[[CoreDataController sharedInstance] backgroundManagedObjectContext] executeFetchRequest:request error:&error];
        if ([results lastObject])   {
            date = [[results lastObject] valueForKey:@"updatedAt"];
        }
    }];
    
    return date;
}

- (void)synchronizeEvents:(Listener*)listener withDeviceId:(NSString*)deviceId {
    
    ParentMessengerApi* api = [[ParentMessengerApi alloc] initWithDeviceId:deviceId];
    NSArray* childWithEvents =
        [api getEventsWithListener:listener andCreatedAt:nil];
    
    NSManagedObjectContext *context = [[CoreDataController sharedInstance] backgroundManagedObjectContext];
   
    for (ChildWithEvents * ce in childWithEvents)
    {
        [self storeChild:ce.child withManagerObjectContext:context andUsingApi:api];
        [self storeEventsWithListener:listener
                            andChild:ce.child
                            andEvents: ce.events
                            withManagerObjectContext:context
                            usingApi:api];
    }
}

-(void)storeChild:(Child*)child withManagerObjectContext: (NSManagedObjectContext*) context andUsingApi:(ParentMessengerApi*)api
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Child" inManagedObjectContext:context]];

    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"id == %@", child.externalId]];
    
    NSError *error = nil;
    
    NSArray * ifRowExists = [context executeFetchRequest:fetchRequest error:&error];
    NSLog(@"[ifRowExists count] = %lu, childId=%@", (unsigned long)[ifRowExists count], child.externalId);
    NSLog(@"Storing child: %@ [externalId=%@]", child.name, child.externalId);
    if ([ifRowExists count] > 0)
    {
        // update
        [ifRowExists[0] setValue:child.name forKey:@"name"];
    }
    else
    {
        // insert
        NSManagedObject *entity = [NSEntityDescription insertNewObjectForEntityForName:@"Child" inManagedObjectContext:context];
        [entity setValue:child.externalId forKey:@"id"];
        [entity setValue:child.name forKey:@"name"];
        [entity setValue:child.code forKey:@"code"];
    }
    
    [self storeImage:child.image withManagerObjectContext:context andUsingApi:api];
}

-(void)storeImage:(Image*)image withManagerObjectContext: (NSManagedObjectContext*) context andUsingApi:(ParentMessengerApi*)api
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity:[NSEntityDescription entityForName:@"Image" inManagedObjectContext:context]];
    
    [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"external_id == %@", image.externalId]];
    
    NSError *error = nil;
    
    NSArray * ifRowExists = [context executeFetchRequest:fetchRequest error:&error];
    NSLog(@"[ifRowExists count] = %lu, image=%@", (unsigned long)[ifRowExists count], image.externalId);
    NSLog(@"Storing image: %@ [external_id=%@]", image.name, image.externalId);
    if ([ifRowExists count] > 0)
    {
        // update
        [ifRowExists[0] setValue:image.name forKey:@"name"];
    }
    else
    {
        // insert
        // save thumbnail
        Image * thumb = [api getThumb:[[Image alloc]initWithExternalId:image.externalId]];
        ImageDb * entity =(ImageDb*)[NSEntityDescription insertNewObjectForEntityForName:@"Image" inManagedObjectContext:context];
        
        NSData *imageData = UIImageJPEGRepresentation(thumb.image, 1.0);
        
        [entity setValue:image.externalId forKey:@"external_id"];
        [entity setValue:image.name forKey:@"name"];
        [entity setValue:image.externalId forKey:@"path"];
        [entity setValue:imageData forKey:@"thumb"];
    }
}

-(void)storeEventsWithListener:(Listener*)listener andChild:(Child*) child andEvents:(NSArray*)events withManagerObjectContext: (NSManagedObjectContext*) context usingApi:(ParentMessengerApi*)api
{
    NSLog(@"Storing events: %d", (int)events.count);
    
    // create the fetch request to get all Events matching the IDs
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    [fetchRequest setEntity: [NSEntityDescription entityForName:@"Event" inManagedObjectContext:context]];
    
    for(EventDto * e in events)
    {
        [fetchRequest setPredicate: [NSPredicate predicateWithFormat: @"id == %@", e.externalId]];
        
        NSError *error = nil;
        
        NSArray * ifRowExists = [context executeFetchRequest:fetchRequest error:&error];
        NSLog(@"[ifRowExists count] = %lu, eventId=%@", (unsigned long)[ifRowExists count], e.externalId);
        if ([ifRowExists count] > 0) continue;
        
        NSLog(@"Saving event with ID=%@", e.externalId);
        
        Event *entity = (Event*)[NSEntityDescription insertNewObjectForEntityForName:@"Event" inManagedObjectContext:context];
        [entity setValue:e.externalId forKey:@"id"];
        [entity setValue:[self mapActionType:e.actionType] forKey:@"action"];
        [entity setValue:e.teacher.externalId forKey:@"teacher_id"];
        [entity setValue:e.teacher.name forKey:@"teacher_name"];
        [entity setValue:listener.externalId forKey:@"listener_id"];
        [entity setValue:e.day forKey:@"day"];
        [entity setValue:child.externalId forKey:@"child_id"];
        [entity setValue:child.image.externalId forKey:@"child_ext_id_image"];
        
        NSString * createdAt = e.createdAt;
        [entity setValue:createdAt forKey:@"createdAt"];
        
        NSLog(@"TEACHER NAME=%@", e.teacher.name);

        for (EventPropertyValueDto *epv in e.properties)
        {
            EventPropertyValue *epvEntity = (EventPropertyValue*)[NSEntityDescription insertNewObjectForEntityForName:@"EventProperty" inManagedObjectContext:context];
            
            NSLog(@"Property name=%@, type=%@, value=%@, isBag=%hhd, valueBag=%@", epv.name, epv.type, epv.value, epv.isBag, epv.valueBag);
            
            [epvEntity setValue:e.externalId forKey:@"event_id"];
            [epvEntity setValue:epv.name forKey:@"name"];
            [epvEntity setValue:epv.type forKey:@"type"];
            [epvEntity setValue:[NSNumber numberWithBool:epv.isBag] forKey:@"is_bag"];
            if (epv.isBag)
            {
                for (NSDictionary * valuePair in epv.valueBag)
                {
                    for (NSString * key in [valuePair allKeys])
                    {
                        NSLog(@"KEY=%@,VALUE=%@", key, [valuePair objectForKey:key]);
                        
                        EventPropertyValueBag *epvBag = (EventPropertyValueBag*)[NSEntityDescription insertNewObjectForEntityForName:@"EventPropertyValueBag" inManagedObjectContext:context];
                        [epvBag setValue:[valuePair objectForKey:key] forKey:@"value"];
                        [epvBag setValue:key forKey:@"name"];
                        [epvEntity addeventProperty2EventPropertyValueBagObject:epvBag];
                        
                        // save thumbnail
                        Image * thumb = [api getThumb:[[Image alloc]initWithExternalId:[valuePair objectForKey:key]]];
                        ImageDb * imgDb =(ImageDb*)[NSEntityDescription insertNewObjectForEntityForName:@"Image" inManagedObjectContext:context];
                        
                        NSData *imageData = UIImageJPEGRepresentation(thumb.image, 1.0);
                                         
                        [imgDb setValue:[valuePair objectForKey:key] forKey:@"external_id"];
                        [imgDb setValue:[valuePair objectForKey:key] forKey:@"path"];
                        [imgDb setValue:imageData forKey:@"thumb"];
                    }
                }
            }
            else
            {
                [epvEntity setValue:epv.value forKey:@"value"];
            }
            
            [entity addevent2EventPropertyObject:epvEntity];
        }
    }
}

-(NSNumber*)mapActionType: (ActionType)actionType
{
    return [NSNumber numberWithInt:(int)actionType];
}


#pragma mark - File Management

- (NSURL *)applicationCacheDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSCachesDirectory inDomains:NSUserDomainMask] lastObject];
}

- (NSURL *)JSONDataRecordsDirectory{
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSURL *url = [NSURL URLWithString:@"JSONRecords/" relativeToURL:[self applicationCacheDirectory]];
    NSError *error = nil;
    if (![fileManager fileExistsAtPath:[url path]]) {
        [fileManager createDirectoryAtPath:[url path] withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    return url;
}

- (void)writeJSONResponse:(id)response toDiskForClassWithName:(NSString *)className {
    NSURL *fileURL = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    if (![(NSDictionary *)response writeToFile:[fileURL path] atomically:YES]) {
        NSLog(@"Error saving response to disk, will attempt to remove NSNull values and try again.");
        // remove NSNulls and try again...
        NSArray *records = [response objectForKey:@"results"];
        NSMutableArray *nullFreeRecords = [NSMutableArray array];
        for (NSDictionary *record in records) {
            NSMutableDictionary *nullFreeRecord = [NSMutableDictionary dictionaryWithDictionary:record];
            [record enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
                if ([obj isKindOfClass:[NSNull class]]) {
                    [nullFreeRecord setValue:nil forKey:key];
                }
            }];
            [nullFreeRecords addObject:nullFreeRecord];
        }
        
        NSDictionary *nullFreeDictionary = [NSDictionary dictionaryWithObject:nullFreeRecords forKey:@"results"];
        
        if (![nullFreeDictionary writeToFile:[fileURL path] atomically:YES]) {
            NSLog(@"Failed all attempts to save reponse to disk: %@", response);
        }
    }
}

- (void)deleteJSONDataRecordsForClassWithName:(NSString *)className {
    NSURL *url = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    NSError *error = nil;
    BOOL deleted = [[NSFileManager defaultManager] removeItemAtURL:url error:&error];
    if (!deleted) {
        NSLog(@"Unable to delete JSON Records at %@, reason: %@", url, error);
    }
}

- (NSDictionary *)JSONDictionaryForClassWithName:(NSString *)className {
    NSURL *fileURL = [NSURL URLWithString:className relativeToURL:[self JSONDataRecordsDirectory]];
    return [NSDictionary dictionaryWithContentsOfURL:fileURL];
}

- (NSArray *)JSONDataRecordsForClass:(NSString *)className sortedByKey:(NSString *)key {
    NSDictionary *JSONDictionary = [self JSONDictionaryForClassWithName:className];
    NSArray *records = [JSONDictionary objectForKey:@"results"];
    return [records sortedArrayUsingDescriptors:[NSArray arrayWithObject:
                                                 [NSSortDescriptor sortDescriptorWithKey:key ascending:YES]]];
}
@end
