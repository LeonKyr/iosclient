//
//  LoginViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 11/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "Model.h"
#import "SevenSwitch.h"
#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *errorMessage;
@property (weak, nonatomic) IBOutlet UILabel *remember_me_label;
@property (weak, nonatomic) IBOutlet UISwitch *remember_meS;
@property (weak, nonatomic) IBOutlet UIButton *forgotPassword;

@property (weak, nonatomic) IBOutlet UIButton *faceBookButton;

@property (weak, nonatomic) IBOutlet UILabel *orLabel;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backItem;

- (IBAction)cancel:(id)sender;

- (IBAction)forgotPassword:(id)sender;

@property (nonatomic, weak) Model* model;

- (IBAction)login:(id)sender;

@end