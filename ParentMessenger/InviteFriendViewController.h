//
//  InviteFriendViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface InviteFriendViewController : UIViewController<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *email;
@property (weak, nonatomic) IBOutlet UITextField *whatsApp;

- (IBAction)invite:(id)sender;
- (IBAction)cancel:(id)sender;

@end
