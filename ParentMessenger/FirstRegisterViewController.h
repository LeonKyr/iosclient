//
//  FirstRegisterViewController.h
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "BaseRegisterViewController.h"

@interface FirstRegisterViewController : BaseRegisterViewController<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UITextField *emailField;
@property (nonatomic, strong) IBOutlet UITextField *phoneField;
@property (nonatomic, strong) IBOutlet UITextField *passwordField;

@end