//
//  BaseLoginViewController.h
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "HelperRegister.h"
#import "StepRegisterView.h"

@interface BaseRegisterViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, weak) IBOutlet UIBarButtonItem *backItem;
@property (nonatomic, weak) IBOutlet UIBarButtonItem *nextItem;
@property (nonatomic, weak) IBOutlet StepRegisterView *stepView;

@property (nonatomic, strong) HelperRegister *helperRegister;

- (void)customizeUI;

- (void)customizeTextField:(UITextField*)textField andPlaceHolderText:(NSString*)text andWidth:(CGFloat)width;

- (IBAction)backMethod:(id)sender;
- (IBAction)nextMethod:(id)sender;

- (void)setColorForLabelInTextField:(UITextField*)textField forValue:(BOOL)value;

@end