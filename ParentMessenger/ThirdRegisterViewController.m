//
//  ThirdRegisterViewController.m
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "AppDelegate.h"
#import "ParentMessengerApi.h"
#import "ThirdRegisterViewController.h"

@interface ThirdRegisterViewController ()

@end

@implementation ThirdRegisterViewController

@synthesize childCodeField;

@synthesize rememberMeSwitch;

@synthesize termsLabel;
@synthesize messageLabel;
@synthesize rememberLabel;

#pragma mark - IBAction methods

- (IBAction)nextMethod:(id)sender{
    
    [self.navigationItem.rightBarButtonItem setEnabled:NO];
    
    BOOL remember = self.rememberMeSwitch.isOn;
    
    self.helperRegister.rememberMe = remember;
    
    if (self.childCodeField.text.length > 0)
        self.helperRegister.childCode = self.childCodeField.text;
    
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        ParentMessengerApi* api = [ParentMessengerApi create];
        
        Listener* listener = [api registerWithName:self.helperRegister.fullName
                                          andEmail:self.helperRegister.email
                                       andPassword:self.helperRegister.password
                                          andImage:self.helperRegister.photo];
        
        if (listener)
        {
            NSLog(@"RegistrationViewController=%@", listener.externalId);
        }
        [self performSelectorOnMainThread:@selector(fetchedData:)
                               withObject:listener waitUntilDone:YES];
    });

    
}

#pragma mark - Feetch data

- (void)fetchedData:(Listener *)listener {
    
      [self.navigationItem.rightBarButtonItem setEnabled:YES];
    
//    [self.activityIndicator stopAnimating];
    
    if (listener)
    {
        NSLog(@"LISTENERID=%@", listener.externalId);
        NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
        [settings setBool:YES forKey:@"newUserRegister"];
        [settings setObject:listener.externalId forKey:@"listenerExternalId"];
        [settings synchronize];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.model.currentListener = listener;
        [appDelegate.model.currentListener.image setImage:self.helperRegister.photo];
        
        [self.navigationController setNavigationBarHidden:YES];
        [self performSegueWithIdentifier:@"registerSuccessfulSegue" sender:self];
    }
    else
    {
        UIAlertView * view = [[UIAlertView alloc]
                              initWithTitle: NSLocalizedString(@"Error", nil)
                              message:NSLocalizedString(@"Registration failed, please try again.", nil)
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil) otherButtonTitles:nil, nil];
        [view show];
    }
}

#pragma mark - customize UI

- (void)customizeUI{
    
    [super customizeUI];
    
    self.stepView.step = 3;
    
    self.childCodeField.returnKeyType = UIReturnKeyDone;
    
    [self customizeTextField:self.childCodeField andPlaceHolderText:NSLocalizedString(@"Child code (if exist)", nil) andWidth:170.0f];
    
    self.rememberLabel.text = NSLocalizedString(@"Remember Me", nil);
    self.messageLabel.text = NSLocalizedString(@"By creating BooGooGoo account/you agree to the", nil);
    
    self.rememberLabel.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
    self.rememberLabel.textColor = [UIColor colorWithRed:153.0f/255.0f
                                                   green:153.0f/255.0f
                                                    blue:153.0f/255.0f
                                                   alpha:1.0f];
    
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.messageLabel.textColor = [UIColor colorWithRed:102.0f/255.0f
                                                  green:102.0f/255.0f
                                                   blue:102.0f/255.0f
                                                  alpha:1.0f];
    
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"Terms of Service & Privacy Policy", nil)];
    
    [attString addAttribute:NSUnderlineStyleAttributeName
                      value:[NSNumber numberWithInt:NSUnderlineStyleSingle]
                      range:(NSRange){0,[attString length]}];
    
    self.termsLabel.attributedText = attString;
    
}

#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
}

@end