//
//  ForgotViewController.h
//  ParentMessenger
//
//  Created by lolsi on 01.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
// 
#import <UIKit/UIKit.h>

@protocol ControllerDismiss <NSObject>

- (void)controllerDismiss;

@end

@interface ForgotViewController : UIViewController<UITextFieldDelegate>

@property (nonatomic, strong) IBOutlet UIView *forgotView;

@property (nonatomic, strong) IBOutlet UITextField *emailField;

@property (nonatomic, strong) IBOutlet UIButton *sendButton;
@property (nonatomic, strong) IBOutlet UIButton *cancelButton;

@property (nonatomic, unsafe_unretained) id <ControllerDismiss> delegate;

- (IBAction)send:(id)sender;
- (IBAction)cancel:(id)sender;

@end