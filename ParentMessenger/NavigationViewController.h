//
//  NavigationViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 17/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FrostedViewController.h"

@interface NavigationViewController : UINavigationController

//- (void)panGestureRecognized:(UIPanGestureRecognizer *)sender;

@end
