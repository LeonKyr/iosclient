//
//  AnalyticsApi.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 16/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Flurry.h"

@interface AnalyticsApi : NSObject

-(void)loginClickedOnTutorial:(NSString*)imageId;

@end
