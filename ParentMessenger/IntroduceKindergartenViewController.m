//
//  IntroduceKindergartenViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "IntroduceKindergartenViewController.h"
#import "ParentMessengerApi.h"
#import "AppDelegate.h"

@interface IntroduceKindergartenViewController ()

@end

@implementation IntroduceKindergartenViewController

@synthesize titleLabel;

@synthesize introduceButton;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                                                        green:178.0f/255.0f
                                                                         blue:178.0f/255.0f
                                                                        alpha:1.0f];
    
    self.title = NSLocalizedString(@"Introduce Kindergarten", nil);
    
    [self setTextField:self.phoneDcc topBorder:YES buttomBorder:YES];
    [self setTextField:self.addressDcc topBorder:NO buttomBorder:YES];
    [self setTextField:self.websiteDcc topBorder:NO buttomBorder:YES];
    [self setTextField:self.emailDcc topBorder:NO buttomBorder:YES];
    [self setTextField:self.phoneParent topBorder:NO buttomBorder:YES];
    
    self.titleLabel.numberOfLines = 0;
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.titleLabel.textColor = [UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
    
    self.introduceButton.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    [self.introduceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.introduceButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
}

- (void)setTextField:(UITextField*)textfield topBorder:(BOOL)topEnabled buttomBorder:(BOOL)butEnabled{
    
    textfield.delegate = self;
    
    textfield.backgroundColor = [UIColor whiteColor];
    
    textfield.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
    textfield.textColor = [UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1.0f];
    
    
    textfield.leftView = ({
    
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, textfield.frame.size.height)];
        view.backgroundColor = [UIColor clearColor];
        
        view;
    
    });
    textfield.leftViewMode = UITextFieldViewModeAlways;
    
    UIColor *borderColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
    
    if (topEnabled){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, textfield.frame.size.width, 1.0f)];
        view.backgroundColor = borderColor;
        [textfield addSubview:view];
    }
    
    if (butEnabled){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, textfield.frame.size.height - 1.0f, textfield.frame.size.width, 1.0f)];
        view.backgroundColor = borderColor;
        [textfield addSubview:view];
    }
}

- (IBAction)introduce:(id)sender {
    if (
        ![self.emailDcc.text isEqualToString:@""]
        || ![self.phoneDcc.text isEqualToString:@""]
        || ![self.addressDcc.text isEqualToString:@""]
        || ![self.websiteDcc.text isEqualToString:@""])
    {
        
        [self.introduceButton setEnabled:NO];
        
        KindergartenIntroduction * intro = [[KindergartenIntroduction alloc] init];
        [intro setEmail:self.emailDcc.text];
        [intro setPhone:self.phoneDcc.text];
        [intro setAddress:self.addressDcc.text];
        [intro setWebsite:self.websiteDcc.text];
        [intro setListerPhoneNumber:self.phoneParent.text];
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            ParentMessengerApi* api = [ParentMessengerApi create];
            
            AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            
            [api introduceKindergartenWithListener:appDelegate.model.currentListener andKindergartenIntroduction:intro];
            
            NSLog(@"IntroduceKindergartenViewController::introduce");
            
            [self performSelectorOnMainThread:@selector(cancel:) withObject:nil waitUntilDone:NO];
        });
    }
}

- (IBAction)cancel:(id)sender {
    
    [self.introduceButton setEnabled:YES]; //правда смысла нет и так контроллер закроется
   
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

@end
