//
//  FirstRegisterViewController.m
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "FirstRegisterViewController.h"

@interface FirstRegisterViewController ()

@end

@implementation FirstRegisterViewController

@synthesize emailField;
@synthesize phoneField;
@synthesize passwordField;

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    
    [self setColorForLabelInTextField:textField forValue:YES];
    
    if (textField.tag == 0){
        textField.keyboardType = UIKeyboardTypeEmailAddress;
        textField.returnKeyType = UIReturnKeyNext;
    }
    else if (textField.tag == 1){
        textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        textField.returnKeyType = UIReturnKeyNext;
    }
    else if (textField.tag == 2){
        textField.keyboardType = UIKeyboardTypeAlphabet;
        textField.returnKeyType = UIReturnKeyDone;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.tag == 2){
        
        return [textField resignFirstResponder];
    
    } else {
        
        UITextField *tf = (UITextField*)[self.view viewWithTag:textField.tag + 1];
        return [tf becomeFirstResponder];
        
    }
}

#pragma mark - IBAction methods

- (IBAction)showHidePassword:(id)sender{
    
    self.passwordField.secureTextEntry = !self.passwordField.isSecureTextEntry;
    
    if (self.passwordField.isSecureTextEntry)
        [sender setTitle:NSLocalizedString(@"SHOW", nil) forState:UIControlStateNormal];
    else
        [sender setTitle:NSLocalizedString(@"HIDE", nil) forState:UIControlStateNormal];
    
}

- (IBAction)nextMethod:(id)sender{
    
    [super nextMethod:sender];
    
    BOOL validatePhone = [self validatePhone:self.phoneField.text];
    BOOL validateEmail = [self validateEmail:self.emailField.text];
    BOOL validatePassword = [self validatePassword:self.passwordField.text];
    
    if (validatePhone && validateEmail && validatePassword){
        
        self.helperRegister.email = self.emailField.text;
        self.helperRegister.phone = self.phoneField.text;
        self.helperRegister.password = self.passwordField.text;
        
        [self performSegueWithIdentifier:@"FirstToTwoIdentifier" sender:self];
    }
}

#pragma mark - check email

- (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    
    BOOL value = [emailTest evaluateWithObject:emailStr];
    
    [self setColorForLabelInTextField:self.emailField forValue:value];
    
    return value;
}

- (BOOL)validatePhone:(NSString*)phone{
    
    NSError *error = nil;
    
    NSDataDetector *matchdetector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber
                                                                    error:&error];
    
    NSUInteger matchNumber = [matchdetector numberOfMatchesInString:phone options:0 range:NSMakeRange(0, [phone length])];
    
    if (matchNumber == 0){
        
        [self setColorForLabelInTextField:self.phoneField forValue:NO];
        return NO;
    }
    
     [self setColorForLabelInTextField:self.phoneField forValue:YES];
    return YES;
    
}

- (BOOL)validatePassword:(NSString*)password{
    
    if (password.length > 0){
        
        [self setColorForLabelInTextField:self.passwordField forValue:YES];
        return YES;
        
    } else{
        
        [self setColorForLabelInTextField:self.passwordField forValue:NO];
        return NO;
    }
}

#pragma mark - customize UI

- (void)customizeUI{
    
    [super customizeUI];
    
    self.stepView.step = 1;

    self.backItem.tintColor = [UIColor colorWithRed:179.0f/255.0f
                                              green:179.0f/255.0f
                                               blue:179.0f/255.0f
                                              alpha:1.0f];
    
    [self customizeTextField:self.emailField andPlaceHolderText:NSLocalizedString(@"Email", nil) andWidth:100.0f];
    [self customizeTextField:self.phoneField andPlaceHolderText:NSLocalizedString(@"Phone", nil) andWidth:100.0f];
    [self customizeTextField:self.passwordField andPlaceHolderText:NSLocalizedString(@"Password", nil) andWidth:120.0f];
    
    self.passwordField.secureTextEntry = YES;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:NSLocalizedString(@"SHOW", nil) forState:UIControlStateNormal];
    [button setTitleColor:[UIColor colorWithRed:201.0f/255.0f
                                          green:201.0f/255.0f
                                           blue:201.0f/255.0f
                                          alpha:1.0f] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    [button setFrame:CGRectMake(0.0f, 0.0f, 60.0f, self.passwordField.frame.size.height)];
    [button setTitleEdgeInsets:UIEdgeInsetsMake(0.0f, -20.0f, 0.0f, 0.0f)];
    [button addTarget:self action:@selector(showHidePassword:) forControlEvents:UIControlEventTouchUpInside];
    
    self.passwordField.rightView = button;
    self.passwordField.rightViewMode = UITextFieldViewModeAlways;
}

#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];
}

@end