//
//  MainMenuTabViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 18/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface MainMenuTabViewController : UIViewController

@property (nonatomic, weak) IBOutlet UIButton *addChildButton;
@property (nonatomic, weak) IBOutlet UIButton *introdiceButton;

@end