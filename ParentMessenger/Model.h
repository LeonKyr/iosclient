//
//  Model.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 29/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Listener.h"
#import "Child.h"

@interface Model : NSObject

@property (nonatomic, strong) Listener* currentListener;
@property (nonatomic, strong) NSString* activeChildExternalId;
@property (nonatomic, strong) NSString* deviceId;

@end
