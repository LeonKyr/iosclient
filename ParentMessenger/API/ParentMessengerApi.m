//
//  ParentMessengerApi.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 12/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "ParentMessengerApi.h"
#import "ActionType.h"
#import "Teacher.h"
#import "Child.h"
#import "ListenerFactory.h"
#import "ChildWithEvents.h"
#import "EventDto.h"
#import "EventPropertyValueDto.h"
#import "AppDelegate.h"

@implementation ParentMessengerApi

ListenerFactory* listenerFactory;

- (ParentMessengerApi*)init
{
    self = [super init];
    
    if (self)
    {
        [self setBaseUrl:@"http://api.parentmgr.com/v1/"];
#if TARGET_IPHONE_SIMULATOR
        // Simulator specific code
//        [self setBaseUrl:@"http://zzz/pm/web/app.php/v1/"];
        [self setBaseUrl:@"http://api.parentmgr.com/v1/"];
#else // TARGET_IPHONE_SIMULATOR
        // Device specific code
#endif // TARGET_IPHONE_SIMULATOR
    }
    
    listenerFactory = [[ListenerFactory alloc] init];
    self.eventFactory = [[EventFactory alloc] init];
    
    return self;
}

-(ParentMessengerApi*)initWithDeviceId:(NSString*) deviceId
{
    NSLog(@"ParentMessengerApi::initWithDeviceId.deviceId=%@", deviceId);
    
    self = [self init];
    if (self)
    {
        [self setDeviceId:deviceId];
    }
    
    return self;
}

//this is singletone make
+ (ParentMessengerApi*)create{
  
    static ParentMessengerApi *sharedApi;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        NSString * deviceId = appDelegate.model.deviceId;
        sharedApi = [[ParentMessengerApi alloc] initWithDeviceId:deviceId];
        
    });
    
    return sharedApi;
}

// ------------------ // event related methods // --------------------- //

-(NSArray*)getEventsWithListener:(Listener*)listener andCreatedAt:(NSDate*)createdAt
{
    NSURL *url = [[NSURL alloc] initWithString:
        [self.baseUrl stringByAppendingFormat: @"events/%@/0/10/", listener.externalId]];

    NSLog(@"getEventsWithListener URL= %@", url);
    
    NSData* data = [NSData dataWithContentsOfURL: url];
    
    NSError* error;
    
    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:data //1
                          options:kNilOptions
                          error:&error];
    
    if (error)
    {
        NSLog(@"Error: %@", error.description);
    }
    
    NSMutableArray* result = [NSMutableArray arrayWithCapacity:1];

    for (NSDictionary* childEvent in [json objectEnumerator])
    {
        NSDictionary* child = [childEvent objectForKey:@"child"];
        NSArray* events = [childEvent objectForKey:@"events"];
        
        Child * c = [self createChildFromJson: child];
        
        NSMutableArray* eventOfChild = [[NSMutableArray alloc] initWithCapacity:events.count];
        for(NSDictionary* e in events)
        {
            NSString* eventId = [e objectForKey:@"id"];
            NSString* action = [e objectForKey:@"action"];

            EventDto *event = [self.eventFactory createWithAction:action andEventId:eventId];
            
            NSLog(@"event ActionType=%u", event.actionType);

            [self setEventTeacherFromJson:e toEvent:event];
            [self setEventCreatedAtFromJson: e toEvent:event];

            [self setEventPropertiesFromJson: e toEvent:event];
            
            NSLog(@"eventId=%@", event.externalId);
 
            [eventOfChild addObject:event];
        }
        
        ChildWithEvents * ce = [[ChildWithEvents alloc] init];
        [ce setChild:c];
        [ce setEvents:eventOfChild];
        
        NSLog(@"Child [%@] has %d events", c.externalId, [eventOfChild count]);
        
        [result addObject:ce];
    }
    
    return result;
}

-(EventDto*)setEventPropertiesFromJson:(NSDictionary *) json toEvent: (EventDto*) event
{
    NSArray* properties = [json objectForKey:@"properties"];
    
    NSMutableArray * props = [[NSMutableArray alloc] init];
    for (NSDictionary* property in properties)
    {
        NSDictionary* type = [property objectForKey:@"type"];
        NSString* name = [type objectForKey:@"name"];
        NSString* ptype = [type objectForKey:@"type"];
        NSNumber * isBagNumber = (NSNumber *)[property valueForKey:@"is_bag"];

        BOOL isBag = (isBagNumber && [isBagNumber boolValue] == YES);

        EventPropertyValueDto * epv = [[EventPropertyValueDto alloc] init];
        [epv setName:name];
        [epv setType:ptype];
        [epv setIsBag:isBag];
        
        if (isBag == TRUE)
        {
            [epv setValueBag:[property objectForKey:@"value"]];
        }
        else
        {
            [epv setValue:[property objectForKey:@"value"]];
        }
        
        [props addObject:epv];
    }
    
    event.properties = props;
    
    return event;
}

-(void)setEventChildrenFromJson:(NSDictionary *) json toEvent: (EventDto*) event
{
    NSArray* children = [json objectForKey:@"children"];
    
    for (int j=0; j < [children count]; j++) {
        NSString* childId = [(NSDictionary*)children[j] objectForKey:@"id"];
        
        Child * child = [[Child alloc] init];
        [child setExternalId:childId];
        
//        [event.children addObject:child];
    }
}

-(void)setEventTeacherFromJson:(NSDictionary *) json toEvent: (EventDto*) event
{
    NSDictionary * data = [json objectForKey:@"teacher"];
    
    Teacher* teacher = [[Teacher alloc] init];
    [teacher setExternalId:[data objectForKey:@"id"]];
    [teacher setName:[data objectForKey:@"name"]];
    
    [event setTeacher:teacher];
}

-(void)setEventCreatedAtFromJson:(NSDictionary *) json toEvent: (EventDto*) event
{
    NSString* createdAt = [json objectForKey:@"created_at"];

    NSTimeInterval createdAtAsInt = [createdAt integerValue];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:createdAtAsInt];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *formattedDateString = [dateFormatter stringFromDate:date];

    [event setCreatedAt: formattedDateString];
    
    NSDateFormatter *dayDateFormatter = [[NSDateFormatter alloc] init];
    [dayDateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSString * day = [dayDateFormatter stringFromDate:date];
    [event setDay: day];
    
    NSLog(@"SET Day=%@, CreatedAt=%@", event.day, event.createdAt);
}

-(Listener*)createListenerFromJson:(NSDictionary *) json
{
    NSString* externalId = [json objectForKey:@"id"];
    NSString* name = [json objectForKey:@"name"];
    NSString* email = [json objectForKey:@"email"];
    
    Listener* result = [[Listener alloc] initWithName:name andEmail:email andExternalId:externalId];
    
    if ([json objectForKey:@"image"])
    {
        Image * image = [[Image alloc] init];
        
        [self setProperties:[json objectForKey:@"image"] forImage:image];
        
        [result setImage:image];
    }
    
    return result;
}

-(NSArray*)createChildrenFromJson:(NSArray *) json
{
    NSMutableArray * children = [[NSMutableArray alloc] init];
    
    for (NSDictionary * childJson in json) {
        Child* child = [self createChildFromJson:childJson];
        [children addObject:child];
    }
    
    return children;
}

-(Child*)createChildFromJson:(NSDictionary*)json
{
    Child * result = [[Child alloc] init];
    
    [result setExternalId:[json objectForKey:@"id"]];
    [result setName:[json objectForKey:@"name"]];
    [result setCode:[json objectForKey:@"code"]];

    if ([json objectForKey:@"image"])
    {
        Image * image = [[Image alloc] init];
        
        [self setProperties:[json objectForKey:@"image"] forImage:image];
        NSLog(@">>>Image: %@", image.name);
        
        [result setImage:image];
    }
    
    NSLog(@">>>Child: %@", result.name);
    
    return result;
}


// ------------------ // account methods // --------------------- //
-(Listener*)loginWithEmail:(NSString*)email andPassword:(NSString*)password
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"login/listener/%@/%@/", email, password]];
    
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
        [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSString * postString = [[NSString alloc] initWithFormat:@"{%@}", [self getDeviceJson]];
    NSLog(@"POST=%@", postString);
    [request setHTTPBody:[postString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError* error;
    NSData *reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error)
    {
        NSLog(@"Error registering: %@", error.description);
        return nil;
    }
    

    NSDictionary* jsonReply = [NSJSONSerialization
                               JSONObjectWithData:reply
                               options:kNilOptions
                               error:&error];
    
    if (error)
    {
        NSLog(@"Error registering: %@", error.description);
        return nil;
    }
    
    Listener * listener = [self createListenerFromJson: jsonReply];
    NSLog(@"loginWithEmail.listener.externalId=%@", listener.externalId);
    
    return listener;
}

-(NSString*)getDeviceJson
{
    NSString * deviceJson = [[NSString alloc] initWithFormat:@"\"device\":{\"id\": \"%@\"}", self.deviceId ];
    return deviceJson;
}

-(Listener*)registerWithName:(NSString*)name andEmail:(NSString*)email andPassword:(NSString*)password andImage:(UIImage*)image
{
    Image * img = [[Image alloc] init];
    img.image = image;
    
    Listener * result = [listenerFactory createName:name andEmail:email andExternalId:nil andImage:img andPassword:password];
    return [self changeListener:result];
}

-(void)changePasswordWithExternalId:(NSString*)externalId andNewPassword:(NSString*)newPassword
{}

- (void)forgotPasswordWithEmail:(NSString*)email andResultCompletion:(void (^)(NSURLResponse *response, NSData *data, NSError *connectionError))completion{
    
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"forgot-password/listener/%@/", email]];
    
    NSLog(@"forgot-password %@", url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
//    NSURLResponse *response;
//    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[NSOperationQueue mainQueue]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
                               
//                               NSError* error;
//                               NSDictionary* jsonReply = [NSJSONSerialization
//                                                          JSONObjectWithData:data
//                                                          options:NSJSONReadingAllowFragments
//                                                          error:&error];
                               //result = @"OK"
                               
                               completion(response, data, connectionError);

    }];
    
}

// ------------------ // Listener // --------------------- //
-(Child*)addChild:(NSString*)childCode toListener:(Listener*)listener
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"listener/%@/add-child/%@/", listener.externalId, childCode]];
    
    NSLog(@"addChild %@", url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSData *reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    NSError* error;
    NSDictionary* jsonReply = [NSJSONSerialization
                               JSONObjectWithData:reply
                               options:kNilOptions
                               error:&error];
    
    Child * child = [self createChildFromJson:jsonReply];
    NSLog(@"child.externalId=%@,name=%@", child.externalId, child.name);
    return child;
}

-(Listener*)getListenerWithExternalId:(NSString*)externalId
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"listener/%@/", externalId]];
    
    NSLog(@"%@", url);
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setValue:@"application/x-www-form-urlencoded;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    
    NSURLResponse *response;
    NSData *reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:nil];
    
    NSError* error;
    NSDictionary* jsonReply = [NSJSONSerialization
                          JSONObjectWithData:reply
                          options:kNilOptions
                          error:&error];
    
    Listener* listener = [self createListenerFromJson: [jsonReply objectForKey:@"listener"]];
    
    if (![externalId isEqualToString:listener.externalId])
    {
        [NSException raise:@"Invalid Listener id returned" format:@"Id %@ is not %@", externalId, listener.externalId];
    }
    
    if (!listener.image.image)
    {
        listener.image = [self getImage:listener.image];
    }

    NSArray* children = [self createChildrenFromJson: [jsonReply objectForKey:@"children"]];
    
    [listener setChildren:children];
    
    return listener;
}

- (Listener*)changeListener:(Listener *)listener{
    
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingString: [NSString stringWithFormat:@"listener%@",
                                                          listener.externalId ? [NSString stringWithFormat: @"/%@", listener.externalId] : @""]]];
    
    NSError *error = nil;
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    [dictionary setObject:[NSDictionary dictionaryWithObject:self.deviceId forKey:@"id"] forKey:@"device"];
    
    if (listener.image != NULL && listener.image.image != NULL)
    {
        [listener setImage: [self postImage: listener.image]];
        if (listener.image != nil)
        {
            
            [dictionary setObject:[NSDictionary dictionaryWithObject:listener.image.externalId forKey:@"id"] forKey:@"image"];
        }
    }
    
    if (listener.password != nil)
        [dictionary setObject:listener.password forKey:@"password"];
    
    [dictionary setObject:listener.name forKey:@"name"];
    [dictionary setObject:listener.email forKey:@"email"];
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:dictionary options:0 error:&error];
    
    if (error){
        NSLog(@"%@", error);
    }

    id result = [NSJSONSerialization JSONObjectWithData:postData options:NSJSONReadingMutableContainers error:&error];
    
    NSLog(@"%@",result);
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSData *reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error)
    {
        NSLog(@"Error changeListener: %@", error.description);
        NSLog(@"Error: %@", error);
        return nil;
    }
    
    NSDictionary* jsonReply = [NSJSONSerialization
                               JSONObjectWithData:reply
                               options:kNilOptions
                               error:&error];
    if (error)
    {
        NSLog(@"Error changeListener (json): %@", error.description);
        return nil;
    }
    
    return [self createListenerFromJson: jsonReply];

    
}

-(void)deleteListener:(Listener*)listener
{}

-(void)replyToTeachWithListener:(Listener*)listener
{}

-(void)thankYouWithListener:(Listener*)listener andTeacherId:(NSString*)teacherId
{}

// ------------------ // Listener's actions // --------------------- //

-(NSArray*)getChildrenWithListener:(Listener*)listener
{
    listener = [self getListenerWithExternalId:listener.externalId];

    return listener.children;
}

-(void)introduceKindergartenWithListener:(Listener*)listener
           andKindergartenIntroduction:(KindergartenIntroduction*)kindergartenIntroduction
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"intro-kindergarten-by/%@/", listener.externalId]];
    
    NSString * deviceJson = [self getDeviceJson];
    
    NSLog(@"introduceKindergartenWithListener URL = %@", url);
    NSString *json = [NSString stringWithFormat:@"{ %@, \"phone\": \"%@\", \"email\": \"%@\", \"website\": \"%@\", \"address\": \"%@\", \"listenerPhoneNumber\": \"%@\" }",
                      deviceJson,
                      kindergartenIntroduction.phone,
                      kindergartenIntroduction.email,
                      kindergartenIntroduction.website,
                      kindergartenIntroduction.address,
                      kindergartenIntroduction.listerPhoneNumber];
    
    NSLog(@"POST=%@", json);
    NSData *postData = [json dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError* error;
    [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error)
    {
        NSLog(@"Error: %@", error.description);
    }
}

// ------------------ // feedback // --------------------- //
-(void)provideFeedbackWithListener:(Listener*)listener andFeedback:(Feedback*)feedback
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"feedback/listener/%@/", listener.externalId]];
    
    NSString * deviceJson = [self getDeviceJson];
    
    NSLog(@"provideFeedbackWithListener URL = %@", url);
    NSString *json = [NSString stringWithFormat:@"{ %@, \"text\": \"%@\", \"source\": \"%@\" }",
                      deviceJson,
                      feedback.text,
                      @"iOSParentClient"];
    
    NSLog(@"POST=%@", json);
    NSData *postData = [json dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:url];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:postData];
    
    NSURLResponse *response;
    NSError * error;
    NSData *reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error)
    {
        NSLog(@"Error call: %@", error.description);
    }
    
    NSDictionary* jsonReply = [NSJSONSerialization
                               JSONObjectWithData:reply
                               options:kNilOptions
                               error:&error];
    if (error)
    {
        NSLog(@"Error json parse: %@", error.description);
    }
    
    NSLog(@"Response Id: %@", [jsonReply valueForKey:@"id"]);
}

-(void)replyToTeacherWithListener:(Listener*)listener
{}

// images
-(Image *)postImage:(Image*)image
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingString: [NSString stringWithFormat:@"image/%@%@", self.deviceId,
                                                          image.externalId ? [NSString stringWithFormat: @"/%@", image.externalId] : @""]]];
    
    NSLog(@"URL= %@", url);
    
    // create request
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // the boundary string : a random string, that will not repeat in post data, to separate post data fields.
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";

    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // Dictionary that holds post parameters. You can set your post parameters that your server accepts or programmed to accept.
    NSMutableDictionary* _params = [[NSMutableDictionary alloc] init];
    [_params setObject:@"1.0" forKey:@"ver"];
    [_params setObject:@"en" forKey:@"lan"];
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    NSString* FileParamConstant = @"upload";
    
    // add params (all params are strings)
    for (NSString *param in _params) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [_params objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    // add image data
    NSData *imageData = UIImageJPEGRepresentation(image.image, 1.0f);
    if (imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"i\"\r\n", FileParamConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    }
    
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    
    // setting the body of the post to the reqeust
    [request setHTTPBody:body];
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%d", [body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    // set URL
    [request setURL:url];
    
    NSURLResponse *response;
    NSError* error;
    NSData *reply = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error)
    {
        NSLog(@"Error image post: %@", error.description);
        return nil;
    }
    
    NSString* s = [[NSString alloc] initWithData:reply encoding:NSUTF8StringEncoding];
    
    NSLog(@"==> %@", s);
    
    NSDictionary* jsonReply = [NSJSONSerialization
                               JSONObjectWithData:reply
                               options:kNilOptions
                               error:&error];
    if (error)
    {
        NSLog(@"Error image post (json): %@", error.description);
        return nil;
    }
    
    [self setProperties:jsonReply forImage:image];
    
    return image;
}

-(Image*)getImage:(Image*)image
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"image/%@", image.externalId]];
    
    NSLog(@"URL getImageByExternalId = %@", url);

    NSData* data = [NSData dataWithContentsOfURL: url];
    
    image.image = [[UIImage alloc] initWithData:data];
    
    return image;
}

-(Image*)getThumb:(Image*)image
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"thumb/%@", image.externalId]];
    
    NSLog(@"URL getThumbByExternalId = %@", url);
    
    NSData* data = [NSData dataWithContentsOfURL: url];
    
    image.image = [[UIImage alloc] initWithData:data];
    
    return image;
}

-(void)setProperties:(NSDictionary *) json forImage: (Image*) image
{
    [image setExternalId:[json objectForKey:@"id"]];
    [image setName:[json objectForKey:@"name"]];
    
    NSLog(@"Set properties for image[id=%@,name=%@]", image.externalId, image.name);
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

// Device
-(void)registerDevice:(NSString*)deviceId forPushNotificationWithToken: (NSString*)deviceToken isEnabled:(BOOL)enabled
{
    NSURL *url = [[NSURL alloc] initWithString:
                  [self.baseUrl stringByAppendingFormat: @"push/%@/platform/ios/with-device-token/%@/%d/",
                   deviceId, deviceToken, enabled]];
    
    NSLog(@"registerDeviceForPushNotification URL= %@", url);
    
    [NSData dataWithContentsOfURL: url];
}

@end
