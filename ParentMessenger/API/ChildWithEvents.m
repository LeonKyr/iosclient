//
//  ChildWithEvents.m
//  ParentMessenger
//
//  Created by Leo on 01/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "ChildWithEvents.h"

@implementation ChildWithEvents

@synthesize child;
@synthesize events;

-(void)dealloc
{
    self.child = nil;
    self.events = nil;
}

@end
