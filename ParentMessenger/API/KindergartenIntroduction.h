//
//  KindergartenIntroduction.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 22/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KindergartenIntroduction : NSObject

@property (nonatomic, strong) NSString * email;
@property (nonatomic, strong) NSString * address;
@property (nonatomic, strong) NSString * website;
@property (nonatomic, strong) NSString * phone;
@property (nonatomic, strong) NSString * listerPhoneNumber;

@end
