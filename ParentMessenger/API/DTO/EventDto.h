//
//  EventDto.h
//  ParentMessenger
//
//  Created by Leo on 03/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ActionType.h"
#import "Teacher.h"
#import "EventPropertyValue.h"


@interface EventDto : NSObject

@property (nonatomic, strong) NSString * externalId;
@property (nonatomic, assign) ActionType actionType;
@property (nonatomic, assign) NSString * createdAt;

@property (nonatomic, strong) NSString * day;
@property (nonatomic, strong) Teacher* teacher;
@property (nonatomic, strong) NSArray * properties;

@end
