//
//  EventDto.m
//  ParentMessenger
//
//  Created by Leo on 03/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "EventDto.h"
#import "ActionType.h"
#import "Teacher.h"
#import "EventPropertyValue.h"

@implementation EventDto

@synthesize externalId;
@synthesize createdAt;

@end
