//
//  EventPropertyValueDto.h
//  ParentMessenger
//
//  Created by Leo on 03/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EventPropertyValueDto : NSObject

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSString* value;
@property (nonatomic, retain) NSArray* valueBag;
@property (nonatomic) BOOL isBag;

@end
