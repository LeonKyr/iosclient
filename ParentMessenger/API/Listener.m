//
//  Listener
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "Listener.h"

@implementation Listener

@synthesize image;

-(Listener*)initWithName:(NSString*)name andEmail:(NSString*)email andExternalId:(NSString*)externalId
{
    self = [super init];
    
    if (self)
    {
        [self setName:name];
        [self setEmail:email];
        [self setExternalId:externalId];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    [encoder encodeObject:self.name forKey:@"listener_name"];
    [encoder encodeObject:self.externalId forKey:@"listener_externalId"];
    [encoder encodeObject:self.email forKey:@"listener_email"];
    [encoder encodeObject:self.children forKey:@"listener_children"];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        //decode properties, other class vars
        self.name = [decoder decodeObjectForKey:@"listener_name"];
        self.externalId = [decoder decodeObjectForKey:@"listener_externalId"];
        self.email = [decoder decodeObjectForKey:@"listener_email"];
        self.children = [decoder decodeObjectForKey:@"listener_children"];
    }
    return self;
}

@end
