//
//  NoteEvent.h
//  ParentMessenger
//
//  Created by Leo on 24/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "Event.h"

@interface NoteEvent : Event

//@property (nonatomic, retain) NSSet* event2EventPropertyValue;
@property (nonatomic, retain) NSArray* properties;

-(NSString*) getMessage;

// INIT
-(Event*)init;
-(Event*)initWithEventId:(NSString*)eventId andActionType: (NSNumber*) actionType;

@end
