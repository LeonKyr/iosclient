//
//  ListenerMessengerApi.h
//  ListenerMessenger
//
//  Created by Leonid Kyrpychenko on 12/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Listener.h"
#import "Child.h"
#import "Feedback.h"
#import "Kindergarten.h"
#import "KindergartenIntroduction.h"
#import "EventFactory.h"
#import "Image.h"

@interface ParentMessengerApi : NSObject

@property (strong) EventFactory *eventFactory;
@property (nonatomic, strong) NSString *baseUrl;
@property (nonatomic, strong) NSString *deviceId;

+(ParentMessengerApi*)create;

-(ParentMessengerApi*)initWithDeviceId:(NSString*) deviceId;

// event related methods
-(NSArray*)getEventsWithListener:(Listener*)listener andCreatedAt:(NSDate*)createdAt;

// account methods
-(Listener*)loginWithEmail:(NSString*)email andPassword:(NSString*)password;
-(Listener*)registerWithName:(NSString*)name andEmail:(NSString*)email andPassword:(NSString*)password andImage:(UIImage*)image;
-(void)changePasswordWithExternalId:(NSString*)externalId andNewPassword:(NSString*)newPassword;

- (void)forgotPasswordWithEmail:(NSString*)email andResultCompletion:(void (^)(NSURLResponse *response, NSData *data, NSError *connectionError))completion;

// Listener
-(Listener*)changeListener:(Listener*)listener;
-(Child*)addChild:(NSString*)childCode toListener:(Listener*)listener;
-(Listener*)getListenerWithExternalId:(NSString*)externalId;
-(void)deleteListener:(Listener*)listener;
-(void)replyToTeacherWithListener:(Listener*)listener;
-(void)thankYouWithListener:(Listener*)Listener andTeacherId:(NSString*)teacherId;

// Device
-(void)registerDevice:(NSString*)deviceId forPushNotificationWithToken: (NSString*)deviceToken isEnabled:(BOOL)enabled;

// Listener's actions
-(NSArray*)getChildrenWithListener:(Listener*)listener;
-(void)introduceKindergartenWithListener:(Listener*)listener
                       andKindergartenIntroduction:(KindergartenIntroduction*)kindergartenIntroduction;

// feedback
-(void)provideFeedbackWithListener:(Listener*)Listener andFeedback:(Feedback*)feedback;

// images
-(Image*)postImage:(Image*)image;
-(Image*)getImage:(Image*)image;
-(Image*)getThumb:(Image*)image;

@end
