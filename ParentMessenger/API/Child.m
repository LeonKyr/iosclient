//
//  Child.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "Child.h"

@implementation Child

- (void)encodeWithCoder:(NSCoder *)encoder {
    //Encode properties, other class variables, etc
    NSString * prefix = [NSString stringWithFormat:@"%@_%@_", @"child", self.externalId];
    [encoder encodeObject:self.name forKey:[NSString stringWithFormat:@"%@%@", prefix, @"name"]];
    [encoder encodeObject:self.externalId forKey:[NSString stringWithFormat:@"%@%@", prefix, @"externalId"]];
}

- (id)initWithCoder:(NSCoder *)decoder {
    if((self = [super init])) {
        NSString * prefix = [NSString stringWithFormat:@"%@_%@_", @"child", self.externalId];
        //decode properties, other class vars
        self.name = [decoder decodeObjectForKey:[NSString stringWithFormat:@"%@%@", prefix, @"name"]];
        self.externalId = [decoder decodeObjectForKey:[NSString stringWithFormat:@"%@%@", prefix, @"external_id"]];
    }
    return self;
}

@end