//
//  ParentFactory.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 22/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "ListenerFactory.h"
#import "Listener.h"

@implementation ListenerFactory

-(Listener*)createName:(NSString*)name andEmail:(NSString*)email andExternalId:(NSString*)externalId andImage:(Image*)image andPassword:(NSString*)password
{
    Listener * result = [[Listener alloc] initWithName:name andEmail:email andExternalId: externalId];
    
    if (image)
    {
        [result setImage:image];
    }
    
    if (password)
    {
        [result setPassword:password];
    }
    
    return result;
}


@end
