//
//  EventPropertyValue.h
//  ParentMessenger
//
//  Created by Leo on 02/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "EventPropertyValueBag.h"

@class EventPropertyValueBag;


@interface EventPropertyValue : NSManagedObject

@property (nonatomic, retain) NSString* name;
@property (nonatomic, retain) NSString* type;
@property (nonatomic, retain) NSString* value;
@property (nonatomic) BOOL is_bag;

@property (nonatomic, retain) NSSet* eventProperty2EventPropertyValueBag;

@end

@interface EventPropertyValue (CoreDataGeneratedAccessors)

- (void)addeventProperty2EventPropertyValueBagObject:(EventPropertyValueBag *)value;
- (void)removeeventProperty2EventPropertyValueBagObject:(EventPropertyValueBag *)value;
- (void)addeventProperty2EventPropertyValueBag:(NSSet *)value;
- (void)removeeventProperty2EventPropertyValueBag:(NSSet *)value;

@end
