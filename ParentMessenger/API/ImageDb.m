//
//  ImageDb.m
//  ParentMessenger
//
//  Created by Leo on 03/02/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "ImageDb.h"

@implementation ImageDb

@synthesize thumb;
@synthesize path;
@synthesize createdAt;
@synthesize externalId;


-(NSData*)thumb
{
    [self willAccessValueForKey:@"thumb"];
    NSData *value = (NSData *)[self valueForKey:@"thumb"];
    [self didAccessValueForKey:@"thumb"];
    
    return value;
}

-(NSString*)path
{
    [self willAccessValueForKey:@"path"];
    NSString* value = (NSString *)[self valueForKey:@"path"];
    [self didAccessValueForKey:@"path"];
    
    return value;
}

-(NSString*)externalId
{
    [self willAccessValueForKey:@"external_id"];
    NSString* value = (NSString *)[self valueForKey:@"external_id"];
    [self didAccessValueForKey:@"external_id"];
    
    return value;
}

-(NSNumber*)createdAt
{
    [self willAccessValueForKey:@"created_at"];
    NSNumber* value = (NSNumber *)[self valueForKey:@"created_at"];
    [self didAccessValueForKey:@"created_at"];
    
    return value;
}

@end
