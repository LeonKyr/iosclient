//
//  KindergartenIntroduction.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 22/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "KindergartenIntroduction.h"

@implementation KindergartenIntroduction

@synthesize email;
@synthesize website;
@synthesize address;
@synthesize phone;
@synthesize listerPhoneNumber;

@end
