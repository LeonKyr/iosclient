//
//  Event.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "ActionType.h"
#import "Teacher.h"
#import "EventPropertyValue.h"
#import "Child.h"

@class EventPropertyValue;

@interface Event : NSManagedObject

// properties
@property (nonatomic, strong) NSString * externalId;
@property (nonatomic, assign) NSNumber * actionType;
@property (nonatomic, strong) NSString * createdAt;
@property (nonatomic, strong) NSString * day;
@property (nonatomic, strong) NSString * teacherName;

@property (nonatomic, retain) NSSet* event2EventProperty;

//@property (nonatomic, strong) NSArray * properties;
@property (nonatomic, retain) NSString * childId;
@property (nonatomic, strong) NSString *child_ext_image;

@property (nonatomic, strong) Event* next;
@property (nonatomic, strong) Event* prev;

// init
-(Event*)init;
-(Event*)initWithEventId:(NSString*)eventId andActionType: (NSNumber* ) actionType;
// methods
-(NSString*) createdAtAsString;
-(NSString*) propertyValueByName:(NSString*)propertyName;

@end

@interface Event (CoreDataGeneratedAccessors)

- (void)addevent2EventPropertyObject:(EventPropertyValue *)value;
- (void)removeevent2EventPropertyObject:(EventPropertyValue *)value;
- (void)addevent2EventProperty:(NSSet *)value;
- (void)removeevent2EventProperty:(NSSet *)value;

@end
