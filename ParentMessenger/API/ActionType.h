//
//  ActionType.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#ifndef ParentMessenger_ActionType_h
#define ParentMessenger_ActionType_h

typedef enum ActionType
{
    NOTE = 1,
    SINGIN = 2,
    PHOTOS = 3,
    SINGOUT = 4,
    SLEEP = 5,
    MEAL = 6,
    CHILD_WILL_HAVE_MEAL = 8,
    CHILD_ABSENCE = 9
} ActionType;

#endif
