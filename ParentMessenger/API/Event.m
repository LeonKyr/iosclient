//
//  Event.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "Event.h"
#import "EventPropertyValue.h"

@implementation Event

@dynamic externalId;
@dynamic actionType;
@dynamic createdAt;
@dynamic day;
@dynamic teacherName;
@dynamic childId;
@dynamic child_ext_image;

@synthesize next;
@synthesize prev;

@dynamic event2EventProperty;

- (NSString *)createdAtAsString{
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate *date = [dateFormat dateFromString:self.createdAt];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"HH:mm"];
//    [dateFormatter setTimeStyle:NSDateFormatterShortStyle];
    return [dateFormatter stringFromDate:date];
}

-(Event*)init
{
    return [super init];
}

-(Event*)initWithEventId:(NSString*)externalId andActionType: (NSNumber*) actionType
{
    self = [super init];
    
    if (self)
    {
        self.externalId = externalId;
        self.actionType = actionType;
    }
    
    return self;
}

-(NSString*)externalId
{
    [self willAccessValueForKey:@"id"];
    NSString *value = (NSString *)[self valueForKey:@"id"];
    [self didAccessValueForKey:@"id"];

    return value;
}

-(NSNumber*)actionType
{
    [self willAccessValueForKey:@"action"];
    NSNumber* value = (NSNumber *)[self valueForKey:@"action"];
    [self didAccessValueForKey:@"action"];
    
    return value;
}

- (NSString *)child_ext_image{
    
    [self willAccessValueForKey:@"child_ext_id_image"];
    NSString* value = (NSString *)[self valueForKey:@"child_ext_id_image"];
    [self didAccessValueForKey:@"child_ext_id_image"];
    return value;
}

-(NSString*) propertyValueByName:(NSString*)propertyName
{
    for (EventPropertyValue * epv in [self event2EventProperty])
    {
        if ([epv.name isEqualToString:propertyName])
            return epv.value;
    }
    
    return nil;
}

- (NSString*)childId{
    
    [self willAccessValueForKey:@"child_id"];
    NSString* value = (NSString *)[self valueForKey:@"child_id"];
    [self didAccessValueForKey:@"child_id"];
    
    return value;
}

-(NSString*) teacherName
{
    [self willAccessValueForKey:@"teacher_name"];
    NSString* value = (NSString *)[self valueForKey:@"teacher_name"];
    [self didAccessValueForKey:@"teacher_name"];
    
    return value;
}


@end