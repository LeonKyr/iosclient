//
//  EventPropertyValueBag.h
//  ParentMessenger
//
//  Created by Leo on 01/02/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface EventPropertyValueBag : NSManagedObject

@property (nonatomic, retain) NSString * value;

@end
