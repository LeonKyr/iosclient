//
//  Image.h
//  ParentMessenger
//
//  Created by Leo on 29/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Image : NSObject

@property (nonatomic, strong) UIImage* image;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* externalId;
@property (nonatomic, strong) NSString* lastUpdatedByDeviceId;

-(Image*)initWithExternalId:(NSString*)theExternalId;

@end
