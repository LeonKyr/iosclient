//
//  NoteEvent.m
//  ParentMessenger
//
//  Created by Leo on 24/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "NoteEvent.h"

@implementation NoteEvent

//@dynamic event2EventPropertyValue;
@dynamic properties;

-(Event*)init
{
    return [super init];
}

-(Event*)initWithEventId:(NSString*)eventId andActionType: (NSNumber*) actionType
{
    return [super initWithEventId:eventId andActionType:actionType];
}

-(NSString*) getMessage
{
    NSString* result = @"";
    
    NSLog(@"Properties count = %d",  [self.event2EventProperty count]);
    
    for (EventPropertyValue * epv in self.event2EventProperty)
    {
        if ([epv.name isEqualToString:@"message"])
        {
            result = epv.value;
        }
    }
    
    return result;
}

@end
