//
//  Image.m
//  ParentMessenger
//
//  Created by Leo on 29/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "Image.h"

@implementation Image

@synthesize externalId;
@synthesize name;
@synthesize image;
@synthesize lastUpdatedByDeviceId;

-(Image*)initWithExternalId:(NSString*)theExternalId
{
    self = [super init];
    
    if (self)
    {
        [self setExternalId:theExternalId];
    }
    
    return self;
}

@end
