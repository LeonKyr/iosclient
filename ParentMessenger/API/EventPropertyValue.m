//
//  EventPropertyValue.m
//  ParentMessenger
//
//  Created by Leo on 02/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "EventPropertyValue.h"

@implementation EventPropertyValue

@synthesize name;
@synthesize type;
@synthesize value;
@synthesize is_bag;

@dynamic eventProperty2EventPropertyValueBag;

@end
