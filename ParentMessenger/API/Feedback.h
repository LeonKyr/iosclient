//
//  Feedback.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 22/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Feedback : NSObject

@property (nonatomic, strong) NSString* text;

@end
