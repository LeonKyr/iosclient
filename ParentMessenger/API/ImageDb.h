//
//  ImageDb.h
//  ParentMessenger
//
//  Created by Leo on 03/02/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <CoreData/CoreData.h>

@interface ImageDb : NSManagedObject

@property (nonatomic, retain) NSData * thumb;
@property (nonatomic, retain) NSString * path;
@property (nonatomic, retain) NSString * externalId;
@property (nonatomic, retain) NSNumber * createdAt;

@end
