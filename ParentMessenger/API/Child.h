//
//  Child.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 15/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "Image.h"

@interface Child : NSObject <NSCoding>

@property (nonatomic, retain) NSString* externalId;
@property (nonatomic, retain) NSString* code;
@property (nonatomic, retain) NSString* name;

@property (nonatomic, strong) Image* image;

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSDate * dataOfBirth;

@end