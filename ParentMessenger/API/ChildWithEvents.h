//
//  ChildWithEvents.h
//  ParentMessenger
//
//  Created by Leo on 01/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Child.h"
#import "Event.h"

@interface ChildWithEvents : NSObject

@property (nonatomic, retain) Child* child;
@property (nonatomic, retain) NSArray* events;

@end
