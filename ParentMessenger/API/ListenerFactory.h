//
//  ListenerFactory.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 22/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Listener.h"

@protocol ListenerFactoryProtocol <NSObject>

-(Listener*)createName:(NSString*)name andEmail:(NSString*)email andExternalId:(NSString*)externalId andImage:(Image*)image andPassword:(NSString*)password;

@end


@interface ListenerFactory : NSObject<ListenerFactoryProtocol>

-(Listener*)createName:(NSString*)name andEmail:(NSString*)email andExternalId:(NSString*)externalId andImage:(Image*)image andPassword:(NSString*)password;


@end
