//
//  Listener.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Listener.h"
#import "Image.h"

@interface Listener : NSObject <NSCoding>

@property (nonatomic, strong) NSString* externalId;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* email;
@property (nonatomic, strong) NSString* password;
@property (nonatomic, strong) NSArray* children;
@property (nonatomic, strong) Image* image;

-(Listener*)initWithName:(NSString*)name andEmail:(NSString*)email andExternalId:(NSString*)externalId;

@end
