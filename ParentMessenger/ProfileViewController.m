//
//  ProfileViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 18/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "ProfileViewController.h"
#import "ParentMessengerApi.h"
#import "Listener.h"
#import "AppDelegate.h"
#import <MobileCoreServices/UTCoreTypes.h>

@interface ProfileViewController ()

@end

@implementation ProfileViewController

@synthesize saveButton;
@synthesize changePasswordButton;

- (void)setTextField:(UITextField*)textfield topBorder:(BOOL)topEnabled buttomBorder:(BOOL)butEnabled{
    
    textfield.delegate = self;
    
    textfield.backgroundColor = [UIColor whiteColor];
    
    textfield.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
    textfield.textColor = [UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1.0f];
    
    
    textfield.leftView = ({
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, 20.0f, textfield.frame.size.height)];
        view.backgroundColor = [UIColor clearColor];
        
        view;
        
    });
    textfield.leftViewMode = UITextFieldViewModeAlways;
    
    UIColor *borderColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];
    
    if (topEnabled){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, textfield.frame.size.width, 1.0f)];
        view.backgroundColor = borderColor;
        [textfield addSubview:view];
    }
    
    if (butEnabled){
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, textfield.frame.size.height - 1.0f, textfield.frame.size.width, 1.0f)];
        view.backgroundColor = borderColor;
        [textfield addSubview:view];
    }
}

- (void)setButton:(UIButton*)button{
    
    button.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:204.0f/255.0f blue:204.0f/255.0f alpha:1.0f];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.name.delegate = self;
    self.email.delegate = self;
    
    [self setTextField:self.name topBorder:YES buttomBorder:YES];
    [self setTextField:self.email topBorder:NO buttomBorder:YES];
    
    [self setButton:self.saveButton];
    [self setButton:self.changePasswordButton];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                                                        green:178.0f/255.0f
                                                                         blue:178.0f/255.0f
                                                                        alpha:1.0f];
    

    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    Listener * listener = appDelegate.model.currentListener;
    if (listener.image)
    {
        self.photoImageView.image = listener.image.image;
        [self.touchToSelectPhotoLabel setHidden:YES];
    }
    else
    {
        self.photoImageView.image = [UIImage imageNamed:@"no_image"];
        
        [self.touchToSelectPhotoLabel setHidden:NO];
    }
    
    [self setupImageView];
    
    [self.name setText:listener.name];
    [self.email setText:listener.email];
}

-(void)setupImageView
{
//    self.photoImageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    self.photoImageView.autoresizingMask = UIViewAutoresizingNone;
    self.photoImageView.layer.masksToBounds = YES;
    self.photoImageView.layer.cornerRadius = 45.0;
    self.photoImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.photoImageView.layer.borderWidth = 3.0f;
    self.photoImageView.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.photoImageView.layer.shouldRasterize = YES;
    self.photoImageView.clipsToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [touches anyObject];
    
    if ([touch view] == self.photoImageView)
    {
        [self showPhotoLibary];
    }
    
}

- (void)showPhotoLibary
{
    if (([UIImagePickerController isSourceTypeAvailable:
          UIImagePickerControllerSourceTypeSavedPhotosAlbum] == NO)) {
        return;
    }
    
    UIImagePickerController *mediaUI = [[UIImagePickerController alloc] init];
    mediaUI.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    // Displays saved pictures from the Camera Roll album.
    mediaUI.mediaTypes = @[(NSString*)kUTTypeImage];
    
    // Hides the controls for moving & scaling pictures
    mediaUI.allowsEditing = YES;
    
    mediaUI.delegate = self;
    
    [self.navigationController presentViewController:mediaUI animated:YES completion:nil];
}

- (void) imagePickerController: (UIImagePickerController *) picker didFinishPickingMediaWithInfo: (NSDictionary *) info {
    
    UIImage *image = (UIImage *) [info objectForKey:UIImagePickerControllerEditedImage];
    self.photoImageView.image = image;
    [self.touchToSelectPhotoLabel setHidden:YES];
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)save:(id)sender
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    if (!appDelegate.model.currentListener.image)
    {
        appDelegate.model.currentListener.image = [[Image alloc] init];
    }
    appDelegate.model.currentListener.image.image = self.photoImageView.image;

    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ParentMessengerApi* api = [ParentMessengerApi create];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        Listener * listener = appDelegate.model.currentListener;
        listener.name = self.name.text;
        listener.email = self.email.text;
//        listener.image.image = self.photoImageView.image;

        listener = [api changeListener:listener];
        
        if (listener)
        {
            NSLog(@"Profile Change=%@", listener.externalId);
            
            appDelegate.model.currentListener = listener;
            
            [self performSelectorOnMainThread:@selector(fetchedData:)
                                   withObject:listener waitUntilDone:YES];
        }
    });
}


- (void)fetchedData:(Listener *)listener {
    
    if (listener)
    {
        NSLog(@"LISTENERID=%@", listener.externalId);
        NSUserDefaults *settings = [NSUserDefaults standardUserDefaults];
        [settings setObject:listener.externalId forKey:@"listenerExternalId"];
        [settings synchronize];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        appDelegate.model.currentListener = listener;
        [appDelegate.model.currentListener.image setImage:self.photoImageView.image];
  
        [self dismissViewControllerAnimated:YES completion:nil];
//        [self performSegueWithIdentifier:@"registerSuccessfulSegue" sender:self];
    }
    else
    {
        UIAlertView * view = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"Error", nil)
                              message:NSLocalizedString(@"Registration failed, please try again.", nil)
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"Cancel", nil)
                              otherButtonTitles:nil, nil];
        [view show];
    }
}

- (IBAction)changePassword:(id)sender {
}

@end
