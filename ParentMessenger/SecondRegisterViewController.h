//
//  SecontRegisterViewController.h
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "BaseRegisterViewController.h"

@interface SecondRegisterViewController : BaseRegisterViewController<UINavigationControllerDelegate, UIImagePickerControllerDelegate, UIActionSheetDelegate>

@property (nonatomic, weak) IBOutlet UITextField *firstName;
@property (nonatomic, weak) IBOutlet UITextField *secondName;

@property (nonatomic, weak) IBOutlet UIButton *photoButton;

- (IBAction)setPhoto:(id)sender;

@end