//
//  MainTabBarController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 11/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "MainTabBarController.h"
#import "NavigationMenuViewController.h"
#import "EventsTableViewController.h"
#import "SyncEngine.h"
#import "AppDelegate.h"

@interface MainTabBarController ()

@property (nonatomic, strong) UIBarButtonItem *item;

@end

@implementation MainTabBarController

@synthesize item;
@synthesize rightItem;

- (void)makeNavigationItem{
    
    self.item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"indent-left"]
                                                 style:UIBarButtonItemStyleBordered
                                                target:self
                                                action:@selector(showMenu:)];
    self.item.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                          green:178.0f/255.0f
                                           blue:178.0f/255.0f
                                          alpha:1.0f];
    self.navigationItem.leftBarButtonItem = self.item;
    /*
    self.rightItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"refresh"]
                                                                  style:UIBarButtonItemStyleBordered
                                                                 target:nil
                                                                 action:nil];
    self.rightItem.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                          green:178.0f/255.0f
                                           blue:178.0f/255.0f
                                          alpha:1.0f];*/
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [[UITabBar appearance] setSelectedImageTintColor:[UIColor colorWithRed:15.0f/255.0f
                                                                     green:128.0f/255.0f
                                                                      blue:121.0f/255.0f
                                                                     alpha:1.0f]];
    
    [self showTabBar:self.tabBarController];
    
    self.title = @"BooGooGoo";
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegate requestPermissionsForPushNotification];
    
    [self makeNavigationItem];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Method implementations
- (void)hideTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 480, view.frame.size.width, view.frame.size.height)];
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 480)];
        }
    }
    
    [UIView commitAnimations];
}

- (void)showTabBar:(UITabBarController *) tabbarcontroller
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    for(UIView *view in tabbarcontroller.view.subviews)
    {
        NSLog(@"%@", view);
        
        if([view isKindOfClass:[UITabBar class]])
        {
            [view setFrame:CGRectMake(view.frame.origin.x, 431, view.frame.size.width, view.frame.size.height)];
            
        }
        else
        {
            [view setFrame:CGRectMake(view.frame.origin.x, view.frame.origin.y, view.frame.size.width, 431)];
        }
    }
    
    [UIView commitAnimations];
}

-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleDefault;
}

- (IBAction)showMenu:(id)sender {
    [self.frostedViewController presentMenuViewController];
}

- (IBAction)refresh:(id)sender {

    NSLog(@"Before sync");
    [[SyncEngine sharedEngine] startSync];
    NSLog(@"After sync");
    
}
@end
