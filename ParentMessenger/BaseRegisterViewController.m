//
//  BaseLoginViewController.m
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "BaseRegisterViewController.h"

@interface BaseRegisterViewController ()

@end

@implementation BaseRegisterViewController

@synthesize nextItem;
@synthesize backItem;
@synthesize stepView;

@synthesize helperRegister;

#define labelTag 100

#pragma mark - touch delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark - IBAction methods

- (IBAction)backMethod:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextMethod:(id)sender{
    
    [self.view endEditing:YES];
}

#pragma mark - UITextField delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    return [textField resignFirstResponder];
}

#pragma mark - CustomizeUI methods

- (UILabel*)getLabelForTextField:(UITextField*)textField{
    
     return (UILabel*)[textField viewWithTag:labelTag];
}

- (void)setErrorColorLabelForTextField:(UITextField*)textField{
    
    textField.backgroundColor = [UIColor redColor];
//    UILabel *label = [self getLabelForTextField:textField];
//    [label setTextColor:[UIColor redColor]];
}

- (void)setNormalColorLabelForTextField:(UITextField*)textField{
    
    textField.backgroundColor = [UIColor clearColor];
//    UILabel *label = [self getLabelForTextField:textField];
//    [label setTextColor:[UIColor colorWithRed:201.0f/255.0f
//                                       green:201.0f/255.0f
//                                        blue:201.0f/255.0f
//                                        alpha:1.0f]];
}

- (void)setColorForLabelInTextField:(UITextField *)textField forValue:(BOOL)value{
    
    if (value)
        [self setNormalColorLabelForTextField:textField];
    else
        [self setErrorColorLabelForTextField:textField];
}

- (void)customizeTextField:(UITextField *)textField andPlaceHolderText:(NSString *)text andWidth:(CGFloat)width{
    
    textField.delegate = self;
    
    textField.font = [UIFont fontWithName:@"Tahoma" size:17.5];
    textField.backgroundColor = [UIColor clearColor];
    textField.tintColor = [UIColor colorWithRed:201.0f/255.0f
                                          green:201.0f/255.0f
                                           blue:201.0f/255.0f
                                          alpha:1.0f];
    textField.textColor = [UIColor colorWithRed:131.0f/255.0f
                                          green:131.0f/255.0f
                                           blue:131.0f/255.0f
                                          alpha:1.0f];
    
    UIColor *borderColor = [UIColor colorWithRed:217.0f/255.0f
                                           green:217.0f/255.0f
                                            blue:217.0f/255.0f
                                           alpha:1.0f];
    UIView *viewBottomLine = [[UIView alloc] initWithFrame:CGRectMake(0.0f, textField.frame.size.height - 1.0f, textField.frame.size.width, 1.0f)];
    viewBottomLine.backgroundColor = borderColor;
    [textField addSubview:viewBottomLine];
    textField.placeholder = text;
    
    textField.leftView = ({UIView *view = [[UIView alloc]
                                            initWithFrame:CGRectMake(0.0f, 0.0f, 10.0f, textField.frame.size.height)];
        view;
    });

//    CGFloat offset = 15.0f;
//
//    textField.leftView = ({
//    
//        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(offset, 0.0f, width - offset, textField.frame.size.height)];
//        label.tag = labelTag;
//        label.text = text;
//        label.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
//        label.textColor = [UIColor colorWithRed:201.0f/255.0f
//                                          green:201.0f/255.0f
//                                           blue:201.0f/255.0f
//                                          alpha:1.0f];
//        label.textAlignment = NSTextAlignmentLeft;
//        label.backgroundColor = [UIColor clearColor];
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, width, textField.frame.size.height)];
//        view.backgroundColor = [UIColor clearColor];
//        [view addSubview:label];
//        
//        view;
//        
//    });
    
    textField.leftView.userInteractionEnabled = NO;
    
    textField.leftViewMode = UITextFieldViewModeAlways;
}

- (void)customizeUI{
    
    self.nextItem.tintColor = [UIColor colorWithRed:15.0f/255.0f
                                              green:128.0f/255.0f
                                               blue:121.0f/255.0f
                                              alpha:1.0f];
    
    self.backItem.tintColor = [UIColor colorWithRed:15.0f/255.0f
                                              green:128.0f/255.0f
                                               blue:121.0f/255.0f
                                              alpha:1.0f];
    
    UIFont *font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    
    [self.nextItem setTitleTextAttributes:@{NSFontAttributeName: font} forState:UIControlStateNormal];
    [self.backItem setTitleTextAttributes:@{NSFontAttributeName: font} forState:UIControlStateNormal];
    
    
    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UINavigationBar appearance] titleTextAttributes]];
    [titleBarAttributes setValue:[UIFont fontWithName:@"Tahoma" size:17.5] forKey:NSFontAttributeName];
    [titleBarAttributes setValue:[UIColor blackColor] forKey:NSForegroundColorAttributeName];
    [[UINavigationBar appearance] setTitleTextAttributes:titleBarAttributes];
}

#pragma mark - view life cycle

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = NO;
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    self.navigationController.navigationBar.barStyle = UIBarStyleDefault;
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.backgroundColor = [UIColor whiteColor];
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    self.backItem.target = self;
    self.backItem.action = @selector(backMethod:);
    
    self.nextItem.target = self;
    self.nextItem.action = @selector(nextMethod:);
    
    self.title = NSLocalizedString(@"Register", nil);
    
    self.helperRegister = [HelperRegister sharedHelper];
    
    [self customizeUI];
}

@end