//
//  ActionTypeConverter.h
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ActionTypeConverter : NSObject

+ (NSString*)eventTypeStringForInt:(NSInteger)value;

@end
