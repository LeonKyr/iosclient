//
//  ActionTypeConverter.m
//  ParentMessenger
//
//  Created by Leo on 18/05/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "ActionTypeConverter.h"
#import "ActionType.h"

@implementation ActionTypeConverter

+ (NSString*)eventTypeStringForInt:(NSInteger)value
{
    // TODO: refactor
    switch (value) {
            
        case NOTE:
            return @"NOTE";
            break;
            
        case SINGIN:
            return @"SINGIN";
            break;
            
        case PHOTOS:
            return @"PHOTOS";
            break;
            
        case SINGOUT:
            return @"SINGOUT";
            break;
            
        case SLEEP:
            return @"SLEEP";
            break;
            
        case MEAL:
            return @"MEAL";
            break;
            
        case CHILD_WILL_HAVE_MEAL:
            return @"CHILD_WILL_HAVE_MEAL";
            break;
            
        case CHILD_ABSENCE:
            return @"CHILD_ABSENCE";
            break;
            
        default:
            return @"";
            break;
    }
    
    return @"";
    
}

@end
