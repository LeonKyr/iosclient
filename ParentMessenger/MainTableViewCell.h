//
//  MainTableViewCell.h
//  ParentMessenger
//
//  Created by lolsi on 07.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface MainTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIImageView *childrenPhotoView;
@property (nonatomic, strong) IBOutlet UILabel *teachersLabel;
@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *messageLabel;
@property (nonatomic, strong) IBOutlet UIImageView *eventTypeView;
@property (nonatomic, strong) IBOutlet UILabel *timeLabel;

@property (nonatomic, strong) IBOutlet UIImageView *separatorLineView;

@end