//
//  NavigationMenuViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 17/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "NavigationMenuViewController.h"
#import "SettingsViewController.h"
#import <CoreData/CoreData.h>
#import "CoreDataController.h"
#import "AppDelegate.h"

#import "ParentMessengerApi.h"
#import "UIImageView+AFNetworking.h"

#import "LearningINViewController.h"

@interface NavigationMenuViewController (){
    
    UILabel *label;
    UIImageView *imageView;
    AppDelegate *appDelegate;
}

@end

NSArray * menuTitles;

@implementation NavigationMenuViewController

#pragma mark - view life cycle

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    label.text = appDelegate.model.currentListener.name;
    
    if (appDelegate.model.currentListener.image &&
        appDelegate.model.currentListener.image.image)
    {
        imageView.image = appDelegate.model.currentListener.image.image;
    } else{
        imageView.image = [UIImage imageNamed:@"no_image"];
    }
}

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    self.tableView.separatorColor = [UIColor colorWithRed:153/255.0f
                                                    green:153/255.0f
                                                     blue:153/255.0f
                                                    alpha:1.0f];
    self.tableView.separatorInset = UIEdgeInsetsMake(0.0f, 0.0f, 0.0f, 0.0f);
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.opaque = NO;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.tableView.tableHeaderView = ({
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 200.0f)];
        view.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        CGFloat sizeIMG = 130.0f;
        
        imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, sizeIMG, sizeIMG)];
        imageView.center = view.center;
        imageView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        if (appDelegate.model.currentListener.image &&
            appDelegate.model.currentListener.image.image)
        {
            imageView.image = appDelegate.model.currentListener.image.image;
        } else{
            imageView.image = [UIImage imageNamed:@"no_image"];
        }
        
        imageView.layer.masksToBounds = YES;
        imageView.layer.cornerRadius = sizeIMG/2.0f;
//        imageView.layer.borderColor = [UIColor blackColor].CGColor;
//        imageView.layer.borderWidth = 0.333f;
        imageView.contentMode = UIViewContentModeScaleAspectFill;
        imageView.clipsToBounds = YES;
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(0, view.frame.size.height - 20.0f, self.tableView.frame.size.width, 20.0f)];
        
        label.text = appDelegate.model.currentListener.name;
        label.font = [UIFont fontWithName:@"Tahoma" size:17.5];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = [UIColor colorWithRed:77/255.0f
                                          green:77/255.0f
                                           blue:77/255.0f
                                          alpha:1.0f];
        label.textAlignment = NSTextAlignmentCenter;

        label.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
        
        [view addSubview:imageView];
        [view addSubview:label];
        view;
    });
    
    menuTitles = @[
                   NSLocalizedString(@"Home", nil),
                   NSLocalizedString(@"Profile", nil),
                   NSLocalizedString(@"Settings", nil),
                   NSLocalizedString(@"Tutorial", nil),
                   NSLocalizedString(@"Add child", nil),
                   NSLocalizedString(@"Introduce", nil),
                   NSLocalizedString(@"Tell Friends", nil),
                   NSLocalizedString(@"Logout", nil)];
}

#pragma mark -
#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0){
        
        UILabel *labels = (UILabel*)[cell.contentView viewWithTag:1];
        labels.text = menuTitles[indexPath.row];
        
        UIImageView *imgView = (UIImageView*)[cell.contentView viewWithTag:2];
        imgView.image = [UIImage imageNamed:menuTitles[indexPath.row]];
    
    } else  if (indexPath.section == 1){
        
        Child *child = (Child*)appDelegate.model.currentListener.children[indexPath.row];
        
        UILabel *labels = (UILabel*)[cell.contentView viewWithTag:1];
        labels.text = child.name;
        
        UIImageView *imgView = (UIImageView*)[cell.contentView viewWithTag:2];
        
        
        if (child.image != NULL)
        {
            NSURL *url = [NSURL URLWithString:[[[ParentMessengerApi create].baseUrl stringByAppendingPathComponent:@"image"] stringByAppendingPathComponent:child.image.externalId]];
            
            [imgView setImageWithURL:url placeholderImage:[UIImage imageNamed:@"no-available-image"]];
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)sectionIndex{
    
    if (sectionIndex == 0)
        return nil;
    
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, tableView.frame.size.width, 28.0f)];
    header.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleRightMargin;
    header.text = NSLocalizedString(@"   Children", nil);
    header.backgroundColor = [UIColor colorWithRed:204.0f/255.0f
                                             green:204.0f/255.0f
                                              blue:204.0f/255.0f
                                             alpha:1.0f];
    header.textColor = [UIColor colorWithRed:153.0f/255.0f
                                       green:153.0f/255.0f
                                        blue:153.0f/255.0f
                                       alpha:1.0f];
    header.font = [UIFont fontWithName:@"Tahoma" size:16.0f];
    header.textAlignment = NSTextAlignmentLeft;
    

    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)sectionIndex{
    
    if (sectionIndex == 0)
        return 0;
    
    return 28;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    UINavigationController *navigationController =
        (UINavigationController *)self.frostedViewController.contentViewController;

    // if we are in hardcoded 1st section we do specific actions
    NSLog(@"indexPath.row = %ld", (long)indexPath.row);
    if (indexPath.section == 0 &&
        indexPath.row < [menuTitles count])
    {
        if (indexPath.row == 1)
        {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileController"];
            
            [navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == 2)
        {
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsController"];
            
            [navigationController pushViewController:vc animated:YES];
        }
        else if (indexPath.row == 3)
        {
            LearningINViewController *vc = nil;
            
            if ([[UIScreen mainScreen] bounds].size.height == 568)
                vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialController4"];
            else
                vc = [self.storyboard instantiateViewControllerWithIdentifier:@"tutorialController3_5"];
            
            vc.isPushed = NO;
            
            UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:vc];
            nc.navigationBar.barStyle = UIBarStyleDefault;
            nc.navigationBar.translucent = NO;
            nc.navigationBar.barTintColor = [UIColor whiteColor];
            nc.navigationBarHidden = YES;
            
            [self presentViewController:nc animated:YES completion:nil];
            
        }
        // add child
        else if (indexPath.row == 4)
        {
            // TODO: Show Add Child View
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"addChild"];
            [navigationController pushViewController:vc animated:YES];
        }
        // introducde dcc
        else if (indexPath.row == 5)
        {
        // TODO: Show Add Child View
            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"introduceKindergarten"];
            [navigationController pushViewController:vc animated:YES];
        }
        // share with friends
        else if (indexPath.row == 6)
        {
            // TODO: Tell Friend - not implemented
//            UIViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsController"];
//            
//            [navigationController pushViewController:vc animated:YES];
        }
        // log out
        else if (indexPath.row == 7)
        {
            appDelegate.model.currentListener = nil;
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults removeObjectForKey:@"listener"];
            [defaults synchronize];
            [self.navigationController popToRootViewControllerAnimated:NO];
        }
    }
    
    [self.frostedViewController hideMenuViewController];
}

#pragma mark -
#pragma mark UITableView Datasource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 47;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if (sectionIndex == 0)
        return [menuTitles count];
    else
        return appDelegate.model.currentListener.children.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier0 = @"Cell0";
    static NSString *cellIdentifier1 = @"Cell1";
    
//    static NSString *cellIdentifier = @"Cell";
    
    UITableViewCell *cell = nil;
    
    if (indexPath.section == 0){
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier0];
        cell.backgroundColor = [UIColor clearColor];
        
        UILabel *labels = (UILabel*)[cell.contentView viewWithTag:1];
        labels.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
        labels.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
        
        UIImageView *imgView = (UIImageView*)[cell.contentView viewWithTag:2];
        imgView.contentMode = UIViewContentModeScaleAspectFit;
        
    } else {
        
        cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier1];
        cell.backgroundColor = [UIColor clearColor];
        
        UILabel *labels = (UILabel*)[cell.contentView viewWithTag:1];
        labels.textColor = [UIColor colorWithRed:153.0f/255.0f green:153.0f/255.0f blue:153.0f/255.0f alpha:1.0f];
        labels.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
    }
    
    
    /*
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor colorWithRed:62/255.0f green:68/255.0f blue:75/255.0f alpha:1.0f];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue" size:17];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    }
    
    if (indexPath.section == 0) {
        cell.textLabel.text = menuTitles[indexPath.row];
    } else {
        NSMutableArray * titles = [[NSMutableArray alloc] init];
        
        if (appDelegate.model.currentListener != NULL &&
            appDelegate.model.currentListener.children != NULL)
        {
            NSLog(@"Iterating through children in NavigationMenuViewController. Children.Count=%lu",
                  (unsigned long)appDelegate.model.currentListener.children.count);
            for (Child * child in appDelegate.model.currentListener.children)
            {
                [titles addObject:child.name];
            }
        }

        if (titles.count > indexPath.row)
        {
            cell.textLabel.text = titles[indexPath.row];
        }
    }
    */
    return cell;
}



@end
