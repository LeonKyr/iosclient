//
//  FeedbackViewController.m
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "Feedback.h"
#import "AppDelegate.h"
#import "ParentMessengerApi.h"
#import "FeedbackViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface FeedbackViewController ()

@end

@implementation FeedbackViewController

@synthesize backGroundView;

@synthesize message;
@synthesize titleLabel;
@synthesize sendButton;

@synthesize textView;

#pragma mark - textView delegate

- (BOOL)textView:(UITextView *)textView_ shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    
    if([text isEqualToString:@"\n"]) {
        [textView_ resignFirstResponder];
        return NO;
    }
    
    return YES;
}

#pragma mark - customize UI

- (void)customizeUI{
    
    self.titleLabel.textColor = [UIColor colorWithRed:77.0f/255.0f green:77.0f/255.0f blue:77.0f/255.0f alpha:1.0f];
    self.titleLabel.font = [UIFont fontWithName:@"Tahoma" size:17.5f];
    
    self.sendButton.backgroundColor = [UIColor colorWithRed:179.0f/255.0f green:179.0f/255.0f blue:179.0f/255.0f alpha:1.0f];
    [self.sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.sendButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    self.message.backgroundColor = [UIColor clearColor];
    
    UIColor *borderColor = [UIColor colorWithRed:207.0f/255.0f green:207.0f/255.0f blue:207.0f/255.0f alpha:1.0f];

    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, self.textView.frame.size.width, 1.0f)];
    view.backgroundColor = borderColor;
    [self.textView addSubview:view];
    
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0.0f, self.textView.frame.size.height - 1.0f, self.textView.frame.size.width, 1.0f)];
    view1.backgroundColor = borderColor;
    [self.textView addSubview:view1];
}

#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self customizeUI];
    
    self.textView.font = [UIFont fontWithName:@"Tahoma" size:15.0f];
    self.textView.textColor = [UIColor colorWithRed:201.0f/255.0f green:201.0f/255.0f blue:201.0f/255.0f alpha:1.0f];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    if (!self.title){
        
        self.title = NSLocalizedString(@"Feedback", nil);
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f
                                                                            green:178.0f/255.0f
                                                                             blue:178.0f/255.0f
                                                                            alpha:1.0f];
    }
    
    self.navigationController.navigationBarHidden = NO;
}

#pragma mark - touches delegate

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    
    [self.view endEditing:YES];
}

#pragma mark - IBAction methods

- (IBAction)cancel:(id)sender{
    
    self.tabBarController.selectedIndex = 0;
}

- (IBAction)feedback:(id)sender {
    
    NSLog(@"Feedback message = %@", self.textView.text);
    
    if ([self.textView.text isEqualToString:@""])
        return;
    
    [self.sendButton setEnabled:NO];
    
    Feedback * feedback = [[Feedback alloc] init];
    NSString * messageS = self.textView.text;
    messageS = [messageS stringByReplacingOccurrencesOfString:@"\n" withString:@" "];
    messageS = [messageS stringByTrimmingCharactersInSet:[NSCharacterSet newlineCharacterSet]];
    [feedback setText:messageS];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        ParentMessengerApi* api = [ParentMessengerApi create];
        
        AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
        [api provideFeedbackWithListener:appDelegate.model.currentListener andFeedback:feedback];
        
        [self performSelectorOnMainThread:@selector(done)
                               withObject:nil waitUntilDone:NO];
    });
}

#pragma mark - custom methods

- (void)done{
    
    [self.sendButton setEnabled:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Thank you for your feedback!", nil)
                                                    message:NSLocalizedString(@"We really appreciate your help.", nil)
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"Done", nil)
                                          otherButtonTitles:nil, nil];
    alert.delegate = self;
    
    [alert show];
}

#pragma mark - UIAlertView delegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    [self.textView setText:@""];
    
    if (buttonIndex == alertView.cancelButtonIndex)
        self.tabBarController.selectedIndex = 0;
}

@end