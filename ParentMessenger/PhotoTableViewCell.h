//
//  PhotoTableViewCell.h
//  ParentMessenger
//
//  Created by lolsi on 08.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface PhotoTableViewCell : UITableViewCell

@property (nonatomic, weak) IBOutlet UILabel *timeLabel;
@property (nonatomic, weak) IBOutlet UIImageView *photoView;
@property (nonatomic, weak) IBOutlet UIImageView *eventTypeView;
@property (nonatomic, weak) IBOutlet UIImageView *separatorLineView;
@property (weak, nonatomic) IBOutlet UILabel *teachersLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@property (nonatomic, strong) IBOutletCollection(UIImageView) NSArray *photoViews;

@end