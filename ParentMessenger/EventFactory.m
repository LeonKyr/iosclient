//
//  EventFactory.m
//  ParentMessenger
//
//  Created by Leo on 01/12/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import "EventFactory.h"
#import "EventDto.h"

@implementation EventFactory

-(EventDto*)createWithAction:(NSString*) action andEventId:(NSString*)eventId
{
    EventDto *event = [[EventDto alloc] init];
    if ([action isEqual: @"note"])
    {
        [event setActionType:NOTE];
    }
    else if ([action isEqual: @"photos"])
    {
        [event setActionType:PHOTOS];
    }
    else if ([action isEqual: @"sign-in"])
    {
        [event setActionType:SINGIN];
    }
    else if ([action isEqual: @"sign-out"])
    {
        [event setActionType:SINGOUT];
    }
    else if ([action isEqual: @"sleep"])
    {
        [event setActionType:SLEEP];
    }
    else if ([action isEqual: @"meal"])
    {
        [event setActionType:MEAL];
    }
    else if ([action isEqual: @"child-will-have-meal"])
    {
        [event setActionType:CHILD_WILL_HAVE_MEAL];
    }
    else if ([action isEqual: @"child-absence"])
    {
        [event setActionType:CHILD_ABSENCE];
    }

    
    [event setExternalId:eventId];
    
    return event;
}

@end
