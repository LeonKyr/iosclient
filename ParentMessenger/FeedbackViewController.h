//
//  FeedbackViewController.h
//  ParentMessenger
//
//  Created by Leonid Kyrpychenko on 13/10/2013.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "NoteView.h"
#import <UIKit/UIKit.h>

@interface FeedbackViewController : UIViewController<UIAlertViewDelegate, UITextViewDelegate>

@property (nonatomic, weak) IBOutlet UIView *backGroundView;

@property (nonatomic, weak) IBOutlet NoteView *message;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;

@property (nonatomic, weak) IBOutlet UIButton *sendButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;

@property (nonatomic, weak) IBOutlet UITextView *textView;

- (IBAction)cancel:(id)sender;
- (IBAction)feedback:(id)sender;

@end