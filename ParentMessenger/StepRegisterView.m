//
//  StepRegisterView.m
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "StepRegisterView.h"

@implementation StepRegisterView

@synthesize step;

- (void)drawRect:(CGRect)rect{
    
    if (step < 1 && step > 3)
        return;
    
    CGFloat size = 8.0f;
    
    CGPoint center = CGPointMake(rect.size.width/2.0f, rect.size.height/2.0f);
    
    CGRect leftRect = CGRectMake(center.x - size/2.0f - 60.0f, center.y - size/2.0f, size, size);
    CGRect centerRect = CGRectMake(center.x - size/2.0f, center.y - size/2.0f, size, size);
    CGRect rightRect = CGRectMake(center.x - size/2.0f + 60.0f, center.y - size/2.0f, size, size);
    
    CGRect leftLineRect = CGRectMake(leftRect.origin.x + size, center.y - 1.0f, 60.0f, 2.0f);
    CGRect rightLineRect = CGRectMake(centerRect.origin.x + size, center.y - 1.0f, 60.0f, 2.0f);
    
    
    UIColor *enabledColor = [UIColor colorWithRed:143.0f/255.0f
                                            green:195.0f/255.0f
                                             blue:191.0f/255.0f
                                            alpha:1.0f];
    
    UIColor *disabledColor = [UIColor colorWithRed:217.0f/255.0f
                                       green:217.0f/255.0f
                                        blue:217.0f/255.0f
                                       alpha:1.0f];
    
    CGRect topRect = CGRectMake(0.0f, 0.0f, rect.size.width, 1.0f);
    CGRect bottomRect = CGRectMake(0.0f, rect.size.height - 1.0f, rect.size.width, 1.0f);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, disabledColor.CGColor);
    CGContextFillRect(context, topRect);
    CGContextFillRect(context, bottomRect);
    
    switch (step) {
        
        case 1:
            CGContextSetFillColorWithColor(context, enabledColor.CGColor);
            CGContextFillEllipseInRect(context, leftRect);
            CGContextSetFillColorWithColor(context, disabledColor.CGColor);
            CGContextFillRect(context, leftLineRect);
            CGContextFillRect(context, rightLineRect);
            CGContextFillEllipseInRect(context, centerRect);
            CGContextFillEllipseInRect(context, rightRect);
            break;
            
        case 2:
            CGContextSetFillColorWithColor(context, enabledColor.CGColor);
            CGContextFillEllipseInRect(context, leftRect);
            CGContextFillRect(context, leftLineRect);
            CGContextFillEllipseInRect(context, centerRect);
            CGContextSetFillColorWithColor(context, disabledColor.CGColor);
            CGContextFillRect(context, rightLineRect);
            CGContextFillEllipseInRect(context, rightRect);
            break;
       
        case 3:
            CGContextSetFillColorWithColor(context, enabledColor.CGColor);
            CGContextFillEllipseInRect(context, leftRect);
            CGContextFillRect(context, leftLineRect);
            CGContextFillEllipseInRect(context, centerRect);
            CGContextFillRect(context, rightLineRect);
            CGContextFillEllipseInRect(context, rightRect);
            break;
        
        default:
            break;
    }
    
}

@end