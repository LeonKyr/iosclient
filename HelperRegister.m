//
//  HelperRegister.m
//  ParentMessenger
//
//  Created by lolsi on 18.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "HelperRegister.h"

@implementation HelperRegister

@synthesize email;
@synthesize phone;
@synthesize password;

@synthesize firstName;
@synthesize lastName;
@synthesize photo;

@synthesize childCode;
@synthesize rememberMe;

@synthesize fullName;

- (NSString *)getSfullName{
    
    return [[firstName stringByAppendingString:@" "] stringByAppendingString:lastName];
}

#define keyRemember @"KeyForRemember"
#define keyEmail @"KeyForEmail"
#define keyPassword @"KeyForPassword"

- (void)setRememberMe:(BOOL)rememberMe_{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if (rememberMe_){
        
        [defaults setBool:YES forKey:keyRemember];
        [defaults setObject:self.email forKey:keyEmail];
        [defaults setObject:self.password forKey:keyPassword];
        
    } else {
        
        [defaults setBool:NO forKey:keyRemember];
        [defaults removeObjectForKey:keyPassword];
        [defaults removeObjectForKey:keyEmail];
    }
    
    [defaults synchronize];
}

+ (HelperRegister *)sharedHelper{
    
    static HelperRegister *helper = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        helper = [[HelperRegister alloc] init];
    });
    return helper;
}

@end