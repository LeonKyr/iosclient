//
//  DetailCollectionViewController.h
//  ParentMessenger
//
//  Created by lolsi on 29.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "Event.h"
#import <UIKit/UIKit.h>
#import "DetailCollectionCell.h"

@interface DetailCollectionViewController : UICollectionViewController

@property (nonatomic, strong) Event *event;
- (IBAction)thankYou:(id)sender;

@end