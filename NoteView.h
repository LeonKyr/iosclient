//
//  NoteView.h
//  ParentMessenger
//
//  Created by lolsi on 14.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>

@interface NoteView : UITextView<UITextViewDelegate>

@end