//
//  LearningINViewController.h
//  ParentMessenger
//
//  Created by lolsi on 16.06.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "DDPageControl.h"

@interface LearningINViewController : UIViewController<UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *imageData;

@property (nonatomic, weak) IBOutlet UIButton *skipButton;
@property (nonatomic, weak) IBOutlet DDPageControl *pageControl;
@property (nonatomic, weak) IBOutlet UIScrollView *scrollView;

@property (nonatomic, assign) BOOL isPushed;

@property (nonatomic, weak) IBOutlet UIButton *mainButon;

@property (nonatomic, weak) IBOutlet UIButton *fullSkipButton;

- (IBAction)skip:(id)sender;

@end