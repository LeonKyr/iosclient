//
//  SyncEngineTests.m
//  ParentMessenger
//
//  Created by Leo on 29/01/2014.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SyncEngine.h"
#import "Listener.h"

@interface SyncEngineTests : XCTestCase

@end

@implementation SyncEngineTests

- (void)setUp
{
    [super setUp];
    // Put setup code here; it will be run once, before the first test case.
}

- (void)tearDown
{
    // Put teardown code here; it will be run once, after the last test case.
    [super tearDown];
}

- (void)testDownloadEventsAndSave
{
    SyncEngine * objectUnderTest = [[SyncEngine alloc] init];
    Listener * listener = [[Listener alloc] initWithName:@"name" andEmail:@"email@e.me" andExternalId:@"3467d02b-a77b-4e83-9a57-51881d8cf23c"];

    [objectUnderTest synchronizeEvents:listener withDeviceId:@"Leo-Mac" ];
}

@end
