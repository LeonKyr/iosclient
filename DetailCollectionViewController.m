//
//  DetailCollectionViewController.m
//  ParentMessenger
//
//  Created by lolsi on 29.04.14.
//  Copyright (c) 2014 Leonid Kyrpychenko. All rights reserved.
//
#import "ImageDb.h"
#import "CoreDataController.h"
#import "FooterThankCollection.h"
#import "MBGalleryViewController.h"
#import "DetailCollectionViewController.h"

@interface DetailCollectionViewController (){
    
    NSMutableArray *images;
}

@end

@implementation DetailCollectionViewController

@synthesize event;

#pragma mark - make event photos

- (void)makeEventPhotos{
    
    images = [NSMutableArray array];
    
    for (EventPropertyValue * epv in self.event.event2EventProperty){
        
        for (EventPropertyValueBag * epvBag in epv.eventProperty2EventPropertyValueBag){
            
            NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
            NSManagedObjectContext *managedObjectContext = [[CoreDataController sharedInstance] masterManagedObjectContext];
            NSEntityDescription *entity = [NSEntityDescription
                                           entityForName:@"Image" inManagedObjectContext:managedObjectContext];
            [fetchRequest setEntity:entity];
            
            [fetchRequest setPredicate:[NSPredicate predicateWithFormat:@"external_id == %@", epvBag.value]];
            
            NSError *error;
            ImageDb * image = (ImageDb*)[[managedObjectContext executeFetchRequest:fetchRequest error:&error] lastObject];
            
            [images addObject:image];
            
        }
    }
    
    [self.collectionView reloadData];

}

#pragma mark - set UICollectionView

- (void)setCollectionViewLayout{
    
    self.collectionView.backgroundColor = [UIColor whiteColor];
    
    UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setMinimumInteritemSpacing:3.0f];
    [flowLayout setMinimumLineSpacing:3.0f];
    [flowLayout setSectionInset:UIEdgeInsetsMake(5.0f, 5.0f, 0.0f, 3.0f)];
    [self.collectionView setCollectionViewLayout:flowLayout];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    return CGSizeMake(100, 100);
}

#pragma mark - IBAction methods

- (IBAction)close:(id)sender{
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)thanks:(id)sender{
    
    NSLog(@"thanks");
}

#pragma mark - make navigation

- (void)makeNavigationButton{
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Close"]
                                                             style:UIBarButtonItemStyleBordered
                                                            target:self
                                                            action:@selector(close:)];
    item.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = item;
}

#pragma mark - view life cycle

- (void)viewDidLoad{
    
    [super viewDidLoad];
    
    [self setCollectionViewLayout];
    
    [self makeEventPhotos];
    
//    [self makeNavigationButton];
}

- (void)viewWillAppear:(BOOL)animated{
    
    [super viewWillAppear:animated];
    
    self.navigationController.tabBarController.tabBar.hidden = YES;
    
    self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:178.0f/255.0f green:178.0f/255.0f blue:178.0f/255.0f alpha:1.0f];
    
    self.navigationController.navigationBar.titleTextAttributes = @{UITextAttributeTextColor : [UIColor blackColor]};
    
    
}

- (IBAction)back:(id)sender{
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UICollectionView dataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return [images count];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    
    return 1;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    
    return CGSizeMake(320.0f, 44.0f);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    
    return CGSizeMake(320.0f, 44.0f);
}

- (UICollectionViewCell*)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"CellIdentifier";
    
    DetailCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    ImageDb *image = [images objectAtIndex:indexPath.row];
    cell.imgView.image = [UIImage imageWithData:image.thumb];
    
    return cell;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    
    if (kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerCollectionView" forIndexPath:indexPath];
        reusableview.backgroundColor = [UIColor clearColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:reusableview.frame];
        label.textAlignment = NSTextAlignmentCenter;
        label.numberOfLines = 0;
        label.textColor = [UIColor blackColor];
        label.font = [UIFont fontWithName:@"Tahoma" size:14.0f];
        label.text = NSLocalizedString(@"Put this message for event", nil);
        [reusableview addSubview:label];
        
        return reusableview;
        
    } else if (kind == UICollectionElementKindSectionFooter){
    
        FooterThankCollection *reusableview = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"footerCollectionView" forIndexPath:indexPath];
        reusableview.backgroundColor = [UIColor clearColor];
        
        return reusableview;
    
    }
    
    return nil;
}

#pragma mark - action triggers

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([[segue identifier] isEqualToString:@"DetailCollectionCellIdentifier"]) {
        
        NSIndexPath *path = [[self.collectionView indexPathsForSelectedItems] lastObject];
        
        MBGalleryViewController *vc = [segue destinationViewController];
        vc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
        vc.event = self.event;
        
        vc.indexToSet = path.row;
    }
}

- (IBAction)thankYou:(id)sender {
    [[self navigationController] popViewControllerAnimated:YES];
}
@end